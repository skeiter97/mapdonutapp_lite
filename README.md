
# Development

## Generate SplashScreen

```
yo rn-toolbox:assets --splash splashscreen.psd --ios

```
https://medium.com/@pqkluan/how-to-implement-splash-screen-in-react-native-navigation-ee2184a1a96


- Add `$(HOME)/Documents/FacebookSDK` to "Framework Search Paths" on build settings(main project and FB plugin)

```

https://github.com/oblador/react-native-vector-icons
(support with cocoapods)

react-native-fbsdk: react-native-fbsdk@1.0.0-rc.5


#import <ReactNativeNavigation/ReactNativeNavigation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Firebase.h>
#import <GoogleMaps/GoogleMaps.h>

// Inside - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
  
  // GoogleMaps
  [GMSServices provideAPIKey:@"AIzaSyCkZe8aaPTh-6eYrVvLCfRu2aLsmeJEx9M"];
  
  // FB
  [[FBSDKApplicationDelegate sharedInstance] application:application
                           didFinishLaunchingWithOptions:launchOptions];
  
  // REact Native Navigation
  NSURL *jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
  [ReactNativeNavigation bootstrap:jsCodeLocation launchOptions:launchOptions];
  
  // Firebase
  [FIRApp configure];


// FB
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
  
  BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                openURL:url
                                                      sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                             annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                  ];
  // Add any custom logic here.
  return handled;
}

// FB
- (void)applicationDidBecomeActive:(UIApplication *)application {
  [FBSDKAppEvents activateApp];
}
```
