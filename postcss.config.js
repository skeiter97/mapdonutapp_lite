
const ENV = require('./src/global/variables');

module.exports = {
    map: false,
    plugins: {
        "postcss-extend": {},
        'postcss-each': {
            plugins: {
                afterEach: [
                    require('postcss-at-rules-variables')({
                        variables: {
                            '--themes': ENV.THEMES.join(', ')
                        }
                    }),
                    require('postcss-color-function')()
                ]
            }
        },
        'postcss-env-function': {
            importFrom: [
                './src/styles/variables.js'
            ]
        },
        "postcss-color-function": {}
    }
}