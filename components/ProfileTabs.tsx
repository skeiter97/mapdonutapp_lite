import React, { useState, useEffect } from 'React';
import { View, Text, FlatList } from 'react-native';
import { Mapdonut_Trip } from '../src/@types/trip';
import FastImage from 'react-native-fast-image';
import { MapdonutFirebaseStorage } from '../src/@types/general';
import { viewportWidth } from '../src/utils/utils';
import { TabView, TabBar } from 'react-native-tab-view';

import { useSelector } from 'react-redux';
import { StoreApp } from '../src/@types/store';
import { Mapdonut_ExploreItem } from '../src/@types/explore';

interface props_ProfileTabs {
    trips: Mapdonut_Trip[],
}

export default function ProfileTabs({ trips }: props_ProfileTabs) {
    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    const { words } = useSelector((state: StoreApp) => state.language);

    const [state, setState] = useState({
        index: 0,
        routes: [
            { key: 'first', title: words.SPOTS },
            { key: 'second', title: words.JOURNALS },
            { key: 'third', title: words.PINES },
        ],
    });

    return <TabView
        navigationState={state}
        renderScene={({ route }) => {
            switch (route.key) {
                case 'first':
                    return <Trips trips={trips} />;
                case 'second':
                    return <Trips trips={trips}/>;
                case 'third':
                    return <Trips trips={trips} />;
                default:
                    return null;
            }
        }}
        onIndexChange={(index: number) => setState(Object.assign(state, {index}))}
        initialLayout={{ width: viewportWidth }}
        style={{
            borderTopColor: themeVariables.TEXT_COLOR_PLACEHOLDER,
            borderTopWidth: 0.4
        }}
        sceneContainerStyle={{
            paddingTop: 1.5
        }}
        renderTabBar={props =>
            <TabBar
                {...props}
                indicatorStyle={{ backgroundColor: themeVariables.TEXT_COLOR, height: 0.8 }}
                style={{ backgroundColor: themeVariables.BG_LAYOUT }}
                activeColor={themeVariables.TEXT_COLOR}
                inactiveColor={themeVariables.TEXT_COLOR_PLACEHOLDER}
                onTabPress={({ route }) => {
                    setState(Object.assign(state, { index: route.key === 'first' ? 0 :
                        route.key === 'second' ? 1 : 2
                    }));
                }}
                renderLabel={({ route, color }) => (
                    <Text style={{ color }}> {route.title} </Text>
                )}
            />
        }
    />
}

interface props_Trip {
    trips: Mapdonut_Trip[]
}

export function Trips({ trips }: props_Trip) {
    
    const allPhotos = trips.map(t => t.photos).reduce((acc, cur) => acc.concat(cur), []);

    return <FlatList
        data={allPhotos}
        renderItem={({ item }) => <ImageCard photo={item} progress={item.metadata.progress} />}
        keyExtractor={(item) => item.url}
        numColumns={3}
        style={{
            width: viewportWidth
        }}
    ></FlatList>
}

interface props_ImageCard {
    photo: Mapdonut_ExploreItem,
    progress: number
}

const ShowLoadingFirebase = ({ progress }: props_ImageCard) => <View style={{
    position: "absolute",
    width: '100%',
    height: '100%',
    zIndex: 10,
    backgroundColor: 'rgba(0, 0, 0, .5)'
}}>
    <View style={{
        position: "absolute",
        right: 0,
        width: 40,
        height: 40,
        backgroundColor: 'white',
        justifyContent: "center",
        alignItems: "center"
    }}>
        <Text style={{ fontSize: 10 }}> {progress } </Text>
    </View>
</View>;

export function ImageCard({ photo }: props_ImageCard) {
    
    const trips = useSelector((state: StoreApp) => state.currentUser.trips);
    const { themeVariables } = useSelector((state: StoreApp) => state.theme);

    const tripIndex = trips.findIndex(t => t.id === photo.tripId);

    const photoIndex = tripIndex !== -1 ?
        trips[tripIndex].photos.findIndex(p => p.id === photo.id) : -1;

    const prog = (tripIndex !== -1 && photoIndex !== -1) ?
        useSelector((state: StoreApp) => state.currentUser.trips[tripIndex].photos[photoIndex].metadata.progress) : 0;

    return <View style={{ width: viewportWidth / 3, height: viewportWidth / 3 }}>
        
        {!photo.metadata.isLoadedToCloud ? <ShowLoadingFirebase {...{ photo, progress: prog }} /> : null}

        <FastImage
            style={{ width: viewportWidth / 3, height: viewportWidth / 3,
                backgroundColor: themeVariables.BG_LAYOUT_ACCENT
            }}
            source={{
                uri: photo.url,
                priority: FastImage.priority.normal
            }}
            resizeMode={FastImage.resizeMode.cover}
            fallback={true}
            onProgress={(pro) => {
                //console.log({ pro: pro.nativeEvent, photo });
            }}
        />
    </View>
}