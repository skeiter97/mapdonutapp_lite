import { createStore as _createStore, applyMiddleware, compose, Reducer } from 'redux';
import { combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger'
import thunkMiddleware from 'redux-thunk'
import currentUserReducer from '../reducers/currentUserReducer';
import { themeReducer } from '../reducers/themeReducer';
import { languageReducer } from '../reducers/languageReducer';
import { userActionSetUser, currentUserActionSet } from '../actions/userActions';
import { Mapdonut_User } from '../../@types/user';
import { uiReducer } from '../reducers/UIReducer';
import { StoreApp } from '../../@types/store';
import { eventsReducer } from '../reducers/eventsReducer';

interface anyAction {
    type: string,
    payload: {
        [prop: string]: any
    }
}

const rootReducer: Reducer<StoreApp, anyAction> = combineReducers({
    currentUser: currentUserReducer,
    theme: themeReducer,
    language: languageReducer,
    ui: uiReducer,
    events: eventsReducer
});

const loggerMiddleware = createLogger();

export const store = _createStore(
    rootReducer,
    (__DEV__ ? composeWithDevTools : compose)(
        applyMiddleware(
            thunkMiddleware,
            ...(__DEV__ ? [loggerMiddleware]: [])
        )
    )
);