import CameraRoll from "@react-native-community/cameraroll";
import { pageInfo } from "../../@types/camera";

export interface uiAction {
    type: string,
    payload: {
        rn_tabs: any,
        cameraRoll: cameraRollData,
        rnHeights: rnHeights
    }
}

export interface UIState {
    rn_tabs: any,
    rn_tabs_filtered: any[],
    cameraRoll: cameraRollData,
    rnHeights: rnHeights
}

const parseState = (tabs: any = null, cameraRoll: cameraRollData, rnHeights: rnHeights) => ({
    rn_tabs: tabs,
    rn_tabs_filtered: (tabs || []).root.children.map((ch: any) => ({
        id: ch.children[0].id,
        name: ch.children[0].data.name
    })),
    cameraRoll,
    rnHeights
});

const initial = {
    rn_tabs: null,
    rn_tabs_filtered: [],
    cameraRoll: {
        photos: [],
        cursor: {} as pageInfo
    },
    rnHeights: {
        statusBarHeight: 0,
        topBarHeight: 0,
        bottomTabsHeight: 0
    }
}

export function uiReducer(state: UIState = initial, action: uiAction): UIState {
    switch (action.type) {
        case 'SET_UI_HOME':
            const { rn_tabs, cameraRoll, rnHeights } = action.payload;
            return parseState(rn_tabs, cameraRoll, rnHeights);
        default:
            return state;
    }
}

interface cameraRollData {
    photos: CameraRoll.PhotoIdentifier[],
    cursor: pageInfo
}

interface rnHeights {
    statusBarHeight: number,
    topBarHeight: number,
    bottomTabsHeight: number
}