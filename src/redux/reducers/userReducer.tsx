import { Mapdonut_User } from "../../@types/user";
import { userAction, userActions } from '../actions/userActions';
import { Mapdonut_Trip } from "../../@types/trip";
import tripsReducer, { tripsActions } from "./tripsReducer";

export interface UserStore {
    uid: string,
    watch: () => void,
    data: Mapdonut_User,
    trips: Mapdonut_Trip[],
    watchTrips: () => void
};

export const initialUserStore = (u?: Mapdonut_User) => ({
    uid: u ? u.uid : '',
    data: u ? u : {} as Mapdonut_User,
    watch: () => { },
    trips: [],
    watchTrips: () => {}
} as UserStore);

export function userReducer(state: UserStore = initialUserStore(), action: userAction) {
    const checkUid = () => action.payload.userId === state.uid;
    switch (action.type) {
        case userActions.SET_USER:
            if (!action.payload.user) return state;
            return initialUserStore(action.payload.user);
        case userActions.UPDATE_USER_REAL_TIME:
            if (!checkUid()) return state;
            if (!action.payload.user) return state;
            return Object.assign(state, { data: action.payload.user });
        case userActions.UPDATE_USER:
            if (!checkUid()) return state;
            if (!action.payload.userPartial) return state;
            return Object.assign(state, {
                data: {...state.data , ...action.payload.userPartial}
            });
        case userActions.UPDATE_TRIPS:
            if (!checkUid()) return state;
            if (!action.payload.trips) return state;
            return Object.assign(state, {
                trips: tripsReducer(
                    state.trips,
                    { type: tripsActions.SET, payload: { trips: action.payload.trips}}
                )
            });
        case userActions.SAVE_TRIPS_WATCH:
            if (!checkUid()) return state;
            if (!action.payload.watch) return state;
            return Object.assign(state, { watchTrips: action.payload.watch });
        case userActions.FINISH_TRIPS_WATCH:
            if (!checkUid()) return state;
            state.watchTrips();
            return state;
        case userActions.ADD_TRIP:
            if (!checkUid()) return state;
            if (!action.payload.trip) return state;
            return Object.assign(state, {
                trips: tripsReducer(
                    state.trips,
                    { type: tripsActions.ADD, payload: { trip: action.payload.trip } }
                )
            });
        case userActions.UPDATE_TRIP:
            if (!checkUid()) return state;
            //if (!action.payload.trip || !action.payload.id) return state;
            if (!action.payload.tripsAction) return;
            return Object.assign(state, {
                trips: tripsReducer(
                    state.trips,
                    action.payload.tripsAction
                    //{ type: tripsActions.UPDATE, payload: { id: action.payload.id, trip: action.payload.trip } }
                )
            });
        default:
            return state;
    }
}
