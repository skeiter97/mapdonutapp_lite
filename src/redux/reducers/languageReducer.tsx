import { LANGUAGE_DEFAULT, DICTIONARIES } from '../../global/variables';
import { userActionUpdate } from '../actions/userActions';
import { Navigation } from 'react-native-navigation';
import { StoreApp } from '../../@types/store';
import { simpleObject } from '../../@types/general';

interface dictionaries {
    [prop: string] : {
        [prop: string]: string
    }
}

const dictionaries: dictionaries = DICTIONARIES;

const getDictionary = (lang: string) => lang === 'zh-hans' ? dictionaries.zh_hans :
    lang === 'zh-hant' ? dictionaries.zh_hant :
    dictionaries[lang]; 

export interface languageAction {
    type: string,
    payload: {
        lang: string
    }
}

export interface languageState {
    lang: string,
    parseTranslation: (code: string, data: simpleObject) => string,
    words: {
        [prop: string]: string
    }
}

export function languageActionChange(lang: string) {
    return (dispatch: any, getState: () => StoreApp) => {
        const { isLogged, data: { uid } } = getState().currentUser;
        dispatch({ type: 'CHANGE_LANGUAGE', payload: { lang } });
        applyLanguage(lang, getState().ui.rn_tabs_filtered);
        if (isLogged) dispatch(userActionUpdate(uid, { languageSelected: lang }));
        return Promise.resolve();
    }
};

export function applyLanguage(_lang: string, tabs: any[] = []) {
    const dictionary = { ...dictionaries.en, ...getDictionary(_lang)};
    tabs.forEach(c => {
        const title = `${c.name.replace('mapdonut.', '').replace('Screen', '').toUpperCase()}`;
        Navigation.mergeOptions(c.id, {
            topBar: {
                title: {
                    text: dictionary[title]
                }
            },
            bottomTab: {
                text: dictionary[title],
            },
        });
    });
}

const parseTranslation = (words: simpleObject, code: string, data: simpleObject = {}) => {
    let res = words[code];
    Object.keys(data).forEach(k => {
        res = (words[code] || '').replace('${' + k + '}', data[k]);
    });
    return res
}

const initialState = (lang: string): languageState => {

    const dictionary = getDictionary(lang);
    const words = {
        ...dictionaries.en, // Base Language
        ...dictionary
    };

    return {
        lang: lang,
        words,
        parseTranslation(code: string, data: simpleObject) {
            return parseTranslation(words, code, data);
        }
    }
};

export function languageReducer(state: languageState = initialState(LANGUAGE_DEFAULT), action: languageAction): languageState {
    switch (action.type) {
        case 'CHANGE_LANGUAGE':
            return initialState(action.payload.lang);
        default:
            return state;
    }
}