import { userReducer, initialUserStore, UserStore } from './userReducer';
import { userAction, currentUserActions } from '../actions/userActions';
import { Mapdonut_User } from '../../@types/user';

export interface CurrentUserStore extends UserStore {
    isLogged: boolean;
};
const initialCurrentUserStore = (u?: Mapdonut_User) => ({
    ...(initialUserStore(u)),
    isLogged: !!u
} as CurrentUserStore);

export default function currentUserReducer(state: CurrentUserStore = initialCurrentUserStore(), action: userAction) {

    switch (action.type) {
        case currentUserActions.SET_CURRENT_USER:
            return initialCurrentUserStore(action.payload.user);
        case currentUserActions.LOGOUT:
            state.watch();
            return initialCurrentUserStore();
        default:
            return Object.assign(userReducer(state, action), { isLogged: true });
    }
    
};