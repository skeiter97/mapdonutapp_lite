import { Mapdonut_Trip } from "../../@types/trip";
import { MapdonutFirebaseStorage } from "../../@types/general";
import { actionGeneral } from "../actions/general";

export enum tripsActions {
    SET = 'TRIPS_SET',
    ADD = 'TRIPS_ADD',
    REMOVE = 'TRIPS_REMOVE',
    UPDATE = 'TRIPS_UPDATE',
    UPDATE_PIC = 'UPDATE_PIC'
};

export interface tripsActionAdd {
    type: tripsActions.ADD,
    payload: {
        trip: Mapdonut_Trip
    },
}

export interface tripsActionUpset {
    type: tripsActions.REMOVE | tripsActions.UPDATE,
    payload: {
        id: string,
        trip: Mapdonut_Trip
    },
}

export interface tripsActionSet {
    type: tripsActions.SET,
    payload: {
        trips: Mapdonut_Trip[]
    },
}

export interface tripsActionUpdatePic {
    type: tripsActions.UPDATE_PIC,
    payload: {
        picId: string,
        tripId: string,
        picPartial: Partial<MapdonutFirebaseStorage>
    },
}

export type tripsActionTypes = tripsActions.ADD | tripsActions.REMOVE | tripsActions.UPDATE |
    tripsActions.SET | tripsActions.UPDATE_PIC;

export type tripsAction = tripsActionSet | tripsActionAdd | tripsActionUpset | tripsActionUpdatePic;

export default function tripsReducer(state: Mapdonut_Trip[] = [], action: tripsAction) {
    const getIndex = (id: string) =>  state.findIndex(t => t.id === id);
    let index: number;

    switch (action.type) {
        case tripsActions.SET:
            return action.payload.trips
        case tripsActions.ADD:
            return state.concat(action.payload.trip);
        case tripsActions.REMOVE:
            if (!action.payload.id) return state;
            index = getIndex(action.payload.id);
            if (index === -1) return state;
            state.splice(index, 1);
            return state;
        case tripsActions.UPDATE:
            if (!action.payload.id) return state;
            index = getIndex(action.payload.id);
            if (index === -1) return state;
            state[index] = action.payload.trip;
            return state;
        case tripsActions.UPDATE_PIC:
            //Todo
            index = getIndex(action.payload.tripId);
            if (index === -1) return state;
            //const photos =
            imagesReducer(state[index].photos, { type: 'UPDATE', payload: { id: action.payload.picId, picPartial: action.payload.picPartial }})
            //console.log({ action, state, photos });
            return state;//.map(t => t);
        default:
            return state
    }
}

function imagesReducer(state: MapdonutFirebaseStorage[] = [], action: actionGeneral) {
    const getIndex = (id: string) => state.findIndex(t => t.id === id);
    let index: number;
    switch (action.type) {
        case 'UPDATE': 
            index = getIndex(action.payload.id);
            if (index === -1) return state;
            state[index] = Object.assign(state[index], action.payload.picPartial);
            return state;
        default:
            return state;
    }
}