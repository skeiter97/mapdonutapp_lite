import { getTheme } from '../../styles/variables';
import { THEME_DEFAULT } from '../../global/variables';
import { Navigation, Options } from 'react-native-navigation';
import { userActionUpdate } from '../actions/userActions';
import { StoreApp } from '../../@types/store';
import { StatusBar } from 'react-native';

export interface themeAction {
    type: string,
    payload: {
        theme: string
    }
}

export interface themeState {
    theme: string,
    themeVariables: {
        [prop: string]: any
    }
}

const initialState = (theme: string) => ({
    theme: theme,
    themeVariables: getTheme(theme).variables,
});

export function themeActionChange(theme: string) {
    return (dispatch: any, getState: () => StoreApp) => {
        const uid = getState().currentUser.uid;
        dispatch({ type: 'CHANGE_THEME', payload: { theme } });
        applyTheme(theme, getState().ui.rn_tabs_filtered);
        dispatch(userActionUpdate(uid, {themeName: theme}));
        return Promise.resolve();
    }
}

export function applyTheme(themeName: string = 'default', tabs: any[] = []) {

    StatusBar.setBarStyle(themeName !== 'dark' ? 'dark-content' : 'light-content', true);

    const { variables: { RN } } = getTheme(themeName);

    tabs.forEach(c => {
        Navigation.mergeOptions(c.id, RN);
    });

    Navigation.setDefaultOptions(RN as Options);
}

// Reducer
export function themeReducer(state: themeState = initialState(THEME_DEFAULT), action: themeAction) {
    switch (action.type) {
        case 'CHANGE_THEME':
            return initialState(action.payload.theme)
        default:
            return state
    }
}

