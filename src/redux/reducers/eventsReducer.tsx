import { Mapdonut_Event } from "../../@types/event";
import { actionGeneral } from "../actions/general";

export enum eventActions {
    SET_NEARBY_EVENTS = 'SET_NEARBY_EVENTS',
    SET_NEARBY_MY_EVENTS = 'SET_NEARBY_MY_EVENTS',
    SET_NEARBY_SUGGESTED_EVENTS = 'SET_NEARBY_SUGGESTED_EVENTS',
    EVENTS_ADD = 'EVENTS_ADD'
};

export interface EventStore {
    nearByEvents: Mapdonut_Event[],
    myEvents: Mapdonut_Event[],
    suggestedEvents: Mapdonut_Event[] 
};

export const initialEventStore = () => ({
    nearByEvents: [],
    myEvents: [],
    suggestedEvents: [] 
} as EventStore);

export function eventsReducer(state: EventStore = initialEventStore(), action: actionGeneral) {
    switch (action.type) {
        case eventActions.EVENTS_ADD: 
            return Object.assign(state, { nearByEvents: state.nearByEvents.concat(action.payload.data) })
        case eventActions.SET_NEARBY_EVENTS:
            return Object.assign(state, { nearByEvents: action.payload.nearByEvents })
        case eventActions.SET_NEARBY_MY_EVENTS:
            return Object.assign(state, { nearByEvents: action.payload.myEvents })
        case eventActions.SET_NEARBY_SUGGESTED_EVENTS:
            return Object.assign(state, { nearByEvents: action.payload.suggestedEvents })
        default:
            return state;
    }
}
