import firebase from 'react-native-firebase';
import { Navigation } from "react-native-navigation";
import { WELCOME_SCREEN } from "../../screens/screenNames";
import { currentUserActions, userActionSaveUpdate } from "./userActions";
import { saveExtraDataUser } from '../../utils/utils';
import { StoreApp } from '../../@types/store';
import { Mapdonut_User } from '../../@types/user';

const saveFirebaseCurrentUser = async (uid: string, userPartial: Partial<Mapdonut_User>) => {
    const httpsCallable = firebase.functions().httpsCallable('updateCurrentUser');
    const u = await httpsCallable({ uid: uid, userPartial: userPartial });
    await saveExtraDataUser(uid, userPartial);
    return Promise.resolve(u);
};

export function currentUserActionLogout() {
    return async(dispatch: any) => {
        await Promise.all([
            Navigation.dismissAllModals(),
            Navigation.setRoot({
                root: {
                    component: {
                        name: WELCOME_SCREEN,
                        passProps: {
                            skipWelcome: true
                        },
                    }
                }
            })
        ]);
        dispatch({ type: currentUserActions.LOGOUT });
        return Promise.all([
            firebase.auth().signOut(),
        ]);
    }
}

export function currentUserActionUpdateEmail(email: string) {
    return async (dispatch: any, getState: () => StoreApp) => {
        const cu = getState().currentUser.data;
        dispatch(userActionSaveUpdate(cu.uid, { email }));
        try {
            const u = await saveFirebaseCurrentUser(cu.uid, { email });
            return u;
        } catch (error) {
            dispatch(userActionSaveUpdate(cu.uid, cu));
            return Promise.reject(error);
        }
    }
}

export function currentUserActionUpdatePhoneNumberl(phoneNumber: string) {
    return async (dispatch: any, getState: () => StoreApp ) => {
        const cu = getState().currentUser.data;
        dispatch(userActionSaveUpdate(cu.uid, { phoneNumber }));
        try {
            const u = await saveFirebaseCurrentUser(cu.uid, { phoneNumber });
            return u;
        } catch (error) {
            dispatch(userActionSaveUpdate(cu.uid, cu));
            return Promise.reject(error);
        }
    }
}

export function currentUserActionUpdateUsername(username: string) {
    return async (dispatch: any, getState: () => StoreApp) => {
        const cu = getState().currentUser.data;
        dispatch(userActionSaveUpdate(cu.uid, { username }));
        try {
            const u = await saveFirebaseCurrentUser(cu.uid, { username });
            return u;
        } catch (error) {
            dispatch(userActionSaveUpdate(cu.uid, cu));
            return Promise.reject(error);
        }
    }
}
