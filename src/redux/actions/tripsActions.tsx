import { Mapdonut_Trip_pre, Mapdonut_Trip } from "../../@types/trip";
import uuidv1 from 'uuid/v1';
import { uploadPictureFirebaseStorage } from "../../utils/uploadMedia";
import { StoreApp } from "../../@types/store";
import { saveDataInFirebase, createFirebaseTimestamp } from "../../utils/utils";
import { userAction, userActions } from "./userActions";
import { tripsActions } from "../reducers/tripsReducer";

const parseTrip = (tripUuid: string, userId: string, item: Mapdonut_Trip_pre, afterUploadToCloud: boolean, progress: number = 0): Mapdonut_Trip => {
    const picId = `trips/${uuidv1()}`;
    return {
        id: tripUuid,
        location: item.locationGeopoint,
        locationText: item.locationText,
        locationId: item.locationId,
        description: item.description,
        photos: item.photosToUpload.map(p => {
            return {
                url: p.uri,
                id: picId,
                tripId: tripUuid,
                type: 'image',
                metadata: {
                    isLoadedToCloud: afterUploadToCloud,
                    progress: progress
                }
            }
        }),
        createdAt: createFirebaseTimestamp(),
        updatedAt: createFirebaseTimestamp(),
        userId,
        metadata: {
            isLoadedToCloud: afterUploadToCloud,
            progress: progress
        }
    };
}

export function tripsActionAdd(item: Mapdonut_Trip_pre) {

    return async (dispatch: any, getState: () => StoreApp) => {
        const userId = getState().currentUser.uid
        const tripUuid = uuidv1() as string;

        let tripsToSave = parseTrip(tripUuid, userId, item, false);

        const action: userAction = {
            type: userActions.ADD_TRIP,
            payload: {
                userId,
                trip: tripsToSave
            }
        }
        dispatch(action);

        const photos = await Promise.all(tripsToSave.photos.map(img => {
            return uploadPictureFirebaseStorage(
                img.url,
                img.id,
                (per) => {
                    const _action: userAction = {
                        type: userActions.UPDATE_TRIP,
                        payload: {
                            userId,
                            tripsAction: {
                                type: tripsActions.UPDATE_PIC,
                                payload: {
                                    picId: img.id,
                                    tripId: tripUuid,
                                    picPartial: {
                                        metadata: {
                                            progress: per,
                                            isLoadedToCloud: false
                                        }
                                    }
                                }
                            }
                        }
                    };
                    dispatch(_action);
                }
            )
        }));

        let toSave: Mapdonut_Trip = {
            ...parseTrip(tripUuid, userId, item, true),
            ...{ photos: photos.map(p => ({
                url: p.url,
                id: p.id,
                tripId: tripUuid,
                type: 'image',
                metadata: {
                    isLoadedToCloud: true,
                    progress: 100
                }
            }))}
        };
        return saveDataInFirebase('trips', tripUuid, toSave)
    }
}
