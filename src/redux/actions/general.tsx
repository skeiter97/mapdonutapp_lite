export interface actionGeneral {
    type: string,
    payload: {
        [prop: string]: any
    },
}