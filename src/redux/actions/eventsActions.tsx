import { Mapdonut_Event } from "../../@types/event";
import uuidv1 from 'uuid/v1';
import { saveDataInFirebase, updateDataInFirebase } from "../../utils/utils";
import { eventActions } from "../reducers/eventsReducer";

export function eventsActionAdd(data: Mapdonut_Event) {
    const uuid = uuidv1();
    return async (dispatch: any) => {
        dispatch({ type: eventActions.EVENTS_ADD, payload: {data: {...data, id: uuid}} });
        try {
            await saveDataInFirebase('events', uuid, data);
        } catch (error) {
            dispatch({ type: 'EVENTS_REMOVE', payload: {id: uuid} });
        }
    };
}
/*
export function eventsActionUpdate(id: string, data: Partial<Mapdonut_Event>) {
    return async (dispatch: any) => {
        dispatch({ type: 'EVENTS_UPDATE', payload: {id, data } });
        try {
            await updateDataInFirebase('events', id, data)
        } catch (error) {
            dispatch({ type: 'EVENTS_REMOVE', payload: { id } });
        }
    };
}
*/