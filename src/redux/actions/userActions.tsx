import { watchUser, saveExtraDataUser, isDefined } from '../../utils/utils';
import { Mapdonut_User } from '../../@types/user';
import firebase from 'react-native-firebase';
import { uploadPicture } from '../../screens/hooks/useSavePicture';
import { StoreApp } from '../../@types/store';
import { Mapdonut_Trip } from '../../@types/trip';
import { tripsAction } from '../reducers/tripsReducer';

export enum userActions {
    UPDATE_USER_REAL_TIME = 'UPDATE_USER_REAL_TIME',
    UPDATE_USER = 'UPDATE_USER',
    SET_USER = 'SET_USER',
    ADD_TRIP = 'ADD_TRIP',
    UPDATE_TRIP = 'UPDATE_TRIP',
    KEEP = 'KEEP',
    UPDATE_TRIPS = 'UPDATE_TRIPS',
    SAVE_TRIPS_WATCH = 'SAVE_TRIPS_WATCH',
    FINISH_TRIPS_WATCH = 'FINISH_TRIPS_WATCH'
};

enum _currentActions {
    SET_CURRENT_USER = 'SET_CURRENT_USER',
    LOGOUT = 'LOGOUT'
};

export const currentUserActions = {
    ...userActions,
    ..._currentActions
}

type userActionType = userActions.UPDATE_USER_REAL_TIME | userActions.SET_USER |
    userActions.UPDATE_USER |
    userActions.ADD_TRIP |
    userActions.UPDATE_TRIP |
    userActions.KEEP |
    userActions.SAVE_TRIPS_WATCH |
    userActions.FINISH_TRIPS_WATCH |
    userActions.UPDATE_TRIPS;

type currentUserType = _currentActions.SET_CURRENT_USER | _currentActions.LOGOUT;

export interface userAction {
    type: userActionType | currentUserType,
    payload: {
        user?: Mapdonut_User,
        userId?: string,
        userPartial?: Partial<Mapdonut_User>,
        trips?: Mapdonut_Trip[],
        trip?: Mapdonut_Trip,
        watch?: () => void,
        id?: string,

        tripsAction?: tripsAction
    },
}

export function userActionKeep(): userAction {
    return {
        type: userActions.KEEP,
        payload: {}
    }
}

export function userActionSaveUpdate(userId: string, userPartial: Partial<Mapdonut_User>): userAction {
    return {
        type: userActions.UPDATE_USER,
        payload: {
            userId,
            userPartial
        }
    }
}

const generateUserPayload = (user: Mapdonut_User, dispatch: any, checkRealtimeUpdates: boolean = false) => ({
    user,
    userId: user.uid,
    watch: checkRealtimeUpdates ? watchUser(user.uid, u => {
        if (!u) return;
        dispatch(userActionUpdateInRealTime(u));
    }) : () => { }
})

export function userActionSetUser(user: Mapdonut_User, checkRealtimeUpdates: boolean = false) {
    return (dispatch: any) => Promise.resolve(dispatch({
        type: userActions.SET_USER,
        payload: generateUserPayload(user, dispatch, checkRealtimeUpdates)
    }));
}

export function currentUserActionSet(user: Mapdonut_User) {
    return (dispatch: any) => Promise.resolve(dispatch({
        type: currentUserActions.SET_CURRENT_USER,
        payload: generateUserPayload(user, dispatch, true)
    }));
}

export function userActionUpdateInRealTime(user: Mapdonut_User): userAction {
    return {
        type: userActions.UPDATE_USER_REAL_TIME,
        payload: { user, userId: user.uid }
    }
}

export function userActionUpdate(uid: string, userPartial: Partial<Mapdonut_User>) {

    const _progress = (x: number, path?: string) => {
        console.log('Uploading', {x, path});
    };

    return async (dispatch: any, getState: () => StoreApp) => {

        const cu = getState().currentUser.data;
        
        try {

            dispatch(userActionSaveUpdate(uid, userPartial));

            const [photoURL, coverURL] = await Promise.all([
                isDefined(userPartial, 'photoURL') && userPartial.photoURL ?
                    uploadPicture(userPartial.photoURL || '', `users/${uid}/photo`, _progress) :
                    Promise.resolve(isDefined(userPartial, 'photoURL') ? userPartial.photoURL : cu.photoURL),
                isDefined(userPartial, 'coverURL') && userPartial.coverURL ?
                    uploadPicture(userPartial.coverURL || '', `users/${uid}/cover`, _progress) :
                    Promise.resolve(isDefined(userPartial, 'coverURL') ? userPartial.coverURL : cu.coverURL),
            ]);

            if (cu.uid === uid) {
                const userFirebase = firebase.auth().currentUser;
                if (!userFirebase) return Promise.resolve();
                if (
                    (isDefined(userPartial, 'photoURL') && cu.photoURL !== userPartial.photoURL ) ||
                    (isDefined(userPartial, 'displayName') && cu.displayName !== userPartial.displayName && userPartial.displayName)
                ) await userFirebase.updateProfile({
                    photoURL: isDefined(userPartial, 'photoURL') ? userPartial.photoURL : cu.photoURL,
                    displayName: isDefined(userPartial, 'displayName') ? userPartial.displayName : cu.displayName,
                });
            }
            
            await saveExtraDataUser(uid, { ...userPartial, photoURL, coverURL});

            return Promise.resolve({ ...cu, ...userPartial });
            
        } catch (error) {
            dispatch(userActionSaveUpdate(uid, cu));
            return Promise.reject(error);
        }

    }
}

export function userActionGetTrips(uid: string) { //Promise<Mapdonut_Trip[]> {

    return async (dispatch: any, getState: () => StoreApp) => {
        const collectionRef = firebase.firestore().collection('trips').where('userId', '==', uid);//.onSnapshot()
        
        const watch = collectionRef.onSnapshot((snaps) => {
            const trips = snaps.docs.map(d => d.data() as Mapdonut_Trip);
            console.log({ snaps, trips });
            const action: userAction = {
                type: userActions.UPDATE_TRIPS,
                payload: {
                    userId: uid,
                    trips
                }
            }
            dispatch(action)
        });

        const action: userAction = {
            type: userActions.SAVE_TRIPS_WATCH,
            payload: {
                watch
            }
        }
        dispatch(action)

        return Promise.resolve([]);
    }
}

export function userActionFinishTripsWatch(userId: string): userAction {
    return {
        type: userActions.FINISH_TRIPS_WATCH,
        payload: { userId }
    }
}