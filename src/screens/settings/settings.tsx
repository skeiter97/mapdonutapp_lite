import React, { useEffect } from 'React';
import { View, Text } from 'react-native';
import stylesButtons from '../../styles/buttons.css';
import Button from '../components/button';
import { useDispatch, useSelector } from 'react-redux';
import { currentUserActionLogout } from '../../redux/actions/currentUserActions';
import { createModal, isEmpty } from '../../utils/utils';
import { SELECT_LANGUAGE, SELECT_THEME } from '../screenNames';
import { StoreApp } from '../../@types/store';

export default function Setttings() {

    const { isLogged } = useSelector((state: StoreApp) => state.currentUser);
    const dispatch = useDispatch();
    const { words } = useSelector((state: StoreApp) => state.language);
    const { themeName, languageSelected } = useSelector((state: StoreApp) => state.currentUser.data);

    if (!isLogged) return <View />;

    return <View>

        <SettingsOption screenName={SELECT_THEME} title={ words.THEME } selected={themeName} />

        <View style={{ marginBottom: 16 }} />

        <SettingsOption screenName={SELECT_LANGUAGE} title={ words.LANGUAGE } selected={languageSelected} />

        <View style={{ marginBottom: 32 }} />

        <View style={{ paddingHorizontal: 48 }}>
            <Button title='Logout' onPress={() => dispatch(currentUserActionLogout())}>
                <View className={stylesButtons.btn}>
                    <Text className={stylesButtons.btnText}> {words.LOGOUT} </Text>
                </View>
            </Button>
        </View>
    </View>
}

function SettingsOption({screenName, title, selected} : any) {

    const { words } = useSelector((state: StoreApp) => state.language);
    const { themeVariables } = useSelector((state: StoreApp) => state.theme);

    return <Button onPress={() => createModal(screenName, { noSearchBar: screenName === SELECT_THEME }, { topBarTitle: screenName === SELECT_LANGUAGE ? words.SELECT_LANGUAGE : words.SELECT_THEME }, words)}>
        <View style={{ padding: 16, flexDirection: "row", justifyContent: "space-between", backgroundColor: themeVariables.BG_LAYOUT_ACCENT }}>
            <Text style={{ color: themeVariables.TEXT_COLOR, fontWeight: "bold" }}> { title } </Text>
            <Text style={{ color: themeVariables.PRIMARY_COLOR, fontWeight: "bold" }} > {screenName === SELECT_LANGUAGE ? words[`LANG_${(selected || '').toUpperCase()}`] : selected} </Text>
        </View>
    </Button>
}