import React, { useState, useEffect, useReducer } from 'React';
import useBindScreen from '../../hooks/useBindScreen';
import DEFAULT_PREFRENCES from './_preferences';
import MasonryList from "react-native-masonry-list";
import { getPhotosFromUnplash, getPhotosFromUnplashSearch } from '../../../utils/unsplash';
import { viewportWidth, closeModal } from '../../../utils/utils';
import { Subject, from, of, merge } from 'rxjs';
import { exhaustMap, tap, map, switchMap, distinctUntilChanged, takeWhile, delay } from 'rxjs/operators';
import { actionGeneral } from '../../../redux/actions/general';
import { Navigation } from 'react-native-navigation';
import { SET_PREFERENCES, PROFILE_EDIT_SCREEN } from '../../screenNames';
import MasonryCustomComponent from '../../components/MasonryCustomComponent/MasonryCustomComponent';
import { useSelector, useDispatch } from 'react-redux';
import { StoreApp } from '../../../@types/store';
import { userActionUpdate } from '../../../redux/actions/userActions';

const PHOTOS_PER_PAGE = 20;

const getPhotos = async(queryText: string, pageNumber: number) => {
    const queryPhotos = 'tourism,hiking,traveler,couple,beach,summer,winter,sports,wanderlust,paradise';
    const defaultPreferences = 'mountains';//DEFAULT_PREFRENCES.map(p => p.name).join(',');

    try {
        //const json = await getPhotosFromUnplash(queryText, pageNumber, PHOTOS_PER_PAGE);
        const json = await getPhotosFromUnplashSearch(`${queryPhotos},${defaultPreferences}`, pageNumber, PHOTOS_PER_PAGE);
        console.log({ json });
        return json.results.map((p: any, i: number) => {
            //const aspectRadio = p.width / p.height;
            return {
                uri: p.urls.small,
                isSelected: false,
                data: p
                //height: (viewportWidth / 2) / aspectRadio
            }
        })
    } catch (error) {
        console.log('XXXXX Error', error);
        return [];
    }
};

export function imagesReducer(state: any = [], action: actionGeneral): any[] {
    switch (action.type) {
        case 'UPDATE_IMAGES':
            return state.concat(action.payload.images);
        case 'UPDATE_IMAGE':
            const ins = state.findIndex((item: any, index: number) => index === action.payload.index);
            state[ins] = Object.assign(state[ins], { isSelected: !state[ins].isSelected});
            return state;
        default:
            return state;
    }
}

export default function Preferences(props: any) {

    const queryPhotos = 'traveler,couple,beach,summer,winter';
    //const defaultPreferences = DEFAULT_PREFRENCES.map(p => p.name).join(',');
    
    const { data: currentUser, isLogged } = useSelector((state: StoreApp) => state.currentUser);
    const { words } = useSelector((state: StoreApp) => state.language);

    const _pull$: Subject<number> = new Subject();
    const dispatch = useDispatch();

    useBindScreen(this, props);

    const [images, imagesDispatcher] = useReducer(imagesReducer, []);

    useEffect(() => {
        let pageNumber = 1;

        const s = from(getPhotos(queryPhotos, 1))
            .pipe(
                //delay(700),
                tap((photos) => {
                    imagesDispatcher({ type: 'UPDATE_IMAGES', payload: { images: photos } });
                }),
                switchMap(() => _pull$),
                distinctUntilChanged(),
                takeWhile((pageNum) => {
                    return pageNum <= 3;
                }),
                exhaustMap((p => from(getPhotos(queryPhotos, p + 1)))),
                map((photos, i) => {
                    imagesDispatcher({ type: 'UPDATE_IMAGES', payload: { images: photos } });
                    console.log(`page asked ${pageNumber + 1}`, i, viewportWidth);
                    pageNumber += 1;
                    return pageNumber;
                }),
                
            )
            .subscribe();

        this.onEndReached = () => {
            //console.log('On END REACHED');
            _pull$.next(pageNumber + 1);
        };

        return () => {
            s.unsubscribe();
        }
    }, []);

    const savePreferences = () => {
        console.log(images
            .filter(p => p.isSelected));
        
        const _preferences = images
            .filter(p => p.isSelected)
            .map(p => (p.data.tags || []).map((t: any) => t && t.title ? t.title : ''))
            .reduce((prev, cur) => prev.concat(cur), [])
            .filter((p: string, i: number, arr: string[]) => arr.findIndex(_p => _p === p) === i);

        //console.log(_preferences);

        dispatch(userActionUpdate(currentUser.uid, {
            firstLogin: false,
            preferences: _preferences
        }))
    }

    const save = async() => {

        savePreferences();

        return Navigation.push(props.componentId, {
            component: {
                name: PROFILE_EDIT_SCREEN,
                passProps: {
                    preferences: images,
                    cancelButtonId: 'CANCEL_EDIT_PROFILE',
                    doneButtonId: 'DONE_EDIT_PROFILE'
                },
                options: {
                    topBar: {
                        title: {
                            text: words.PROFILE_EDIT_PROFILE
                        },
                        rightButtons: [
                            {
                                id: 'DONE_EDIT_PROFILE',
                                text: words.DONE,
                            }
                        ]
                    }
                }
            }
        });
    };

    useEffect(() => {
        const navigationButtonEventListener = Navigation.events()
            .registerNavigationButtonPressedListener(({ buttonId, componentId }) => {
                if (buttonId === `NEXT_${SET_PREFERENCES}`) {

                    save();

                } else if (buttonId === `CLOSE_${SET_PREFERENCES}`) {
                    savePreferences();
                    closeModal(componentId);
                }
            });
        return () => {
            navigationButtonEventListener.remove();
        }
    });

    return <MasonryList
        images={images}
        containerWidth={viewportWidth}
        completeCustomComponent={(props: any) => <MasonryCustomComponent
            {...props} dispatch={imagesDispatcher} item={images[props.data.index]} />}
        masonryFlatListColProps={{
            onEndReachedThreshold: 10,
            onEndReached: () => {
                if (this.onEndReached) this.onEndReached();
            }
        }}
    />
}