import React, { useState, useEffect } from 'React';
import FastImage from 'react-native-fast-image';
import { View} from 'react-native';

import { AVATAR_DEFAULT_URL } from '../../global/variables';
import Button from './button';
import useSavePicture from '../hooks/useSavePicture';

interface Props {
    editable?: boolean;
    styles: any;
    photoUrl: string;
    //photoUrlFn?: (res: { uri: string, url: string } ) => void;
    onChange?: (v: string) => void
}

export default function Avatar(props: Props) {

    const { photoUrl, lunchOptions } = useSavePicture(props.photoUrl);

    useEffect(() => {
        if (props.onChange) props.onChange(photoUrl);
    }, [photoUrl]);

    const AvatarWrapper = props.editable ? Button : View;

    return <View style={props.styles}>
        <AvatarWrapper onPress={() => props.editable ? lunchOptions() : null}>
            <FastImage
                style={props.styles}
                source={{
                    uri: props.photoUrl || AVATAR_DEFAULT_URL,
                    priority: FastImage.priority.normal,
                }}
                resizeMode={FastImage.resizeMode.cover}
            /> 
        </AvatarWrapper>
    </View>
}
