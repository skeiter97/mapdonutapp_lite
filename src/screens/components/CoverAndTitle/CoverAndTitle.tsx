import React, { useContext, useState, useEffect } from 'react';
import { View, Platform, Alert, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { TextInput } from 'react-native-gesture-handler';
import Button from '../button';
import ActionSheet from 'react-native-action-sheet';
import ImagePicker from 'react-native-image-picker';
import useSavePicture from '../../hooks/useSavePicture';
import FastImage from 'react-native-fast-image';
import styles from  './CoverAndTitle.css';
import { useInput } from '../../../utils/utils';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../../@types/store';

interface Props {
    placeholder: string,
    title: string,
    setTitle: (txt: string) => void,
    coverURL: string,
    setCoverURL: (txt: string) => void,
    path: string
}

export default function CoverTitle({ placeholder, coverURL, title, setTitle, setCoverURL, path } : Props) {

    const textInput = useInput(title);
    const { themeVariables } = useSelector((state: StoreApp) => state.theme);

    const { photoUrl, setPhotoUrl, progress, uploading } = useSavePicture(coverURL);

    const _onPress = () => {

        const BUTTONSiOS = [
            'Take Photo',
            'Choose from Library',
            'Remove Picture',
            'Cancel'
        ];

        const BUTTONSandroid = [
            'Take Photo',
            'Choose from Library',
            'Remove Picture'
        ];

        if (!photoUrl) {
            BUTTONSiOS.splice(2, 1);
            BUTTONSandroid.splice(2, 1);
        }

        ActionSheet.showActionSheetWithOptions({
            options: (Platform.OS == 'ios') ? BUTTONSiOS : BUTTONSandroid,
            destructiveButtonIndex: photoUrl ? 2 : -1,
            cancelButtonIndex: BUTTONSiOS.length - 1
        },
            (buttonIndex) => {
                console.log('button clicked :', buttonIndex);
                const opts = {
                    storageOptions: {
                        skipBackup: true,
                        path: 'images',
                    },
                    noData: true,
                    //allowsEditing: true, // IOS only
                }

                if (buttonIndex === 0 || buttonIndex === 1) ImagePicker[buttonIndex === 0 ? 'launchCamera' : 'launchImageLibrary'](opts, (response) => {
                    if (response.didCancel) {
                        //console.log('User cancelled image picker');
                        //setUploading(false);
                    } else if (response.error) {
                        console.log('ImagePicker Error: ', response.error);
                        Alert.alert(response.error);
                        //setUploading(false);
                    } else {
                        //processPhoto(response.uri);
                        setCoverURL(response.uri);
                        setPhotoUrl(response.uri);
                    }
                });
                else if (buttonIndex === 2) {
                    setCoverURL('');
                    setPhotoUrl('');
                }
            });
    };

    useEffect(() => {
        setTitle(textInput.value);
    }, [textInput.value])

    return <View>

        {photoUrl ? <View>
            <FastImage
            style={{height: 180}}
            source={{
                uri: photoUrl,
                priority: FastImage.priority.high
            }}
            resizeMode={FastImage.resizeMode.cover}
        />
            {uploading ? <View className={styles.avatarLoading}>
                <Text className={styles.avatarLoadingText}> {progress}% </Text>
            </View> : null}
        </View> : null }

        <View style={{ paddingVertical: 16, paddingHorizontal: 16, flexDirection: 'row', backgroundColor: themeVariables.BG_LAYOUT_ACCENT, alignItems: "center" }}>
            <View style={{ flex: 1, paddingRight: 12 }}>
                <TextInput {...textInput} placeholder={placeholder} placeholderTextColor={themeVariables.TEXT_COLOR_PLACEHOLDER} style={{ fontSize: 20, color: themeVariables.TEXT_COLOR }} />
            </View>
            <View style={{}}>
                <Button onPress={_onPress}>
                    <View style={{ padding: 8, borderRadius: 20, height: 40, width: 40, backgroundColor: themeVariables.BG_LAYOUT, justifyContent: "center", alignItems: "center" }}>
                        <Icon name={'camera'} size={20} color={'#000'} style={{ color: themeVariables.TEXT_COLOR }}></Icon>
                    </View>
                </Button>
            </View>
        </View>
    </View>
}