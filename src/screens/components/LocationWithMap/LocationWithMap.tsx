import React, { useRef, useEffect, useState } from 'React';
import { View, TextInput, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../../@types/store';
import MapView, { PROVIDER_GOOGLE, Marker, LatLng } from 'react-native-maps';
import Button from '../button';
import { createModal, createFirebaseGeopoint } from '../../../utils/utils';
import { SELECT_LOCATION } from '../../screenNames';
import { Mapdonut_inputHook } from '../../../@types/inputs';
import { useGeolocation } from '../../hooks/useGeolocation';
import { Geopoint } from '../../../@types/general';
import { ItemSearchBarList } from '../../../@types/searchBarList';
import RNGooglePlaces from 'react-native-google-places';

interface props {
    locationText: Mapdonut_inputHook,
    locationId: Mapdonut_inputHook,
    locationGeopoint: Geopoint,
    setLocationGeopoint: (a: Geopoint) => void
}

export default function LocationWithMap({ locationText, locationId, locationGeopoint, setLocationGeopoint }: props) {

    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    //const currentUser = useSelector((state: StoreApp) => state.currentUser.data);
    const [point, setPoint] = useState({} as LatLng);
    const { words } = useSelector((state: StoreApp) => state.language);

    const mapRef = useRef<MapView>(null);

    const geolocationHelpers = useGeolocation();

    const moveMap = (lat: number, lng: number) => {
        if (!mapRef.current) return;
        mapRef.current.animateCamera({
            center: {
                latitude: lat,
                longitude: lng,
            },
            zoom: 14
        }, { duration: 100 });
    }

    const setLocationData = (place_id: string, formatted_address: string) => {
        locationId.setValue(place_id);
        locationText.setValue(formatted_address || '');
    } 

    const setCurrentLocation = async () => {
        const {lat, lng} = await geolocationHelpers.getCurrentLocation();
        moveMap(lat, lng);
        setPoint({
            latitude: lat,
            longitude: lng
        });
        const res = await geolocationHelpers.getCurrentPlaceGooglePlaces(lat, lng);
        if (res) setLocationData(res.place_id, res.formatted_address);
        setLocationGeopoint(createFirebaseGeopoint(lat, lng));
    };

    useEffect(() => {
        if (!mapRef.current) return;
        //if (currentUser.allowLocation)
        geolocationHelpers.getCurrentLocation().then(({ lat, lng }) => moveMap(lat, lng));
    }, [mapRef.current]);

    const selectLocation = async(item: ItemSearchBarList) => {
        const res = await RNGooglePlaces.lookUpPlaceByID(item.id); //geolocationHelpers.getCurrentPlaceGooglePlaces(lat, lng);
        if (res) setLocationData(item.id, `${item.title}, ${res.address}`);
        //console.log(item, res);
        setLocationGeopoint(createFirebaseGeopoint(res.location.latitude, res.location.longitude));
    };

    return <View>

        <View style={{flexDirection: "row", marginBottom: 16, justifyContent: "space-between", alignItems: "center"}}>

            <View style={{ flexDirection: "row", flex: 1 }}>

                <Button onPress={() => createModal(SELECT_LOCATION, { selectLocation }, { topBarTitle: words.SET_LOCATION }, words)}>
                    <View style={{ flexDirection: "row", flex: 1, alignItems: "center" }}>
                        <Icon
                            name={'search'}
                            color={themeVariables.TEXT_COLOR}
                            size={16}
                        />
                        <Text style={{
                            color: locationText.value.length > 0 ? themeVariables.TEXT_COLOR : themeVariables.TEXT_COLOR_PLACEHOLDER,
                            paddingHorizontal: 16
                        }}>
                            {locationText.value.length > 0 ? locationText.value : locationText.placeholder}
                        </Text>
                    </View>
                </Button>
            </View>
            <View style={{ flexDirection: "row", width: 24 }}>
                <Button onPress={setCurrentLocation}>
                    <Icon
                        name={'location-arrow'}
                        color={themeVariables.TEXT_COLOR}
                        size={16}
                    />
                </Button>
            </View>
        </View>

        <MapView
            provider={PROVIDER_GOOGLE}
            style={{ height: 140, borderRadius: 16 }}
            ref={mapRef}
            zoomEnabled={false}
            zoomTapEnabled={false}
            scrollEnabled={false}
            pitchEnabled={false}
        >
            {point.latitude && point.longitude ? <Marker coordinate={point} >
                <View style={{ bottom: -16 }}>
                    <Icon name={'map-pin'} color={themeVariables.MARK_PIN_COLOR} size={32}> </Icon>
                </View>
            </Marker> : null}
        </MapView>
    </View>
}