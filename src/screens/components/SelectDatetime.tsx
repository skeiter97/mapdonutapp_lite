import React, { useState } from 'react';
import { View } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import useBindScreen from '../hooks/useBindScreen';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../@types/store';

export default function SelectDatetime(props: any) {

    const { lang } = useSelector((state: StoreApp) => state.language);
    const [date, setDate] = useState(props.datetime ? props.datetime : new Date);
    const [time, setTime] = useState(props.datetime ? props.datetime : new Date);
    
    useBindScreen(this, props, {
        date, time
    });

    const _date =  <DateTimePicker
        minimumDate={new Date()}
        value={date}
        mode={'date'}
        is24Hour={false}
        display="default"
        locale={lang}
        onChange={(ev: any, date: Date) => setDate(date)} />;

    const _time = <DateTimePicker
        locale={lang}
        value={time}
        mode={'time'}
        is24Hour={false}
        display="default"
        onChange={(ev: any, date: Date) => setTime(date)} />;

    return <View>
        <View style={{ marginTop: 32 }}></View>
        {_date}
        <View style={{ marginTop: 32 }}></View>
        {_time}
    </View>
}