import React from 'react';
import { View, Text, ScrollView, FlatList  } from 'react-native';
import { } from 'react-native-gesture-handler';
import styles from './SearchBarList.css';
import Button from '../button';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../../@types/store';
import { ItemSearchBarList } from '../../../@types/searchBarList';

interface Props {
    items: ItemSearchBarList[],
    selected?: string,
    prevItems?: ItemSearchBarList[],
    selectItem: (item: ItemSearchBarList) => void
}

export default function SearchBarList({ items, selectItem, prevItems, selected }: Props) {

    const { themeVariables } = useSelector((state: StoreApp) => state.theme);

    const Item = ({ item }: { item: ItemSearchBarList }) => <Button onPress={() => selectItem(item)}>
        <View className={styles.listItem}>

            <View style={{flexDirection: "row", justifyContent: "space-between", width: '100%'}}>
                {item.icon ? <View className={styles.listItemIconWrapper} style={{ backgroundColor: themeVariables.BG_LAYOUT }}>
                    <Icon name={item.icon} size={20} color={selected === item.id ? themeVariables.PRIMARY_COLOR  : themeVariables.TEXT_COLOR_PLACEHOLDER } /></View> :
                    null
                }
                <View style={{ marginLeft: 12, alignContent: "flex-start", justifyContent: "center", flex: 1 }}>
                    <Text style={{ color: selected === item.id ? themeVariables.PRIMARY_COLOR : themeVariables.TEXT_COLOR }}> {item.title} </Text>
                    {item.subtitle ? <Text style={{ fontSize: 14, color: selected === item.id ? themeVariables.PRIMARY_COLOR : themeVariables.TEXT_COLOR }}> {item.subtitle} </Text> : null}
                </View>
                
            </View>
        </View>
    </Button>;

    return <ScrollView>

        {prevItems ? <FlatList
            data={prevItems}
            keyExtractor={(item) => item.id}
            initialNumToRender={5}
            renderItem={({ item }) => <Item item={item} />}
        /> : null}

        <FlatList
            style={{ backgroundColor: themeVariables.BG_LAYOUT_ACCENT }}
            data={items}
            keyExtractor={(item) => item.id}
            initialNumToRender={15}
            renderItem={({ item }) => <Item item={item} />}
        />
    </ScrollView>
}
