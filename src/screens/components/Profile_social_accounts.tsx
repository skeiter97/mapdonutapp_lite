import React, { useContext, useState } from 'react';
import { View, TextInput, Text } from 'react-native';
import { useInput, saveExtraDataUser } from '../../utils/utils';
import Button from './button';
import firebase from 'react-native-firebase';
import { Mapdonut_Social } from '../../@types/user';

import ActionSheet from 'react-native-action-sheet';
import { Platform } from 'react-native';
import DropdownIcon from './dropdownIcons/DropdownIcon';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../@types/store';

interface Props {
    saveItem: (item: Mapdonut_Social) => void;
    item?: Mapdonut_Social
}

export function ProfileSocialAccountItem(props: Props) {

    const url = useInput(props.item ? props.item.url : '');
    const provider = useInput(props.item ? props.item.provider : '');
    const username = useInput(props.item ? props.item.username : '');
    const privacy = useInput(props.item ? props.item.privacy : '');
    const { words } = useSelector((state: StoreApp) => state.language);

    var BUTTONSiOS = [
        'Option 0',
        'Option 1',
        'Option 2',
        'Delete',
        'Cancel'
    ];

    var BUTTONSandroid = [
        'Option 0',
        'Option 1',
        'Option 2'
    ];

    var DESTRUCTIVE_INDEX = 3;
    var CANCEL_INDEX = 4;

    /**
     * 
     * 
     * <View>
            <Button onPress={() => {
                const ac = ActionSheet.showActionSheetWithOptions({
                    options: (Platform.OS == 'ios') ? BUTTONSiOS : BUTTONSandroid,
                    cancelButtonIndex: CANCEL_INDEX,
                    destructiveButtonIndex: DESTRUCTIVE_INDEX,
                    tintColor: 'blue'
                },
                    (buttonIndex) => {
                        console.log('button clicked :', buttonIndex);

                    });
            }}>
                <TextInput {...provider} editable={false} placeholder="provider" ></TextInput>
            </Button>
        </View>
     */

    return <View style={{flexDirection: "row", justifyContent: "center", alignItems: "center" }}>

        <View>
            <TextInput {...username} placeholder="Username" ></TextInput>
        </View>

        <DropdownIcon title="Provider" setValue={provider.setValue} options={[
            { title: 'Facebook', icon: 'facebook', value: "facebook" },
            { title: 'Instagram', icon: 'instagram', value: "instagram" },
            { title: 'Couchsurfing', icon: 'couch', value: "cs" },
            { title: 'Airbnb', icon: 'airbnb', value: "airbnb" },
        ]} />

        <DropdownIcon title="Privacy" setValue={privacy.setValue} options={[
            { title: 'Private', icon: 'user-lock', subtitle: words.PRIVACY_PRIVATE, value: "private" },
            { title: 'Followers', icon: 'user-shield', subtitle: words.PRIVACY_FOLLOWERS, value: "followers" },
            { title: 'Public', icon: 'users', subtitle: words.PRIVACY_PUBLIC, value: "public" },
        ]} />

        <Button title="Save" onPress={() => props.saveItem({
            privacy: privacy.value,
            username: username.value,
            url: url.value,
            provider: provider.value
        })}>
        </Button>
    </View>
}

interface Props_SociaProfileSocialAccounts {
    userId: string,
    socials: any //Mapdonut_Social[] | RNFirebase.firestore.FieldValue
}

export function SociaProfileSocialAccounts(props: Props_SociaProfileSocialAccounts) {

    if (!props.socials) return <View />;

    const [showTmp, setShowTmp] = useState((props.socials || []).length === 0);

    const _saveItem = async (item: Mapdonut_Social) => {
        try {
            await saveExtraDataUser(props.userId, { socials: firebase.firestore.FieldValue.arrayUnion(item) });
            if (showTmp) setShowTmp(false);
        } catch (error) {
            console.log({error});
        }
    };

    const socials = (props.socials || []).map((s, i: number) => <ProfileSocialAccountItem key={i} saveItem={_saveItem} />);

    return <View>
        
        { socials }

        {showTmp ? <ProfileSocialAccountItem saveItem={_saveItem} /> : <View />}

        <Button disabled={!showTmp} onPress={() => {
            setShowTmp(true);
        }}>
            <Text> Add A Social Network </Text>
        </Button>
    </View>;
}