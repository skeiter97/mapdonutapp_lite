import React, { useState, useEffect } from 'React';
import { View, Text, FlatList, Image } from 'react-native';
import CameraRoll from "@react-native-community/cameraroll";
import { viewportWidth, viewportHeight } from '../../../utils/utils';
import Cover from '../cover/cover';
import Button from '../button';
import { Navigation } from 'react-native-navigation';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../../@types/store';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { imageSelected } from '../../../@types/camera';
import ButtonInCameraViewScreen from '../SelectPicture/ButtonInCameraViewScreen';
import { MAPDONUT_CAMERA_OVERLAY } from '../../screenNames';
import { getBottomSpace, getStatusBarHeight } from 'react-native-iphone-x-helper';
import ModalSimple from '../ModalSimple/ModalSimple';

const MAX_PHOTOS_PER_TRIP = 10;

interface PickPhotoLibraryProps {
    componentId: 'string',
    currentScreen: number,
    photosCameraRoll: imageSelected[],
    setPhotosCameraRoll: (a: imageSelected[]) => void
}

export default function PickPhotoLibrary(props: PickPhotoLibraryProps) {

    const { parseTranslation } = useSelector((state: StoreApp) => state.language);
    const { themeVariables } = useSelector((state: StoreApp) => state.theme);

    const cameraRoll = useSelector((state: StoreApp) => state.ui.cameraRoll);
    const h = useSelector((state: StoreApp) => state.ui.rnHeights.topBarHeight);

    const [photos, setPhotos] = useState(cameraRoll.photos);
    const [pageInfo, setPageInfo] = useState(cameraRoll.cursor);
    const [typeSelection, setTypeSelection] = useState('simple');
    const { words } = useSelector((state: StoreApp) => state.language);
    const [showAlert, setShowAlert] = useState(false);

    const parsePhotos = (items: CameraRoll.PhotoIdentifier[]): imageSelected[]  => items.map(p => ({
        uri: p.node.image.uri
    }));

    const getPhotos = async () => {
        const _photos = await CameraRoll.getPhotos({
            first: 9,
            after: pageInfo.end_cursor
        });
        //if (isEmpty(pageInfo) && _photos.edges.length > 0) props.setPhotosCameraRoll(parsePhotos([_photos.edges[0]]));
        setPhotos(photos.concat(_photos.edges));
        setPageInfo(_photos.page_info);
        return _photos;
    };

    useEffect(() => {
        if (props.currentScreen === 0) Navigation.mergeOptions(props.componentId, {
            topBar: {
                title: {
                    text: words.CAMERA_ROLL
                },
                leftButtons: [
                    {
                        id: `CANCEL_${MAPDONUT_CAMERA_OVERLAY}`,
                        text: words.CANCEL
                    }
                ],
                rightButtons: [
                    {
                        id: `NEXT_${MAPDONUT_CAMERA_OVERLAY}`,
                        text: words.NEXT,
                    }
                ]
            },
        });
    }, [props.currentScreen]);

    const _onSelect = (item: CameraRoll.PhotoIdentifier) => {
        if (typeSelection === 'multi') {
            const isSelected = props.photosCameraRoll.find(p => p.uri === item.node.image.uri);
            const newPhotosSelected = !!isSelected ?
                props.photosCameraRoll.filter(p => p.uri !== isSelected.uri) :
                props.photosCameraRoll.concat(parsePhotos([item]));
            if (newPhotosSelected.length > MAX_PHOTOS_PER_TRIP) return setShowAlert(true);
            else if (newPhotosSelected.length > 0) props.setPhotosCameraRoll(newPhotosSelected);
            setShowAlert(false);
        } else {
            props.setPhotosCameraRoll(parsePhotos([item]));
        }
    }

    return <View>
        <PhotoSelected uri={props.photosCameraRoll.length === 0 ?
                (photos.length > 0 ? photos[0].node.image.uri : '') :
                props.photosCameraRoll[props.photosCameraRoll.length - 1].uri
            }
            typeSelection={typeSelection}
            setTypeSelection={setTypeSelection}
        />
        <FlatList
            data={photos}
            renderItem={({ item }) => <Photo
                item={item}
                onSelect={_onSelect}
                typeSelection={typeSelection}
                photosSelected={props.photosCameraRoll}
            />}
            keyExtractor={(item, i) => i + ''}
            initialNumToRender={9}
            numColumns={3}
            style={{
                height: viewportHeight - viewportWidth - getBottomSpace() - getStatusBarHeight() - h - 48
            }}
            onEndReached={() => {
                getPhotos();
            }}
        />
        <ModalSimple isVisible={showAlert}>
            <Text style={{
                color: themeVariables.TEXT_COLOR,
                textAlign: "center"
            }}> {parseTranslation('MAX_PICS_TRIP', { limit: '' + MAX_PHOTOS_PER_TRIP })}  </Text>
        </ModalSimple>
    </View>
}

interface PropsPhoto {
    item: CameraRoll.PhotoIdentifier,
    photosSelected: imageSelected[],
    onSelect: (item: CameraRoll.PhotoIdentifier) => void,
    typeSelection: string
}

function Photo({ item, onSelect, photosSelected, typeSelection }: PropsPhoto) {

    const w = viewportWidth / 3;
    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    const isSelectedIndex = photosSelected.findIndex(p => p.uri === item.node.image.uri);

    return <View style={{
        width: w,
        height: w
    }}>

        <Button onPress={() => onSelect(item) }>
            <Image
                style={{
                    width: w,
                    height: w,
                }}
                source={{ uri: item.node.image.uri }}
            />
            {
                isSelectedIndex !== -1 && isSelectedIndex + 1 === photosSelected.length ?
                    <View style={{
                        position: "absolute",
                        right: 0,
                        top: 0,
                        width: '100%',
                        height: '100%',
                        backgroundColor: 'rgba(255, 255, 255, 0.6)',
                        zIndex: 10
                    }} />
                    : null
            }

            {
                typeSelection === 'multi' ? <View style={{
                    position: "absolute",
                    right: 6,
                    top: 6,
                    width: 24,
                    height: 24,
                    borderRadius: 24,
                    backgroundColor: isSelectedIndex !== -1 ?
                        themeVariables.PRIMARY_COLOR :
                        'rgba(255, 255, 255, 0.4)',
                    zIndex: 20,
                    justifyContent: "center",
                    alignItems: "center",
                    borderWidth: 1,
                    borderColor: themeVariables.LIBRARY_PHOTO_SELECTED_BORDER
                }}>
                    <Text style={{
                        color: themeVariables.PRIMARY_COLOR_TEXT,
                        fontSize: 12
                    }} > {isSelectedIndex !== -1 ? isSelectedIndex + 1 : ''} </Text>
                </View> :
                null
            }
        </Button>
    </View>
}

function PhotoSelected({ uri, typeSelection, setTypeSelection }: any) {
    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    return <View>
        <Cover coverURL={uri} height={viewportWidth} nativeImage={true} />
        <View style={{
            position: "absolute",
            bottom: 0,
            left: 0,
            width: '100%',
            flexDirection: "row",
            justifyContent: "space-between",
            padding: 16
        }}>
            <View />
            <View>
                <Button onPress={() => setTypeSelection(typeSelection === 'multi' ? 'simple' : 'multi')}>
                    <ButtonInCameraViewScreen isSelected={typeSelection === 'multi'} >
                        <Icon
                            name={'clone'}
                            color={typeSelection === 'multi' ? themeVariables.PRIMARY_COLOR_TEXT : '#fff'}
                            size={16}
                        />
                    </ButtonInCameraViewScreen>
                </Button>
            </View>
        </View>
    </View>
}
