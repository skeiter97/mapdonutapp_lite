import React, { useEffect, useState } from 'react';
import SearchBarList from './SearchBarList/SearchBarList';
import { THEMES } from '../../global/variables';
import { useSelector, useDispatch } from 'react-redux';
import { StoreApp } from '../../@types/store';
import { closeModal } from '../../utils/utils';
import useBindScreen from '../hooks/useBindScreen';
import { themeActionChange } from '../../redux/reducers/themeReducer';

export default function SelectTheme(props: any) {

    //const { theme } = useSelector((state: StoreApp) => state.theme);
    const { languageSelected } = useSelector((state: StoreApp) => state.currentUser.data);

    const dispatch = useDispatch();
    const initialItems = THEMES.map(t => ({ title: t, id: t }));
    const [items, setItems] = useState(initialItems);

    useBindScreen(this, props);

    return <SearchBarList
        items={items}
        selected={languageSelected}
        selectItem={(item) => {
            dispatch(themeActionChange(item.id));
            closeModal(props.componentId);
        }}
    />
}
