import React, { useState, useEffect, useRef } from 'React';
import { Animated } from 'react-native';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../../@types/store';

interface ModalSimpleProps {
    isVisible: boolean,
    children: any
}

export default function ModalSimple({ isVisible, children }: ModalSimpleProps) {

    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    const [h, setH] = useState(60);

    const translateY = useRef(new Animated.Value(h)).current;
    const opacity = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        Animated.parallel([
            Animated.timing(
                translateY,
                {
                    toValue: isVisible ? -14 : h,
                    duration: 180,
                    useNativeDriver: true,
                }
            ),
            Animated.timing(
                opacity,
                {
                    toValue: isVisible ? 1 : 0,
                    duration: 100,
                    useNativeDriver: true,
                }
            )
        ])
            .start();
    }, [isVisible, h]);

    return <Animated.View
        onLayout={(e: any) => {
            setH(e.nativeEvent.layout.height);
        }}
        style={{
            position: 'absolute',
            backgroundColor: themeVariables.BG_LAYOUT_ACCENT,
            padding: 16,
            bottom: 0,
            width: '100%',
            opacity,
            transform: [{
                translateY: translateY
            }]
        }}
    >
        {children}
    </Animated.View>
}