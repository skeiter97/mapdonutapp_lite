import React, { useEffect, useState } from 'react';
import SearchBarList from './SearchBarList/SearchBarList';
import { LANGUAGES } from '../../global/variables';
import { useSelector, useDispatch } from 'react-redux';
import { StoreApp } from '../../@types/store';
import { closeModal, simpleSearchOnList } from '../../utils/utils';
import { languageActionChange } from '../../redux/reducers/languageReducer';
import useBindScreen from '../hooks/useBindScreen';

export default function SelectLanguage(props: any) {

    const { words } = useSelector((state: StoreApp) => state.language);
    const { lang } = useSelector((state: StoreApp) => state.language);

    const dispatch = useDispatch();
    const initialItems = LANGUAGES.map(l => ({
        title: words[`LANG_${l}`.toUpperCase()],
        id: l,
    }));
    const [items, setItems] = useState(initialItems);

    useBindScreen(this, props);

    useEffect(() => {
        this.searchBarUpdated = ({ text }: any) => {
            const itemsFiltered = simpleSearchOnList(text, items, initialItems);
            setItems(itemsFiltered);
        }
    }, []);

    return <SearchBarList
        items={items}
        selected={lang}
        selectItem={(item) => {
            closeModal(props.componentId);
            dispatch(languageActionChange(item.id));
        }}
    />
}
