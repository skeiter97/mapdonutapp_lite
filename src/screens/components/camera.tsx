import React, { useEffect, useState } from 'react';
import { CameraKitCameraScreen } from 'react-native-camera-kit';
import { View, Text } from 'react-native';
import { viewportWidth } from '../../utils/utils';
import Button from './button';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../@types/store';
import Icon from 'react-native-dynamic-vector-icons';
import FastImage from 'react-native-fast-image';
import { Navigation } from 'react-native-navigation';
import { MAPDONUT_CAMERA_OVERLAY } from '../screenNames';

//import { RNCamera, FaceDetector } from 'react-native-camera';

const WIDTH_CAMERA_BUTTON = 72;

interface CameraScreen {
    componentId: 'string',
    currentScreen: number,
    photoCamera: string,
    setPhotoCamera: any
}

export default function CameraScreen(props: CameraScreen) {

    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    const [cameraRef, setCameraRef] = useState(null as any);
    const { words } = useSelector((state: StoreApp) => state.language);

    useEffect(() => {
        if (props.currentScreen === 1) Navigation.mergeOptions(props.componentId, {
            topBar: {
                title: {
                    text: words.PHOTO
                },
                leftButtons: [
                    props.photoCamera ?
                        {
                            id: `BACK_${MAPDONUT_CAMERA_OVERLAY}`,
                            text: words.BACK,
                        } :
                        {
                            id: `CANCEL_${MAPDONUT_CAMERA_OVERLAY}`,
                            text: words.CANCEL,
                        }
                ],
                rightButtons: props.photoCamera ? [
                    {
                        id: `NEXT_${MAPDONUT_CAMERA_OVERLAY}`,
                        text: words.NEXT,
                    }
                ] : []
            },
        });
    }, [props.currentScreen, props.photoCamera]);

    return <View style={{flex: 1}}>
        <View style={{ height: viewportWidth }}>
            {!props.photoCamera ? <CameraKitCameraScreen
                ref={(cam: any) => setCameraRef(cam)}
                style={{
                    height: viewportWidth,
                    backgroundColor: themeVariables.BG_LAYOUT_ACCENT
                }}
                showFrame={false}
                hideControls={true}
                cameraOptions={{
                    flashMode: 'auto',             // on/off/auto(default)
                    focusMode: 'on',               // off/on(default)
                    zoomMode: 'on',                // off/on(default)
                    ratioOverlay: '1:1',           // optional, ratio overlay on the camera and crop the image seamlessly
                    ratioOverlayColor: '#00000000' // optional
                }}
            /> : <FastImage
                    style={{
                        height: viewportWidth,
                    }}
                    source={{
                        uri: props.photoCamera,
                        priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.cover}
                />}
        </View>
        <View style={{ flex: 1 }}>
            <CameraHandlers cameraRef={cameraRef} photo={props.photoCamera} onChange={(p: string) => {
                props.setPhotoCamera(p)
            }} />
        </View>
    </View>
}

function CameraHandlers({ cameraRef, onChange, photo } : any) {

    const { themeVariables } = useSelector((state: StoreApp) => state.theme);

    const FLASH_MODES = ['auto', 'on', 'off'];
    const CAMERA_MODES = ['front', 'rear'];

    const [flashMode, setFlashMode] = useState(0);
    const [cameraMode, setCameraMode] = useState(0);

    useEffect(() => {
        if (cameraRef) cameraRef.camera.changeCamera(CAMERA_MODES[cameraMode]);
    }, [cameraMode]);

    useEffect(() => {
        if (cameraRef) cameraRef.camera.setFlashMode(FLASH_MODES[flashMode]);
    }, [flashMode]);

    if (!cameraRef || photo) return <View />;

    return <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>

        <View style={{
            position: "absolute",
            top: -56 - 4,
            width: '100%',
            flexDirection: "row",
            justifyContent: "space-between",
            paddingHorizontal: 16,
            zIndex: 10
        }}>
            <Button onPress={() => {
                setCameraMode(cameraMode === 0 ? 1 : 0);
            }}>
                <View style={{
                    flexDirection: "row",
                    width: 56,
                    height: 56,
                    justifyContent: "center",
                    alignItems: "center"
                }}>
                    <Icon
                        name={'refresh'}
                        color={'#fff'}
                        type="EvilIcons"
                        size={34}
                    />
                </View>
                
            </Button>
            <Button onPress={() => {
                setFlashMode(flashMode + 1 <= 2 ? flashMode + 1 : 0)
            }}>
                <View style={{
                    flexDirection: "row",
                    width: 56,
                    height: 56,
                    justifyContent: "center",
                    alignItems: "center"
                }}>
                    <Text style={{color: '#fff', position: 'absolute', top: 20, left: 10}}>
                        {flashMode === 0 ? 'A' : ''}
                    </Text>
                    <Icon
                        name={flashMode === 2 ? 'ios-flash-off' : 'ios-flash'}
                        type="Ionicons"
                        color={'#fff'}
                        size={24}
                    />
                </View>
            </Button>
        </View>

        <Button onPress={async () => {
            const image = await cameraRef.camera.capture(true);
            onChange(image.uri);
        }}>
            <View style={{
                width: WIDTH_CAMERA_BUTTON,
                height: WIDTH_CAMERA_BUTTON,
                borderRadius: WIDTH_CAMERA_BUTTON,
                backgroundColor: themeVariables.TEXT_COLOR_PLACEHOLDER,
                alignItems: "center",
                justifyContent: "center"
            }}>
                <View style={{
                    width: WIDTH_CAMERA_BUTTON * 0.7,
                    height: WIDTH_CAMERA_BUTTON * 0.7,
                    borderRadius: WIDTH_CAMERA_BUTTON * 0.7,
                    backgroundColor: themeVariables.BG_LAYOUT
                }}>
                </View>
            </View>
        </Button>
    </View>
}