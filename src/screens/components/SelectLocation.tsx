import React, { useState, useEffect } from 'react';
import SearchBarList, { ItemSearchBarList } from './SearchBarList/SearchBarList';
import { closeModal } from '../../utils/utils';
import useBindScreen from '../hooks/useBindScreen';
import RNGooglePlaces, { GMSTypes } from 'react-native-google-places';
import { Subject, from, of, merge } from 'rxjs';
import { debounceTime, tap, switchMap, map, distinctUntilKeyChanged } from 'rxjs/operators';

interface Props {
    setPrevItems?: () => Promise<ItemSearchBarList[]>,
    selected?: string,
    [prop: string]: any;
}

export default function SelectLocation(props: Props) {

    const initialItems = (items: GMSTypes.AutocompletePrediction[]): ItemSearchBarList[] => items.map(item => ({
        title: item.primaryText,
        subtitle: item.secondaryText,
        id: item.placeID,
        icon: 'map-marker-alt'
    }));

    const [items, setItems] = useState(initialItems([]));
    const search$: Subject<string> = new Subject();
    const close$: Subject<boolean> = new Subject();

    useBindScreen(this, props);

    const firstLoad$ = from(RNGooglePlaces.getCurrentPlace())
        .pipe(
            map(res => res.map(r => ({
                title: r.name,
                subtitle: r.address,
                id: r.placeID,
                icon: 'map-marker-alt'
            })) as ItemSearchBarList[])
        );

    const _search$ = (props.setPrevItems ? from(props.setPrevItems()) : firstLoad$).pipe(
        tap((locations) => setItems(locations)),
        switchMap(() => search$, (initials, text) => ({initials, text})),
        distinctUntilKeyChanged('text'),
        debounceTime(400),
        switchMap(({ initials, text }) => {
            return (text || '').length > 0 ?
                from(RNGooglePlaces.getAutocompletePredictions(text, props.gpOptions ? props.gpOptions : {} )).pipe(map(locations => initialItems(locations))) :
                of(initials)
        }),
        tap((locations) => setItems(locations))
    );

    const _close$ = close$.pipe(tap(r => {
        closeModal(props.componentId);
        closeModal(props.componentId); //Necesaary to close modalmeanwhile the keyboard is open
    }))

    useEffect(() => {
        const s = merge(_search$, _close$).subscribe();
        this.searchBarUpdated = ({ text }: any) => search$.next(text);
        this.close = (item: any) => {
            if (props.selectLocation) props.selectLocation(item);
            close$.next(true);
        }
        return () => s.unsubscribe();
    }, []);

    return <SearchBarList
        items={items}
        selected={props.selected}
        selectItem={this.close}
    />
}
