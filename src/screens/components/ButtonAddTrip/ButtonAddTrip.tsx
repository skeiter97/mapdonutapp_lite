import React from 'React';
import { View, Alert } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { StoreApp } from '../../../@types/store';
import { createModal } from '../../../utils/utils';
import Button from '../button';
import { CREATE_TRIP } from '../../screenNames';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Mapdonut_Trip_pre } from '../../../@types/trip';
import { tripsActionAdd } from '../../../redux/actions/tripsActions';

export default function ButtonAddTrip(props: any) {

    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    const { words } = useSelector((state: StoreApp) => state.language);

    const dispatch = useDispatch();

    const done = ({ item }: { item: Mapdonut_Trip_pre }) => {
        dispatch(tripsActionAdd(item))
    }

    const preDone = ({ item }: { item: Mapdonut_Trip_pre }) => {
        if (!item.locationId) {
            Alert.alert('Please register location'); //Todo
            return Promise.reject({ code: '', message: 'Please register location' });
        } else if (item.photosToUpload.length === 0) {
            Alert.alert('Please add least upload an image'); //Todo
            return Promise.reject({ code: '', message: 'Please add least upload an image' });
        }
    }

    return <Button onPress={() => {
        createModal(CREATE_TRIP, { ...props, noSearchBar: true, done, preDone }, { topBarTitle: words.CREATE_TRIP }, words);
    }}>
        <View style={{
            backgroundColor: themeVariables.PRIMARY_COLOR,
            width: 56,
            height: 56,
            borderRadius: 56,
            alignItems: "center",
            justifyContent: "center"
        }}>
            <Icon name={'plus'} color={themeVariables.PRIMARY_COLOR_TEXT} size={26} />
        </View>
    </Button>
}