import React, { useContext, useState, useRef } from 'react';

interface OptionItem {
    title: string;
    value: string;
    icon?: string;
    subtitle?: string;
}

interface Props {
    options: OptionItem[];
    title: string;
    mainIcon?: string;
    setValue: any;
};

export default function DropdownIcon(props: Props) {

}