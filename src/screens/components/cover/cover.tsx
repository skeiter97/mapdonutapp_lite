import React, { useEffect } from 'react';
import { View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import styles from './cover.css';
import Button from '../button';
import useSavePicture from '../../hooks/useSavePicture';
import FastImage from 'react-native-fast-image';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../../@types/store';

interface Props {
    coverURL: string,
    path?: string,
    height?: number,
    coverURLDefault?: string
    editable?: boolean,
    onChange?: (v: string) => void,
    nativeImage?: boolean
}

export default function Cover(props: Props) {

    const { photoUrl, lunchOptions } = useSavePicture(props.coverURL);

    const { themeVariables } = useSelector((state: StoreApp) => state.theme)

    const h = props.height || 120;

    useEffect(() => {
        if (props.onChange) props.onChange(photoUrl);
    }, [photoUrl]);

    return <View className={styles.coverWrapper} style={{ height: h, backgroundColor: themeVariables.BG_LAYOUT_ACCENT }}>
        
        {props.editable ? <View className={styles.coverWrapperEditIcon}>
            <Button onPress={lunchOptions}>
                <View className={styles.coverWrapperEditIconAction}>
                    <Icon name={'edit'} size={16} color={themeVariables.TEXT_COLOR} />
                </View>
            </Button>
        </View> : null}

        {
            !props.coverURLDefault && !props.coverURL ?
                <View style={{
                    height: h
                }} /> :
            props.nativeImage ?
                <Image
                    style={{ width: '100%', height: h }}
                    source={{ uri: props.coverURL || props.coverURLDefault }}
                /> :
                <FastImage
                    style={{ width: '100%', height: h }}
                    source={{
                        uri: props.coverURL || props.coverURLDefault,
                        priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.cover}
                />
        } 

    </View>
}
