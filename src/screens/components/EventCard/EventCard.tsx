import React, { useContext } from 'react';
import { Mapdonut_Event } from '../../../@types/event';
import { View, Text, Image } from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from './EventCard.css';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { format } from 'date-fns';
import Button from '../button';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../../@types/store';
import { formateDate, getDateFnsLocale } from '../../../utils/utils';

interface Props {
    item: Mapdonut_Event,
    onPress?: () => void
}

export default function EventCard({item, onPress} : Props) {

    const { lang } = useSelector((state: StoreApp) => state.language);
    const { theme, themeVariables } = useSelector((state: StoreApp) => state.theme);
    const HEIGHT_COVER = 120;
    
    return <Button onPress={onPress ? onPress : () => {}}>
        <View className={styles.cardWrapper}>
            <View className={styles.card} style={{
                shadowOffset: { width: 0, height: 2 },
                shadowColor: 'black',
                shadowOpacity: 0.3,
                shadowRadius: 2
            }}>
                {item.photoURL ? <FastImage
                    style={{ width: '100%', height: HEIGHT_COVER }}
                    source={{
                        uri: item.photoURL,
                        priority: FastImage.priority.normal,
                    }}
                    fallback={true}
                    resizeMode={FastImage.resizeMode.cover}
                /> : <View
                        className={styles.eventCoverNoPhoto}
                        style={{ width: '100%', height: HEIGHT_COVER, backgroundColor: themeVariables.COVER_BG  }}
                >
                        <Icon name={'calendar'} size={62} color={themeVariables.TEXT_COLOR} />
                        <View className={styles.eventCoverNoPhotoFloat}>
                            <Text style={{color: themeVariables.TEXT_COLOR}}> {format(item.datetime.toDate(), 'd', {locale: getDateFnsLocale(lang)})} </Text>
                            <Text style={{color: themeVariables.TEXT_COLOR}}> {format(item.datetime.toDate(), 'MMM', {locale: getDateFnsLocale(lang)})} </Text>
                        </View>
                    </View>}
                <View className={styles.cardContent}>
                    <Text> {item.title} </Text>
                    <View>
                        <Text> {formateDate(item.datetime.toDate(), lang) } </Text>
                    </View>
                </View>
            </View>
        </View>
    </Button>;
}
