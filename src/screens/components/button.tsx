import React from 'React';
import { Platform, GestureResponderEvent } from 'react-native';
import {
    TouchableOpacity, TouchableNativeFeedback,
} from 'react-native-gesture-handler'

interface props {
    disabled?: boolean;
    onPress?: (e: GestureResponderEvent) => void,
    children: any
}

export default function Button(props: props) {
    return Platform.OS === 'ios' ?  <TouchableOpacity {...props} >
        {props.children}
    </TouchableOpacity> : <TouchableNativeFeedback {...props}>
        {props.children}
    </TouchableNativeFeedback>
}
