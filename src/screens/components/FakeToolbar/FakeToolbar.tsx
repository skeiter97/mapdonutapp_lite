import React, { useRef, useEffect, useState } from 'React';
import { View, StyleProp, ViewStyle } from 'react-native';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import { Navigation } from 'react-native-navigation';

interface props {
    backgroundColor?: string,
    style: StyleProp<ViewStyle>,
    right?: JSX.Element,
    center?: JSX.Element
}

export function FakeToolbar({ backgroundColor, right, center, style }: props) {

    const [toolbarHeight, setToolbarHeight] = useState(44);

    useEffect(() => {
        Navigation.constants().then(({ topBarHeight }) => setToolbarHeight(topBarHeight));
    });

    const _style: StyleProp<ViewStyle> = {...{
        position: "absolute",
        height: toolbarHeight,
        paddingHorizontal: 16,
        top: getStatusBarHeight(),
        left: 0,
        width: '100%',
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        zIndex: 1000,
        backgroundColor: backgroundColor || 'none'
    }, ...(style || {})};

    return <View style={_style}>
        <View>
            {center}
        </View>
        <View>
            {right}
        </View>
    </View>
}