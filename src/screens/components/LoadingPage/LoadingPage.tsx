import React from 'React';
import { View, ActivityIndicator } from "react-native";
import { useSelector } from 'react-redux';
import { StoreApp } from '../../../@types/store';

export default function LoadingPage() {
    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    return <View
        style={{ justifyContent: "center", height: '100%' }}
    >
        <ActivityIndicator size={'large'} color={themeVariables.PRIMARY_COLOR} />
    </View>
}