import React, { useState, useRef, useEffect } from "react";
import { Animated, View } from "react-native";
import FastImage from "react-native-fast-image";
import Button from "../button";
import { useSelector } from "react-redux";
import { StoreApp } from "../../../@types/store";
import { Mapdonut_ExploreItem } from "../../../@types/explore";
import { viewportWidth } from "../../../utils/utils";

interface ExploreItemProps {
    item: Mapdonut_ExploreItem,
    index: number,
    style: any
}

export default function ExploreItem(props: ExploreItemProps) {

    const { item, index, style } = props;
    //console.log(props);
    
    return <Button onPress={() => {
        //dispatch({ type: 'UPDATE_IMAGE', payload: { index: data.index } });
        //setIsSelected(!isSelected);
    }}
    >
        <View
            style={style}
        >
            <FastImage
                style={{width: '100%', height: '100%'}}
                source={{
                    uri: item.url,
                    priority: FastImage.priority.normal,
                }}
                resizeMode={FastImage.resizeMode.cover}
            />
        </View>
    </Button>
}