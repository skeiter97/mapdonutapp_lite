import React from 'React';
import { View } from 'react-native';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../../@types/store';

const CAMERA_WIDTH_ICON = 32;

interface props {
    isSelected?: boolean,
    children: any
}

export default function ButtonInCameraViewScreen({ isSelected, children }: props) {
    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    return <View style={{
        backgroundColor: isSelected ? themeVariables.PRIMARY_COLOR : 'rgba(0, 0, 0, 0.7)',
        justifyContent: "center",
        alignItems: "center",
        borderRadius: CAMERA_WIDTH_ICON,
        width: CAMERA_WIDTH_ICON,
        height: CAMERA_WIDTH_ICON
    }}>
        {children}
    </View>
}