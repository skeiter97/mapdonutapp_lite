import React, { useState, useEffect } from 'React';
import { Text } from 'react-native';
import { viewportWidth, closeModal } from '../../../utils/utils';
import PickPhotoLibrary from '../PickPhotoLibrary/PickPhotoLibrary';
import CameraScreen from '../camera';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../../@types/store';
import { imageSelected } from '../../../@types/camera';
import { Navigation } from 'react-native-navigation';
import { MAPDONUT_CAMERA_OVERLAY } from '../../screenNames';
import { TabView, TabBar } from 'react-native-tab-view';
import { getBottomSpace } from 'react-native-iphone-x-helper';

export default function SelectPicture(props: any) {

    const { words } = useSelector((state: StoreApp) => state.language);
    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    const [currentIndex, setCurrentIndex] = useState(0);
    const [state, setState] = useState({
        index: 0,
        routes: [
            { key: 'first', title: words.LIBRARY },
            { key: 'second', title: words.PHOTO },
        ],
    });
    const [photoCamera, setPhotoCamera] = useState('');
    const [photosCameraRoll, setPhotosCameraRoll] = useState((props.photosSelected || []) as imageSelected[]);

    useEffect(() => {
        const navigationButtonEventListener = Navigation.events()
            .registerNavigationButtonPressedListener(({ buttonId, componentId }) => {
                if (buttonId === `NEXT_${MAPDONUT_CAMERA_OVERLAY}`) {
                    if (state.index === 0) props.save_pics(photosCameraRoll);
                    else if (state.index === 1) props.save_pics(photoCamera ? [{ uri: photoCamera }] : []);
                    closeModal(componentId);
                } else if (buttonId === `BACK_${MAPDONUT_CAMERA_OVERLAY}`) {
                    if (state.index === 1) setPhotoCamera('');
                }
                else if (buttonId === `CANCEL_${MAPDONUT_CAMERA_OVERLAY}`) closeModal(componentId);
            });
        return () => {
            navigationButtonEventListener.remove()
        }
    });

    return <TabView
        navigationState={state}
        renderScene={({ route }) => {
            switch (route.key) {
                case 'first':
                    return <PickPhotoLibrary componentId={props.componentId} currentScreen={currentIndex} photosCameraRoll={photosCameraRoll} setPhotosCameraRoll={setPhotosCameraRoll} />
                case 'second':
                    return <CameraScreen componentId={props.componentId} currentScreen={currentIndex} photoCamera={photoCamera} setPhotoCamera={setPhotoCamera} />
                default:
                    return null;
            }
        }}
        onIndexChange={(index: number) => {
            setState(Object.assign(state, { index }));
            setCurrentIndex(index);
        }}
        initialLayout={{ width: viewportWidth }}
        tabBarPosition={"bottom"}
        renderTabBar={props =>
            <TabBar
                {...props}
                indicatorStyle={{ backgroundColor: themeVariables.TEXT_COLOR, height: 0 }}
                style={{ backgroundColor: themeVariables.BG_LAYOUT, bottom: 1 * getBottomSpace() }}
                activeColor={themeVariables.TEXT_COLOR}
                inactiveColor={themeVariables.TEXT_COLOR_PLACEHOLDER}
                onTabPress={({ route }) => {
                    setState(Object.assign(state, { index: route.key === 'first' ? 0 : 1 }));
                    setCurrentIndex(route.key === 'first' ? 0 : 1);
                }}
                renderLabel={({ route, color }) => (
                    <Text style={{ color }}>
                        {route.title}
                    </Text>
                )}
            />
        }
    />
}
