import React, { useState, useRef, useEffect } from "react";

import { Animated, View } from "react-native";
import FastImage from "react-native-fast-image";
import Button from "../button";
import { useSelector } from "react-redux";
import { StoreApp } from "../../../@types/store";

export default function MasonryCustomComponent({ source, data, style, dispatch, item }: any) {

    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    const [isSelected, setIsSelected] = useState(item.isSelected);

    return <Button onPress={() => {
        dispatch({ type: 'UPDATE_IMAGE', payload: { index: data.index } });
        setIsSelected(!isSelected);
    }}
    >
        <View style={style}>
            <FastImage
                style={style}
                source={{
                    uri: source.uri,
                    priority: FastImage.priority.normal,
                }}
                resizeMode={FastImage.resizeMode.cover}
            />
            <Animated.View style={{
                position: "absolute",
                borderWidth: isSelected ? 16 : 0,
                borderColor: isSelected ? themeVariables.MASONRY_SELECTED : 'transparent',
                ...style
            }} />
        </View>
    </Button>
}