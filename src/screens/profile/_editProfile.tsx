import React, { useState, useEffect, useContext } from 'React';
import { View, Text, TextInput, ScrollView, Alert } from 'react-native';
import { Navigation } from 'react-native-navigation';
import Avatar from '../components/avatar';
import { closeModal, pushScreen } from '../../utils/utils';
//import { SociaProfileSocialAccounts } from '../components/Profile_social_accounts';
import Cover from '../components/cover/cover';
import styles from './_editProfile.css';
import { useSelector, useDispatch } from 'react-redux';
import { userActionUpdate } from '../../redux/actions/userActions';
import Button from '../components/button';
import { SET_EMAIL, SET_PHONE_NUMBER, SET_USERNAME } from '../screenNames';
import { StoreApp } from '../../@types/store';
import { useInput } from '../hooks/useInput';
import { COVER_DEFAULT_URL } from '../../global/variables';

export default function EditProfile({ cancelButtonId, doneButtonId, componentId }: any) {

    const currentUser = useSelector((state: StoreApp) => state.currentUser.data);
    const dispatch = useDispatch();
    const { words } = useSelector((state: StoreApp) => state.language);
    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    const name = useInput(currentUser ? (currentUser.displayName || '') : '');
    const email = useInput(currentUser ? (currentUser.email || '') : '', words.REGISTER_EMAIL);
    const phone = useInput(currentUser ? (currentUser.phoneNumber || '') : '', words.REGISTER_PHONE);
    const [photoUrl, setPhotoUrl] = useState(currentUser ? (currentUser.photoURL || '') : '');
    const [coverURL, setCoverURL] = useState(currentUser.coverURL);
    const username = useInput(currentUser.username);
    const bio = useInput(currentUser.bio);

    useEffect(() => {
        const navigationButtonEventListener = Navigation.events()
            .registerNavigationButtonPressedListener(({ buttonId, componentId }) => {
                if (buttonId === cancelButtonId) closeModal(componentId);
                else if (buttonId === doneButtonId) saveData();
            });
        return () => {
            navigationButtonEventListener.remove();
        }
    });

    useEffect(() => {
        if (!currentUser) return;
        setCoverURL(currentUser.coverURL || '');
        setPhotoUrl(currentUser.photoURL || '');
        bio.setValue(currentUser.bio || '');
        username.setValue(currentUser.username || '');
        name.setValue(currentUser.displayName);
        phone.setValue(currentUser.phoneNumber);
        email.setValue(currentUser.email);
    }, [currentUser]);

    const saveData = () => {

        if (username.value.length === 0) return Alert.alert('Username cannot be blank');
        if (name.value.length === 0) return Alert.alert('Name cannot be blank'); 

        dispatch(userActionUpdate(currentUser.uid, {
            displayName: name.value,
            username: username.value,
            bio: bio.value,
            coverURL,
            photoURL: photoUrl || ''
        }));
        return closeModal(componentId);
    };

    /**** 
        <View>
            <Text className={styles.inputWrapperLabelTitle}> {words.SOCIAL_ACCOUNTS}  </Text>
            <SociaProfileSocialAccounts userId={currentUser.uid} socials={currentUser.socials} />
        </View>
    */

    return <ScrollView className={styles.page}>

        <Cover
            coverURL={coverURL}
            coverURLDefault={COVER_DEFAULT_URL}
            onChange={v => setCoverURL(v)}
            editable={true}
            path={`users/${currentUser.uid}/profile-photo`}
        />

        <View className={styles.photoWrapper}>
            <Avatar photoUrl={photoUrl} editable={true} onChange={(photo) => setPhotoUrl(photo)} styles={styles.profileAvatar} />
            <Text style={{ textAlign: 'center', marginTop: 4, color: themeVariables.TEXT_COLOR }} > {words.CHANGE_PROFILE_PHOTO} </Text>
        </View>

        <View className={styles.editProfileContent}>

            <View className={styles.inputWrapper}>
                <Text className={styles.inputWrapperLabel} style={{color: themeVariables.TEXT_COLOR}}> {words.NAME} </Text>
                <View className={styles.inputWrapperInput}>
                    <TextInput {...name} style={{ color: themeVariables.TEXT_COLOR }} />
                    <View className={styles.hr}></View>
                </View>
            </View>

            <Button onPress={() => pushScreen(componentId, SET_USERNAME, words.USERNAME, words)}>
                <View className={styles.inputWrapper}>
                    <Text className={styles.inputWrapperLabel} style={{ color: themeVariables.TEXT_COLOR }}> {words.USERNAME} </Text>
                    <View className={styles.inputWrapperInput}>
                        <TextInput {...username} style={{ color: themeVariables.TEXT_COLOR }} editable={false} />
                        <View className={styles.hr}></View>
                    </View>
                </View>
            </Button>

            <View className={styles.inputWrapper}>
                <Text className={styles.inputWrapperLabel} style={{color: themeVariables.TEXT_COLOR}}> {words.BIO} </Text>
                <View className={styles.inputWrapperInput}>
                    <TextInput {...bio} style={{ color: themeVariables.TEXT_COLOR }} multiline={true} textAlignVertical={'top'} />
                    <View className={styles.hr}></View>
                </View>
            </View>

            <View>
                <Text className={styles.inputWrapperLabelTitle} style={{ color: themeVariables.TEXT_COLOR }}> {words.PRIVATE_INFORMACION} </Text>
            </View>

            <Button onPress={() => pushScreen(componentId, SET_PHONE_NUMBER, words.PHONE_NUMBER, words)}>
                <View className={styles.inputWrapper}>
                    <Text className={styles.inputWrapperLabel} style={{ color: themeVariables.TEXT_COLOR }}> Phone </Text>
                    <TextInput {...phone} className={styles.inputWrapperInput} style={{ color: themeVariables.TEXT_COLOR }} editable={false} />
                </View>
            </Button>

            
        </View>

    </ScrollView>
}

/**  
 *             <Button onPress={() => Navigation.push(componentId, {
                component: {
                    name: SET_EMAIL,
                    passProps: {
                        doneButtonId: `done_${SET_EMAIL}`
                    },
                    options: {
                        topBar: {
                            title: {
                                text: currentUser.email ? words.EDIT_EMAIL : words.REGISTER_EMAIL
                            },
                            rightButtons: [
                                {
                                    id: `done_${SET_EMAIL}`,
                                    text: 'Done',
                                }
                            ]
                        }
                    }
                }
            })}>
                <View className={styles.inputWrapper}>
                    <Text className={styles.inputWrapperLabel} style={{color: themeVariables.TEXT_COLOR}}> Email </Text>
                    <TextInput {...email} className={styles.inputWrapperInput} style={{color: themeVariables.TEXT_COLOR}} editable={false} />
                </View>
            </Button>
 * 
 * 
*/
