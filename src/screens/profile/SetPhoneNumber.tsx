import React, { useEffect, useRef } from 'react';
import { View, Text, TextInput, Alert } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { StoreApp } from '../../@types/store';
import { Navigation } from 'react-native-navigation';
import { currentUserActionUpdatePhoneNumberl } from '../../redux/actions/currentUserActions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useInput } from '../hooks/useInput';
import Button from '../components/button';

export default function SetPhoneNumber(props: any) {

    const phoneNumber = useSelector((state: StoreApp) => state.currentUser.data.phoneNumber);
    const { words } = useSelector((state: StoreApp) => state.language);
    const dispatch = useDispatch();
    const { theme, themeVariables } = useSelector((state: StoreApp) => state.theme);
    const email = useInput(phoneNumber || '', words.PHONE_NUMBER);

    const inputRef = useRef<TextInput>(null);

    const save = async () => {
        if (email.value.length === 0) return Alert.alert('Phon Number cannot be blank');
        try {
            Navigation.pop(props.componentId);
            await dispatch(currentUserActionUpdatePhoneNumberl(email.value))
        } catch (error) {
            console.log({ error });
            Alert.alert(error.toString()); //TODO: Support multilanguage for Error Firebase
        }
    }

    useEffect(() => {
        const navigationEventListener = Navigation.events().bindComponent(this, props.componentId);
        const navigationButtonEventListener = Navigation.events()
            .registerNavigationButtonPressedListener(({ buttonId, componentId }) => {
                if (buttonId === props.doneButtonId) save();
            });
        return () => {
            navigationButtonEventListener.remove();
            navigationEventListener.remove();
        }
    });

    this.componentDidAppear = () => {
        if (inputRef.current) inputRef.current.focus();
    }

    return <View style={{
        backgroundColor: themeVariables.BG_LAYOUT_ACCENT, marginTop: 16,
        flexDirection: "row", alignItems: "center",
        paddingHorizontal: 16
    }}>
        <Icon
            name={'mobile-alt'}
            color={themeVariables.TEXT_COLOR_PLACEHOLDER}
            size={18}
            style={{}}
        />
        <TextInput ref={inputRef} {...email} style={{ flex: 1, padding: 16 }}
            autoCompleteType={'tel'}
            keyboardType={'phone-pad'}
            textContentType={'telephoneNumber'}
            returnKeyType={"done"}
            keyboardAppearance={theme === 'dark' ? 'dark' : 'light'}
        />
        <Button onPress={() => email.setValue('')}>
            <Icon
                name={'times-circle'}
                color={themeVariables.TEXT_COLOR_PLACEHOLDER}
                size={16}
            />
        </Button>
    </View>;
}
