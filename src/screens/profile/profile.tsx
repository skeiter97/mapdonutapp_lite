import React, { useState, useEffect, useRef } from 'React';
import { View, Text, ScrollView, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image';
import styles from './profile.css';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Navigation } from 'react-native-navigation';
import { PROFILE_EDIT_SCREEN, CREATE_TRIP } from '../screenNames';
import Avatar from '../components/avatar';
import Button from '../components/button';
import { Mapdonut_User } from '../../@types/user';
import { COVER_DEFAULT_URL } from '../../global/variables';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { StoreApp } from '../../@types/store';
import { viewportHeight, createModal } from '../../utils/utils';
import { isIphoneX } from 'react-native-iphone-x-helper'
import ButtonAddTrip from '../components/ButtonAddTrip/ButtonAddTrip';
import { userActionGetTrips, userActionFinishTripsWatch } from '../../redux/actions/userActions';
import ProfileTabs from '../../../components/ProfileTabs';

interface Props {
    userId: string;
    user: Mapdonut_User;
    currentUser: Mapdonut_User;
    componentId: string,
    isCurrentUser?: boolean;
}

Profile.propTypes = {
    isCurrentUser: PropTypes.bool.isRequired,
    userId: PropTypes.string.isRequired,
    user: PropTypes.any.isRequired,
    componentId: PropTypes.string.isRequired
};

export default function Profile({ isCurrentUser, user, componentId }: Props) {

    const isLogged = useSelector((state: StoreApp) => state.currentUser).isLogged;
    const currentUser = useSelector((state: StoreApp) => state.currentUser.data);
    const trips = useSelector((state: StoreApp) => state.currentUser.trips);
    const { theme, themeVariables } = useSelector((state: StoreApp) => state.theme);
    const { words } = useSelector((state: StoreApp) => state.language);
    const [userProfile, setUserProfile] = useState(isCurrentUser ? currentUser : user);
    const dispatch = useDispatch();

    useEffect(() => {
        console.log({ isCurrentUser, currentUser });
        if (isCurrentUser) setUserProfile(currentUser);
    }, [currentUser]);

    const mapDOM = useRef<MapView>(null);
    
     useEffect(() => {
        dispatch(userActionGetTrips(currentUser.uid));
        return () => {
            dispatch(userActionFinishTripsWatch(currentUser.uid));
        }
     }, []);

    const _onPress = () => Navigation.showModal({
        stack: {
            children: [{
                component: {
                    name: PROFILE_EDIT_SCREEN,
                    passProps: {
                        _reference: 'Modal for Edit Profile',
                        cancelButtonId: 'CANCEL_EDIT_PROFILE',
                        doneButtonId: 'DONE_EDIT_PROFILE'
                    },
                    options: {
                        topBar: {
                            title: {
                                text: words.PROFILE_EDIT_PROFILE
                            },
                            rightButtons: [
                                {
                                    id: 'DONE_EDIT_PROFILE',
                                    text: words.DONE,
                                }
                            ],
                            leftButtons: [
                                {
                                    id: 'CANCEL_EDIT_PROFILE',
                                    text: words.CANCEL,
                                }
                            ]
                        }
                    }
                }
            }]
        }
    });

    if (!isLogged || !userProfile) return <View />;

    return <View>
        <ScrollView style={{
            height: viewportHeight - 56 - 56 - (isIphoneX() ? 64 : 0)
        }}>
            <FastImage
                style={styles.headerCover}
                source={{
                    uri: userProfile.coverURL || COVER_DEFAULT_URL,
                    priority: FastImage.priority.normal,
                }}
                resizeMode={FastImage.resizeMode.cover}
            />

            <View className={styles.header}>

                <View className={styles.headerPhoto}>
                    <Avatar photoUrl={userProfile.photoURL} styles={styles.profileAvatar} />
                    <Text className={styles[`${theme}__headerTextName`]} style={{ color: themeVariables.PRIMARY_COLOR }}> {userProfile.displayName} </Text>
                    <Text className={styles[`${theme}__headerTextUsername`]} style={{ color: themeVariables.TEXT_COLOR__BIO }}> @{userProfile.username} </Text>
                </View>

                <View className={styles.headerText}>

                    <View className={styles.headerFollowers}>
                        <View className={styles.headerTextFollowingWrapper}>
                            <Button>
                                <View style={{ alignItems: "center" }}>
                                    <Text className={styles[`${theme}__headerTextFollowingNumber`]} style={{ color: themeVariables.TEXT_COLOR__BIO }}> 0 </Text>
                                    <Text className={styles[`${theme}__headerTextFollowing`]} style={{ color: themeVariables.TEXT_COLOR__BIO }}> {words.TRIPS} </Text>
                                </View>
                            </Button>
                        </View>

                        <View className={styles.headerTextFollowingWrapper}>
                            <Button>
                                <View style={{ alignItems: "center" }}>
                                    <Text className={styles[`${theme}__headerTextFollowingNumber`]} style={{ color: themeVariables.TEXT_COLOR__BIO }}> 0 </Text>
                                    <Text className={styles[`${theme}__headerTextFollowing`]} style={{ color: themeVariables.TEXT_COLOR__BIO }}> {words.FOLLOWERS} </Text>
                                </View>
                            </Button>
                        </View>

                        <View className={styles.headerTextFollowingWrapper}>
                            <Button>
                                <View style={{ alignItems: "center" }}>
                                    <Text className={styles[`${theme}__headerTextFollowingNumber`]} style={{ color: themeVariables.TEXT_COLOR__BIO }}> 1 </Text>
                                    <Text className={styles[`${theme}__headerTextFollowing`]} style={{ color: themeVariables.TEXT_COLOR__BIO }}> {words.FOLLOWING} </Text>
                                </View>
                            </Button>
                        </View>

                    </View>

                </View>
            </View>

            <Text className={styles[`${theme}__headerTextBio`]} style={{ color: themeVariables.TEXT_COLOR__BIO }}>
                {userProfile.bio} {__DEV__ ? '_ Dev _' : ''}
            </Text>

            <View style={{ flexDirection: 'row', paddingHorizontal: 16 }}>
                <View style={{ flex: 1 }}>
                    <Button onPress={_onPress}>
                        <View style={{
                            borderColor: themeVariables.BUTTON_BORDER_DEFAULT, borderWidth: 1, backgroundColor: themeVariables.BG_LAYOUT_ACCENT,
                            paddingVertical: 4, borderRadius: 4
                        }}>
                            <Text style={{ color: themeVariables.TEXT_COLOR, textAlign: "center" }}> {words.PROFILE_EDIT_PROFILE} </Text>
                        </View>
                    </Button>
                </View>
            </View>

            <View className={styles.content}>

                <View className={styles.mapWrapper}>

                    <View className={styles.mapWrapperIcon}>
                        <Icon name={'search-plus'} size={20} color={themeVariables.TEXT_COLOR} />
                    </View>

                    <MapView
                        provider={PROVIDER_GOOGLE}
                        style={[styles.map, { height: 120 }]}
                        showsUserLocation={true}
                        showsMyLocationButton={false}
                        ref={mapDOM}
                        zoomEnabled={false}
                        zoomTapEnabled={false}
                        scrollEnabled={false}
                        pitchEnabled={false}
                        onUserLocationChange={(res) => {
                            if (!mapDOM.current || !res.nativeEvent) return;
                            mapDOM.current.setCamera({
                                center: {
                                    latitude: res.nativeEvent.coordinate.latitude,
                                    longitude: res.nativeEvent.coordinate.longitude,
                                },
                                zoom: 1
                            });
                        }}
                    />
                </View>
            </View>

            <ProfileTabs trips={trips} />

        </ScrollView>
        
        <View style={{ zIndex: 10, alignItems: "center", top: -56 - 8 }}>
            <ButtonAddTrip componentId={componentId} />
        </View>
    </View>;

};
