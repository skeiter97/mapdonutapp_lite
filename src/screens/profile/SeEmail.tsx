import React, { useEffect, useRef } from 'react';
import { View, Text, TextInput, Alert } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { StoreApp } from '../../@types/store';
import { Navigation } from 'react-native-navigation';
import { currentUserActionUpdateEmail } from '../../redux/actions/currentUserActions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useInput } from '../hooks/useInput';
import Button from '../components/button';

export default function SetEmail(props: any) {

    const _email = useSelector((state: StoreApp) => state.currentUser.data.email);
    const { words } = useSelector((state: StoreApp) => state.language);
    const dispatch = useDispatch();
    const { theme, themeVariables } = useSelector((state: StoreApp) => state.theme);
    const email = useInput(_email || '', words.EMAIL);

    const emailRef = useRef<TextInput>(null);

    const save = async () => {
        try {
            await dispatch(currentUserActionUpdateEmail(email.value))
            await Navigation.pop(props.componentId);
        } catch (error) {
            console.log({error});
            Alert.alert(error.toString()); //TODO: Support multilanguage for Error Firebase
        }
    }

    useEffect(() => {
        const navigationEventListener = Navigation.events().bindComponent(this, props.componentId);
        const navigationButtonEventListener = Navigation.events()
            .registerNavigationButtonPressedListener(({ buttonId, componentId }) => {
                if (buttonId === props.doneButtonId) save();
            });
        return () => {
            navigationButtonEventListener.remove();
            navigationEventListener.remove();
        }
    });

    useEffect(() => {
        //console.log(emailRef.current);
        //if (emailRef.current) emailRef.current.focus();
    }, []);

    this.componentDidAppear = () => {
        console.log('componentDidAppear', emailRef.current);
        if (emailRef.current) emailRef.current.focus();
    }

    return <View style={{
        backgroundColor: themeVariables.BG_LAYOUT_ACCENT, marginTop: 16,
        flexDirection: "row", alignItems: "center",
        paddingHorizontal: 16
    }}>
        <Icon
            name={'envelope'}
            color={themeVariables.TEXT_COLOR_PLACEHOLDER}
            size={18}
            style={{}}
        />
        <TextInput ref={emailRef} {...email} style={{ flex: 1, padding: 16 }}
            autoCompleteType={'email'}
            keyboardType={'email-address'}
            textContentType={"emailAddress"}
            returnKeyType={"done"}
            autoCapitalize={"none"}
            keyboardAppearance={theme === 'dark' ? 'dark' : 'light'}
        />
        <Button onPress={() => email.setValue('')}>
            <Icon
                name={'times-circle'}
                color={themeVariables.TEXT_COLOR_PLACEHOLDER}
                size={16}
            />
        </Button>
    </View>;
}
