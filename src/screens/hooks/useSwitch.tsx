import { useState } from "react";

export function useSwitch(initialValue: boolean) {
    const [value, setValue] = useState(initialValue);
    const onValueChange = (v: boolean) => setValue(v);
    return { value, onValueChange, setValue };
}