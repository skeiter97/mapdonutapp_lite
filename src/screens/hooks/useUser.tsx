import { useState, useEffect } from "react";
import { Mapdonut_User } from "../../@types/user";
import { watchUser } from "../../utils/utils";

export function useUser(uid: string, u?: Mapdonut_User) {
    const [user, setValue] = useState(u || null);

    useEffect(() => {
        watchUser(uid, u => {
            if (!u) return;
            setValue(u);
        });
    }, []);

    return { user };
}