import { useState, useEffect, useContext } from 'react';
import firebase, { RNFirebase } from 'react-native-firebase';
import { Alert, Platform } from 'react-native';
import ActionSheet from 'react-native-action-sheet';
import ImagePicker from 'react-native-image-picker';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../@types/store';

export function uploadPicture(uri: string, _path: string, onProgres?: (percentage: number, path?: string) => void): Promise<string> {
    return new Promise((resolve, reject) => {
        if (!uri) return reject();
        const defaultStorage = firebase.storage();
        const fileRef = defaultStorage.ref(_path);
        const taskUpload = fileRef.putFile(uri);
        taskUpload.on(
            firebase.storage.TaskEvent.STATE_CHANGED,
            snapshot => {
                const _progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                if (onProgres) onProgres(_progress, _path);
                if (snapshot.state === firebase.storage.TaskState.SUCCESS) {
                    if (onProgres) onProgres(100, _path);
                    resolve(snapshot.downloadURL || '');
                }
            },
            error => {
                Alert.alert(error && error.message ? error.message : 'Try again');
                reject(error);
            }
        );
    });
}

export default function useSavePicture(defaultPhotoUrl?: string) {

    const [photoUrl, setPhotoUrl] = useState(defaultPhotoUrl || '');
    const [uploading, setUploading] = useState(false);
    const [progress, setProgress] = useState(0);
    const [photoUrlSaved, setPhotoUrlSaved] = useState(defaultPhotoUrl || '');

    const { words } = useSelector((state: StoreApp) => state.language);

    const setBlank = () => {
        setPhotoUrl('');
        setUploading(false);
        setProgress(0);
    }

    const processPhoto = (uri: string, _path: string) => {
        setProgress(0);
        setUploading(true);
        setPhotoUrl(uri);
        return uploadPicture(uri, _path, _progress => setProgress(_progress))
            .then(downloadURL => {
                setUploading(false);
                setProgress(100);
                setPhotoUrlSaved(downloadURL);
            })
            .catch(error => {
                Alert.alert(error && error.message ? error.message : 'Try again');
                setBlank();
            });
    };

    useEffect(() => {
        if (!photoUrl) setBlank();
    }, [photoUrl]);

    const lunchOptions = () => {

        const BUTTONSiOS = [
            words.TAKE_PHOTO,
            words.CHOOSE_FROM_LIBRARY,
            words.REMOVE_PICTURE,
            words.CANCEL
        ];

        const BUTTONSandroid = [
            words.TAKE_PHOTO,
            words.CHOOSE_FROM_LIBRARY,
            words.REMOVE_PICTURE,
        ];

        if (!photoUrl) {
            BUTTONSiOS.splice(2, 1);
            BUTTONSandroid.splice(2, 1);
        }

        ActionSheet.showActionSheetWithOptions({
            options: (Platform.OS == 'ios') ? BUTTONSiOS : BUTTONSandroid,
            destructiveButtonIndex: photoUrl ? 2 : -1,
            cancelButtonIndex: BUTTONSiOS.length - 1
        },
            (buttonIndex) => {
                console.log('button clicked :', buttonIndex);
                const opts = {
                    storageOptions: {
                        skipBackup: true,
                        path: 'images',
                    },
                    noData: true,
                    //allowsEditing: true, // IOS only
                }

                if (buttonIndex === 0 || buttonIndex === 1) ImagePicker[buttonIndex === 0 ? 'launchCamera' : 'launchImageLibrary'](opts, (response) => {
                    if (response.didCancel) {
                        console.log('User cancelled image picker');
                        //setUploading(false);
                    } else if (response.error) {
                        console.log('ImagePicker Error: ', response.error);
                        Alert.alert(response.error);
                        //setUploading(false);
                    } else {
                        //processPhoto(response.uri);
                        //setCoverURL(response.uri);
                        setPhotoUrl(response.uri);
                    }
                });
                else if (buttonIndex === 2) {
                    //setCoverURL('');
                    setPhotoUrl('');
                }
            });
    };

    return {
        photoUrl,
        setPhotoUrl,
        progress,
        uploading,
        processPhoto,
        photoUrlSaved,
        lunchOptions
    }
}
