import { useEffect } from "react";
import { Navigation } from "react-native-navigation";
import { closeModal } from "../../utils/utils";

interface screenLifeCycle {
    componentDidAppear?: () => void,
    componentDidDisappear?: (e: any) => void,
    searchBarUpdated?: (opts: { text: string, isFocused: boolean}) => void,
    searchBarCancelPressed?: () => void,
}

export default function useBindScreen(_this: any, props: any, state: any = {}) {

    const processDone = async (componentId: string) => {
        try {
            if (props.preDone) await props.preDone(state);
            closeModal(componentId);
            if (props.done) props.done(state);
        } catch (error) {
            console.log({ error_useBindScreen: error});
        }
    };

    useEffect(() => {
        const navigationEventListener = Navigation.events().bindComponent(_this, props.componentId);
        const navigationButtonEventListener = Navigation.events()
            .registerNavigationButtonPressedListener(({ buttonId, componentId }) => {
                if (buttonId === props.cancelButtonId) closeModal(componentId);
                else if (buttonId === props.doneButtonId) processDone(componentId);
            });
        this.searchBarCancelPressed = () => closeModal(props.componentId);
        return () => {
            navigationEventListener.remove();
            navigationButtonEventListener.remove();
        }
    });

    return {
        state
    }

}

export function useBindRNN(_this: any, props: any) {
    useEffect(() => {
        const navigationEventListener = Navigation.events().bindComponent(_this, props.componentId);
        return () => {
            navigationEventListener.remove();
        }
    });
}