import Geocoder from 'react-native-geocoding';
import Geolocation from 'react-native-geolocation-service';

interface shortcutPlace {
    place_id: string,
    formatted_address: string
}

export function useGeolocation() {
    const getCurrentLocation = (): Promise<{lat: number, lng: number}> => {
        return new Promise((resolve, reject) => {
            Geolocation.getCurrentPosition((position) => {
                resolve({lat: position.coords.latitude, lng: position.coords.longitude});
            }, (err) => reject(err));
        });
    };

    const getCurrentPlaceGooglePlaces = async (lat: number, lng: number): Promise<shortcutPlace | null> => {
        const geos = await Geocoder.from(lat, lng);
        //const res = geos.results.find((g: any) => g.types.find((t: any) => // t.toLowerCase() === 'locality' ||
        //    t.toLowerCase() === 'administrative_area_level_2' ||
        //    t.toLowerCase() === 'administrative_area_level_1'
        //));
        return geos.results.length > 0 ? geos.results[0] : null;
    }

    return { getCurrentLocation, getCurrentPlaceGooglePlaces };
}