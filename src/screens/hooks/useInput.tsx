import { useState } from "react";
import { useSelector } from "react-redux";
import { StoreApp } from "../../@types/store";
import { Mapdonut_inputHook } from "../../@types/inputs";

export function useInput(initialValue: string = '', placeholder: string = ''): Mapdonut_inputHook {
    const [value, setValue] = useState(initialValue);
    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    const onChangeText = (text: string) => setValue(text);
    return { value, onChangeText, setValue, placeholder, placeholderTextColor: themeVariables.TEXT_COLOR_PLACEHOLDER };
}
