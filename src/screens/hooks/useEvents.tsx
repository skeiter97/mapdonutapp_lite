import { useState, useEffect } from "react";
import MapView from "react-native-maps";
import Geocoder from 'react-native-geocoding';
import Geolocation from 'react-native-geolocation-service';
import RNGooglePlaces from 'react-native-google-places';
import firebase from 'react-native-firebase';
import { Mapdonut_Event } from "../../@types/event";
import { createFirebaseGeopoint, createFirebaseTimestampFromSeconds } from "../../utils/utils";
import AsyncStorage from "@react-native-community/async-storage";
import { ItemSearchBarList } from "../../@types/searchBarList";

enum DEFAULTS_LOCATION {
    CURRENT_LOCATION = 'CURRENT_LOCATION'
}

export default function useCurrentCity(mapDOM: MapView | null) {
    const [name, setName] = useState('');

    const [currentLocation, setCurrentLocation] = useState({
        placeId: '',
        currentEvents: [] as Mapdonut_Event[]
    });

    const getEventsAccordingPosition = (lat: number, lng: number) => {
        const httpsCallable = firebase.functions().httpsCallable('getEventAccordingLocation');
        return httpsCallable({ lat, lng })
            .then(({ data: { events, algoliaResponse }}) => {
                return {
                    algoliaResponse,
                    events: events.map((ev: any) => ({
                        ...ev,
                        location: createFirebaseGeopoint(ev.location._latitude, ev.location._longitude),
                        datetime: createFirebaseTimestampFromSeconds(ev.datetime._seconds)
                    }))
                }
            })
    };

    const printMap = (lat: number, lng: number) => {
        if (!mapDOM) return;
        mapDOM.setCamera({
            center: {
                latitude: lat,
                longitude: lng
            },
            zoom: 12
        });
    }

    const getCurrentLocation = () => {
        Geolocation.getCurrentPosition(async (position) => {
            printMap(position.coords.latitude, position.coords.longitude);
            try {
                const geos = await Geocoder.from(position.coords.latitude, position.coords.longitude);
                const res = geos.results.find((g: any) => g.types.find((t: any) => // t.toLowerCase() === 'locality' ||
                    t.toLowerCase() === 'administrative_area_level_2' ||
                    t.toLowerCase() === 'administrative_area_level_1'
                ));
                setName(res && res.formatted_address ? res.formatted_address : 'UNKNOWN');
                const response = await getEventsAccordingPosition(position.coords.latitude, position.coords.longitude);
                setCurrentLocation({
                    placeId: DEFAULTS_LOCATION.CURRENT_LOCATION,
                    currentEvents: response.events
                });
            } catch (error) {
                console.log({error});
            }
        }, (err) => console.log({ err_getCurrentPosition: err}));
    }

    const changeCity = async (item: ItemSearchBarList) => {
        try {
            if (item.id === DEFAULTS_LOCATION.CURRENT_LOCATION) return getCurrentLocation();
            setName(item.title + ', ' + item.subtitle);
            const city = await RNGooglePlaces.lookUpPlaceByID(item.id);
            printMap(city.location.latitude, city.location.longitude);
            const response = await getEventsAccordingPosition(city.location.latitude, city.location.longitude);
            setCurrentLocation({ placeId: city.placeID, currentEvents: response.events });

            const _lastCities = (await AsyncStorage.getItem('@lastCities')) || JSON.stringify([]);
            const lastCities = JSON.parse(_lastCities) as ItemSearchBarList[];
            await AsyncStorage.setItem('@lastCities',
                JSON.stringify(
                    lastCities.concat(item)
                    .filter((c) => c.id !== 'CURRENT_LOCATION')
                    .filter((c, i: number, arr: any[]) => arr.findIndex(_c => _c.id === c.id) === i)
                    .slice(-10)
                )
            );

        } catch (httpsError) {
            //console.log(httpsError.code); // invalid-argument
            //console.log(httpsError.message); // Your error message goes here
            //console.log(httpsError.details.foo); // bar
        }
    };

    return {
        ...currentLocation,
        name,
        changeCity,
        getCurrentLocation
    }
}