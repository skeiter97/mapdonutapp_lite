import React from 'react';
import { View, TouchableHighlight, Text } from 'react-native';
import { Navigation } from 'react-native-navigation';
import stylesButtons from '../../styles/buttons.css';

export default function LoginProblems(props: any) {
    return <View>

        


        <TouchableHighlight
            onPress={async () => {
                Navigation.dismissModal(props.componentId);
            }}
        >
            <View className={stylesButtons.btn}>
                <Text className={stylesButtons.btnText}> Close </Text>
            </View>
        </TouchableHighlight>
    </View>
}

