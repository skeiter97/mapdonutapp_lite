import React, { useEffect, useState, useRef } from 'react';
import { View, Text, Alert, TextInput } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import stylesButtons from '../../styles/buttons.css';
import PhoneInput from 'react-native-phone-input'
import CountryPicker, { getAllCountries } from 'react-native-country-picker-modal';
import DeviceInfo from 'react-native-device-info';
import styles from './_phoneModal.css';
import stylesForm from '../../styles/forms.css';
import firebase, { RNFirebase } from 'react-native-firebase';
import { openLoadingOverlay, closeLoadingOverlay, viewportWidth } from '../../utils/utils';
import { goToHome } from '../handleUi';
import useBindScreen from '../hooks/useBindScreen';
import Button from '../components/button';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../@types/store';

export default function PhoneModal(props: any) {

    const { words } = useSelector((state: StoreApp) => state.language);

    let userLocaleCountryCode = DeviceInfo.getDeviceCountry();
    let _cca2 = userLocaleCountryCode || 'US';
    //const [_lng, _lng2] = [DeviceInfo.getDeviceLocale(), DeviceInfo.getPreferredLocales()];

    const userCountryData = getAllCountries()
        .filter(country => country.cca2 === userLocaleCountryCode)

    const phone = useRef(null as any);
    const myCountryPicker = useRef(null as any);
    const carousel = useRef(null as any);

    const [phoneInputValue, setPhoneInputValue] = useState(userCountryData.length > 0 ? '+' + userCountryData[0].callingCode : '+1');
    const [cca2, setCca2] = useState(_cca2);
    const [confirmResult, setConfirmResult] = useState({} as RNFirebase.ConfirmationResult);
    const [codeConfirmation, setCodeConfirmation] = useState('');
    useBindScreen(this, props);

    const sendCode = async() => {
        //console.log('SEND CODE', { carousel, phone, phoneExists: !!phone, isValid: phone.current.isValidNumber() }, 'Send a sms to: ', phone.current.state.formattedNumber, phoneInputValue);
        if (!phone || (phone && phone.current && !phone.current.isValidNumber())) return await Alert.alert('The phone Number is Invalid');
        try {
            carousel.current.snapToNext();
            const confirmResult = await firebase.auth().signInWithPhoneNumber(phone.current.state.formattedNumber);//('+51991363083');
            setConfirmResult(confirmResult);
        } catch (e) {
            carousel.current.snapToPrev();
        }
    };

    const LoginPhoneNumber = <View className={styles.container}>
        <View>
            <PhoneInput
                ref={phone}
                value={ phoneInputValue }
                onPressFlag={() => myCountryPicker ? myCountryPicker.current.openModal() : null}
                onSelectCountry={(iso2: any) => {
                    //console.log({ onSelectCountry: iso2 });
                    if (phone && phone.current) phone.current.focus();
                }}
                onChangePhoneNumber={(number: any) => {
                    if (phone && phone.current) setPhoneInputValue(number);
                }}
                className={stylesForm.input}
                textStyle={styles.inputText}
                offset={16}
                pickerButtonColor={'#000'}
                pickerBackgroundColor={'#666'}
                autoFormat={true}
            />

            <CountryPicker
                ref={myCountryPicker}
                onChange={(country: any) => {
                    // console.log('onChange', { country, phone });
                    if (phone) setPhoneInputValue(`+${country.callingCode}`);
                    setCca2(country.cca2);
                }}
                translation={'eng'}
                cca2={cca2}
                closeable={true}
                filterable={false}
                hideAlphabetFilter={false}
                filterPlaceholder={'Country'}
                showCallingCode={true}
            >
                <View />
            </CountryPicker>
        </View>

        <View style={{marginBottom: 16}}></View>

        <Button onPress={sendCode} disabled={phone && phone.current && !phone.current.isValidNumber()}>
            <View className={phone && phone.current && phone.current.isValidNumber() ? stylesButtons.btnPrimary : stylesButtons.btn}>
                <Text className={phone && phone.current && phone.current.isValidNumber() ? stylesButtons.btnPrimaryText : stylesButtons.btnText}>
                    {words.NEXT}
                </Text>
            </View>
        </Button>
    </View>

    const ConfirmationCode = <View className={styles.container}>
        <TextInput
            placeholder='Code'
            className={stylesForm.input}
            onChangeText={(text) => setCodeConfirmation(text)}
            value={codeConfirmation}
            style={{ textAlign: 'center', fontWeight: 'bold' }}
            keyboardType={"number-pad"}
            returnKeyType={"done"}
            textContentType={"oneTimeCode"}
        ></TextInput>
        <View style={{ marginBottom: 16 }}></View>
        <Button
            disabled={codeConfirmation.length < 6}
            onPress={async () => {
                const overlay = await openLoadingOverlay();
                try {
                    const user = await confirmResult.confirm(codeConfirmation);
                    if (!user) throw new Error('LOGIN_PHONE_CODE_INVALID');
                    await goToHome(user);
                    await closeLoadingOverlay(overlay);
                } catch (err) {
                    console.log('CHECK CODE', err);
                    Alert.alert('The Code is Invalid');
                    closeLoadingOverlay(overlay);
                }
            }}
        >
            <View className={codeConfirmation.length === 6 ? stylesButtons.btnPrimary : stylesButtons.btn}>
                <Text className={stylesButtons.btnText}> { words.VERIFY } </Text>
            </View>
        </Button>
    </View>;

    return <Carousel
        ref={carousel}
        data={['loginPhoneNumber', 'confirmationCode']}
        renderItem={({ item }: any) => item === 'loginPhoneNumber' ? LoginPhoneNumber : ConfirmationCode }
        sliderWidth={viewportWidth}
        itemWidth={viewportWidth}
        scrollEnabled={false}
    />;
}

