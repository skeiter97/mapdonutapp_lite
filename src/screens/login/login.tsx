import React from 'react';
import { View, Text } from 'react-native';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import firebase from 'react-native-firebase';
import stylesButtons from '../../styles/buttons.css';
import { openLoadingOverlay, closeLoadingOverlay, createModal } from '../../utils/utils';
import { goToHome } from '../handleUi';
import { PHONE_LOGIN, LOGIN_PROBLEMS } from '../screenNames';
import Button from '../components/button';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../@types/store';

export default function Login(props: any) {

    const { words } = useSelector((state: StoreApp) => state.language);

    const _facebookLogin = async function () {
        let overlay: any;
        try {
            //https://medium.com/building-with-react-native/coding-with-facebook-login-in-react-native-like-a-pro-1x06-9064fc2f5bfc
            const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);
            overlay = await openLoadingOverlay();
            if (result.isCancelled) {
                // handle this however suites the flow of your app
                throw new Error('FB_LOGIN_USER_CANCEL');
            }
            // get the access token
            const data = await AccessToken.getCurrentAccessToken();
            if (!data) {
                // handle this however suites the flow of your app
                //throw new Error('Something went wrong obtaining the users access token');
                throw new Error('FB_LOGIN_NO_ACCESS_TOKEN');
            }
            // create a new firebase credential with the token
            const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
            // login with credential
            const firebaseUserCredential = await firebase.auth().signInWithCredential(credential);
            //console.warn(JSON.stringify(firebaseUserCredential.user.toJSON()))
            const user = firebaseUserCredential.user;
            //console.log('User logged FB', { user, firebaseUserCredential, result, data, credential });
            if (user) await goToHome(user);
            else if (!user) throw new Error('LOGIN_FB_INVALID');
            await closeLoadingOverlay(overlay);
        } catch (e) {
            console.info('Err catched: ', e);
            if (overlay) await closeLoadingOverlay(overlay);
        }
    }

    return <View>
        <View style={{paddingHorizontal: 16}}>

            <Button onPress={_facebookLogin}>
                <View className={stylesButtons.btnFb}>
                    <Text className={stylesButtons.btnFbText}> {words.LOGIN_WITH_FB} </Text>
                </View>
            </Button>

            <View style={{ marginBottom: 16 }}></View>

            <Button onPress={() => createModal(PHONE_LOGIN, { noSearchBar: true }, { topBarTitle: words.LOGIN_WITH_PHONE }, words)}>
                <View className={stylesButtons.btn}>
                    <Text className={stylesButtons.btnText}> {words.LOGIN_WITH_PHONE } </Text>
                </View>
            </Button>
        </View>

        <View style={{ marginBottom: 16 }}></View>

        <Button onPress={() => createModal(LOGIN_PROBLEMS, { noSearchBar: true }, { topBarTitle: words.HELP_CENTER }, words)}>
            <View className={stylesButtons.btnClear} style={{}}>
                <Text className={stylesButtons.btnClearText}> {words.LOGIN_PROBLEMS} </Text>
            </View>
        </Button>

        <View style={{ marginBottom: 36 }}></View>

        <Button onPress={() => props.showWelcome()}>
            <View className={stylesButtons.btnClear}>
                <Text className={stylesButtons.btnClearText}> { words.SHOW_WELCOME } </Text>
            </View>
        </Button>
    </View>
    
}