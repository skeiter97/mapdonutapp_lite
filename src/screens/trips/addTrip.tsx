import React, { useEffect, useState } from 'React';
import { View, Text, Platform, FlatList, TouchableWithoutFeedback, TouchableNativeFeedback, TouchableOpacity, Image } from 'react-native';
import { Navigation } from 'react-native-navigation';
import RNGooglePlaces, { GMSTypes } from 'react-native-google-places';
import styles from './addTrip.css';
import {  TRIP_FORM_SCREEN } from '../screenNames';
import { viewportHeight } from '../../utils/utils';

export default function AddTrip(props: any) {

    const [topBarHeight, setTopBarHeight] = useState(0);
    const [cities, setCities] = useState([] as GMSTypes.AutocompletePrediction[]);
    const [isFocused, setIsFocused] = useState(0);

    this.searchBarUpdated = async ({ text, isFocused }) => {
        console.log({ text, isFocused, topBarHeight });
        setIsFocused(isFocused);
        //Navigation.mergeOptions(props.componentId, {
        //    topBar: {
        //        topMargin: topBarHeight
        //    }
        //});

        try {
            const cities = await RNGooglePlaces.getAutocompletePredictions(text, {
                type: 'cities'
            });
            setCities(cities);
        } catch (error) {
            console.log(error.message)
        }
    };

    useEffect(() => {

        Navigation.constants().then(c => setTopBarHeight(c.topBarHeight))
        
        let navigationEventListener: any;
        if (Platform.OS === 'ios') {
            //searchBarUpdated (iOS 11+ only)
            navigationEventListener = Navigation.events().bindComponent(this, props.componentId);
        }

        return () => {
            if (navigationEventListener) navigationEventListener.remove();
        }

    }, []);

    const _selectCity = async() => {
        return Navigation.push(props.componentId, {
            component: {
                name: TRIP_FORM_SCREEN,
                passProps: {
                    _reference: 'Form Trip'
                },
                options: {
                    topBar: {
                        title: {
                            text: 'Form Trip',
                        },
                    }
                }
            }
        })
    }

    const Touchable = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity;

    return <View>
        <FlatList
            data={cities}
            keyExtractor={(item: any) => item.placeID}
            initialNumToRender={5}
            style={{ height: viewportHeight - (50 * 4), top: isFocused ? -1 * topBarHeight : 0 }}
            ListEmptyComponent={<View className={styles.emptyWrapper} style={{ height: viewportHeight - ( 50 *  4) }}>
                <Image
                    style={styles.emptyImage}
                    source={require('../../assets/bg/capaddocia.jpg')}
                />
            </View>}
            ListHeaderComponent={() => cities.length === 0 ? null : <View>
                <View className={styles.listItem}>
                    <Text> Select City </Text>
                </View>
                <View className={styles.hr} />
            </View>}
            renderItem={({ item }) => <Touchable onPress={_selectCity}>
                <View className={styles.listItem}>
                    <Text> {item.primaryText} </Text>
                    <Text> {item.secondaryText} </Text>
                </View>
                <View className={styles.hr}/>
            </Touchable>}
        />
    </View>
}
