import React, { useState } from 'React';
import { View, Text, Image, ScrollView, FlatList, TextInput } from 'react-native';
import useBindScreen from '../hooks/useBindScreen';
import Button from '../components/button';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { createModal, createFirebaseGeopointDefault } from '../../utils/utils';
import { MAPDONUT_CAMERA_OVERLAY, SET_DESCRIPTION } from '../screenNames';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../@types/store';
import { useInput } from '../hooks/useInput';
import { imageSelected } from '../../@types/camera';
import { Mapdonut_Trip } from '../../@types/trip';
import LocationWithMap from '../components/LocationWithMap/LocationWithMap';
import FastImage from 'react-native-fast-image';

const WIDTH_THUMBNAIL = 100;
const BORDER_THUMBNAIL = 4;
const WIDTH_THUMBNAIL_CLOSE = 24;

interface CreateTrip {
    item?: Mapdonut_Trip,
}

export default function CreateTrip(props: CreateTrip) {

    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    const { words } = useSelector((state: StoreApp) => state.language);

    const [photosToUpload, setPhotosToUpload] = useState([] as imageSelected[]);
    const [locationGeopoint, setLocationGeopoint] = useState(createFirebaseGeopointDefault());
    const locationText = useInput('', words.LOCATION);
    const locationId = useInput('');
    const description = useInput('', words.DESCRIPTION);

    useBindScreen(this, props, {
        item: {
            photosToUpload,
            locationGeopoint,
            locationText: locationText.value,
            locationId: locationId.value,
            description: description.value
        }
    });

    const save_pics = (photos: imageSelected[]) => {
        console.log({ photos });
        const newPhotos = photosToUpload.concat(photos)
            .filter((p, i, arr) => arr.findIndex(_p => _p.uri === p.uri) === i);
        setPhotosToUpload(newPhotos);
    }

    const removeItem = (item: imageSelected) => {
        const newPhotos = photosToUpload
            .filter((p) => p.uri !== item.uri);
        setPhotosToUpload(newPhotos);
    };

    return <ScrollView>
        
        <View style={{
            backgroundColor: themeVariables.BG_LAYOUT_ACCENT,
            padding: 16
        }}>
            <Text style={{ color: themeVariables.TEXT_COLOR, marginBottom: 16, fontWeight: "bold" }}> {words.ADD_PHOTO} </Text>
            <View style={{flexDirection: "row"}}>
                <Button onPress={() => {
                    createModal(MAPDONUT_CAMERA_OVERLAY, { noSearchBar: true, hideStatusBar: true, save_pics, photosSelected: photosToUpload }, { topBarTitle: words.CAMERA_ROLL }, words)
                }}>
                    <View style={{ width: WIDTH_THUMBNAIL, height: WIDTH_THUMBNAIL, justifyContent: "center", alignItems: "center", backgroundColor: themeVariables.BG_LAYOUT }}>
                        <Icon name={'camera'} size={32} style={{ color: themeVariables.PRIMARY_COLOR}} />
                    </View>
                </Button>
                <FlatList
                    style={{marginLeft: 8}}
                    bounces={false}
                    data={photosToUpload}
                    keyExtractor={(item) => item.uri}
                    renderItem={({ item }) => <TripPhoto item={item} removeItem={removeItem} />}
                    horizontal={true}
                    initialNumToRender={10}
                    ItemSeparatorComponent={() => <View style={{ width: 8, height: WIDTH_THUMBNAIL, backgroundColor: themeVariables.BG_LAYOUT_ACCENT }} />}
                />
            </View>
            
        </View>

        <View style={{
            backgroundColor: themeVariables.BG_LAYOUT_ACCENT,
            padding: 16,
            marginTop: 16
        }}>
            <Text style={{ color: themeVariables.TEXT_COLOR, marginBottom: 16, fontWeight: "bold" }}> { words.WHERE__ } </Text>
            <LocationWithMap locationText={locationText} locationId={locationId} locationGeopoint={locationGeopoint} setLocationGeopoint={setLocationGeopoint} />
        </View>

        
        <Button onPress={ async () => {
            await createModal(SET_DESCRIPTION, {
                noSearchBar: true, inputField: description, done: ({ inputValue } : any) => {
                    description.setValue(inputValue);
            } }, { topBarTitle: words.DESCRIPTION }, words)
        }}>
            <View style={{
                backgroundColor: themeVariables.BG_LAYOUT_ACCENT,
                padding: 16,
                marginTop: 16
            }}>
                { description.value.length > 0 ?
                    <Text style={{ color: themeVariables.TEXT_COLOR }} numberOfLines={4} ellipsizeMode={'tail'} > { description.value } </Text> :
                    <Text style={{ color: themeVariables.TEXT_COLOR_PLACEHOLDER }} > { description.placeholder } </Text>
                }
            </View>
        </Button>

    </ScrollView>
}

interface TripPhotosProps {
    item: imageSelected,
    removeItem: (a: imageSelected) => void
}
function TripPhoto({ item, removeItem } : TripPhotosProps) {
    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    const stylesWrapper = {
        width: WIDTH_THUMBNAIL, height: WIDTH_THUMBNAIL
    };
    return <View style={{ ...stylesWrapper, borderWidth: BORDER_THUMBNAIL, borderColor: themeVariables.BG_LAYOUT }}>
        <View style={{
            position: "absolute", right: 4, top: 4,
            justifyContent: "center",
            backgroundColor: themeVariables.BG_LAYOUT,
            alignItems: "center", zIndex: 10,
            width: WIDTH_THUMBNAIL_CLOSE,
            height: WIDTH_THUMBNAIL_CLOSE,
            borderRadius: WIDTH_THUMBNAIL_CLOSE
        }}>
            <Button onPress={() => removeItem(item)}>
                <Icon
                    name={'times'}
                    color={themeVariables.TEXT_COLOR}
                    size={16}
                />
            </Button>
        </View>
        <FastImage fallback={true} style={{
            width: WIDTH_THUMBNAIL - (BORDER_THUMBNAIL * 2), height: WIDTH_THUMBNAIL - (BORDER_THUMBNAIL * 2)
        }} source={{ uri: item.uri }} />
    </View>
}