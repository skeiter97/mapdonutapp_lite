import React from 'react';
import { View, Text } from 'react-native';
import { Mapdonut_Event } from '../../@types/event';
import Cover from '../components/cover/cover';

interface Props{
    item: Mapdonut_Event
}

export default function Event({item} : Props) {

    return <View>
        <Cover coverURL={item.photoURL} path={`events/jkjkkjjkj`} height={180} ></Cover>
        <Text> {item.title} </Text>
        <Text> {item.description} </Text>
    </View>;
}
