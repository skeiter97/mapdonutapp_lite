import React, { useRef, useEffect } from 'React';
import { View, Alert, TextInput, Text } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { StoreApp } from '../../@types/store';
import { useInput } from '../hooks/useInput';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Button from '../components/button';
import { Navigation } from 'react-native-navigation';
import { currentUserActionUpdateUsername } from '../../redux/actions/currentUserActions';
import { SET_USERNAME } from '../screenNames';

export default function SetUsername(props: any) {

    const userField = useSelector((state: StoreApp) => state.currentUser.data.username);
    const { words } = useSelector((state: StoreApp) => state.language);
    const { theme, themeVariables } = useSelector((state: StoreApp) => state.theme);
    const dispatch = useDispatch();
    const field = useInput(userField || '');

    const inputRef = useRef<TextInput>(null);

    const save = async () => {
        //Todo: Add Validations
        try {
            Navigation.pop(props.componentId);
            await dispatch(currentUserActionUpdateUsername(field.value))
        } catch (error) {
            console.log({ error });
            Alert.alert(error.toString()); //TODO: Support multilanguage for Error Firebase
        }
    }

    useEffect(() => {
        const navigationEventListener = Navigation.events().bindComponent(this, props.componentId);
        const navigationButtonEventListener = Navigation.events()
            .registerNavigationButtonPressedListener(({ buttonId }) => {
                if (buttonId === props.doneButtonId) save();
            });
        return () => {
            navigationButtonEventListener.remove();
            navigationEventListener.remove();
        }
    });

    this.componentDidAppear = () => {
        if (inputRef.current) inputRef.current.focus();
    }

    return <View style={{
        backgroundColor: themeVariables.BG_LAYOUT_ACCENT,
        paddingHorizontal: 16,
    }}>
        <Text style={{ color: themeVariables.TEXT_COLOR_PLACEHOLDER, textAlign: "left", marginTop: 8, left: -4 }}> {words.USERNAME} </Text>
        <View style={{
            flexDirection: "row", alignItems: "center",
        }}>
            <TextInput ref={inputRef} {...field} style={{ flex: 1, paddingVertical: 8, paddingHorizontal: 0 }}
                autoCompleteType={'username'}
                keyboardType={'twitter'}
                textContentType={"username"}
                returnKeyType={"done"}
                keyboardAppearance={theme === 'dark' ? 'dark' : 'light'}
            />
            {field.value.length > 0 ? <Button onPress={() => field.setValue('')}>
                <Icon
                    name={'times-circle'}
                    color={themeVariables.TEXT_COLOR_PLACEHOLDER}
                    size={16}
                />
            </Button> : null}
        </View>
        
    </View>;
}