import { StatusBar } from 'react-native';
import Geocoder from 'react-native-geocoding';

import {
    WELCOME_SCREEN,
    PROFILE_SCREEN,
    PROFILE_EDIT_SCREEN,
    PHONE_LOGIN,
    LOGIN_PROBLEMS,
    SETTINGS_SCREEN,
    EXPLORE_SCREEN,
    INBOX_SCREEN,
    EVENTS_SCREEN,
    ADD_TRIP_SCREEN,
    TRIP_FORM_SCREEN,
    MAPDONUT_LOADING_OVERLAY,
    MAPDONUT_CAMERA_OVERLAY,
    EVENT_FORM,
    EVENT_SINGLE,
    EVENT_FORM_CHANGE_LOCATION,
    SET_EMAIL,
    SET_PHONE_NUMBER,
    SELECT_LANGUAGE,
    SELECT_THEME,
    SELECT_LOCATION,
    SELECT_DATETIME,
    SET_PREFERENCES,
    SET_USERNAME,
    CREATE_TRIP,
    SET_DESCRIPTION
} from './screenNames';

import Welcome from './welcome/welcome';
import Profile from './profile/profile';
import ProfileEdit from './profile/_editProfile';
import PhoneLogin from './login/_phoneModal';
import Settings from './settings/settings';
import LoginProblems from './login/_loginProblems';
import Loading from './loading/loading';
import Inbox from './inbox/inbox';
import Explore from './explore/explore';
import Events from './events/events';
import EventForm from './events/EventForm';
import AddTrip from '../screens/trips/addTrip';
import firebase from 'react-native-firebase';
import { openLoadingOverlay, closeLoadingOverlay } from '../utils/utils';
import TripForm from './trips/tripForm';
import EventsChangeLocation from './events/EventChangeLocation';
import Event from './Event/Event';
import SetEmail from './profile/SeEmail';
import SetPhoneNumber from './profile/SetPhoneNumber';
import { goToWelcome, goToHome } from './handleUi';
import SelectLanguage from './components/SelectLanguage';
import SelectTheme from './components/SelectTheme';
import SelectLocation from './components/SelectLocation';
import SelectDatetime from './components/SelectDatetime';
import Preferences from './settings/Preferences/Preferences';
import SetUsername from './SetUsername/SetUsername';
import CreateTrip from './CreateTrip/CreateTrip';
import SelectPicture from './components/SelectPicture/SelectPicture';
import SetDescription from './SetDescription/SetDescription';

export const Screens = new Map();

Screens.set(WELCOME_SCREEN, Welcome);
Screens.set(PROFILE_SCREEN, Profile);
Screens.set(PROFILE_EDIT_SCREEN, ProfileEdit);
Screens.set(EVENT_FORM, EventForm);
Screens.set(PHONE_LOGIN, PhoneLogin);
Screens.set(SETTINGS_SCREEN, Settings);
Screens.set(EXPLORE_SCREEN, Explore);
Screens.set(INBOX_SCREEN, Inbox);
Screens.set(EVENTS_SCREEN, Events);
Screens.set(ADD_TRIP_SCREEN, AddTrip);
Screens.set(TRIP_FORM_SCREEN, TripForm);
Screens.set(LOGIN_PROBLEMS, LoginProblems);
Screens.set(MAPDONUT_LOADING_OVERLAY, Loading);
Screens.set(MAPDONUT_CAMERA_OVERLAY, SelectPicture);
Screens.set(EVENT_FORM_CHANGE_LOCATION, EventsChangeLocation);
Screens.set(EVENT_SINGLE, Event);
Screens.set(SET_EMAIL, SetEmail);
Screens.set(SET_PHONE_NUMBER, SetPhoneNumber);
Screens.set(SELECT_LANGUAGE, SelectLanguage);
Screens.set(SELECT_THEME, SelectTheme);
Screens.set(SELECT_LOCATION, SelectLocation);
Screens.set(SELECT_DATETIME, SelectDatetime);
Screens.set(SET_PREFERENCES, Preferences);
Screens.set(SET_USERNAME, SetUsername);
Screens.set(CREATE_TRIP, CreateTrip);
Screens.set(SET_DESCRIPTION, SetDescription);

export const startApp = async() => {
    const overlay = await openLoadingOverlay();
    const u = firebase.auth().currentUser;


    StatusBar.setHidden(false, "none");
    const GOOGLE_MAPS_API_KEY = 'AIzaSyCkZe8aaPTh-6eYrVvLCfRu2aLsmeJEx9M';
    Geocoder.init(GOOGLE_MAPS_API_KEY);
    
    if (!u) await goToWelcome();
    else await goToHome(u);
    await closeLoadingOverlay(overlay);
};