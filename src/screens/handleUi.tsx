import { store } from "../redux/stores";
import { Platform, StatusBar } from "react-native";
import { Navigation, Options } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import { PROFILE_SCREEN, INBOX_SCREEN, EXPLORE_SCREEN, EVENTS_SCREEN, SETTINGS_SCREEN, WELCOME_SCREEN, PROFILE_EDIT_SCREEN, SET_PREFERENCES } from "./screenNames";
import firebase, { RNFirebase } from "react-native-firebase";
import { createFirebaseArrayUnion, createFirebaseTimestamp, createModal } from "../utils/utils";
import { Mapdonut_User } from "../@types/user";
import { currentUserActionSet } from "../redux/actions/userActions";
import DeviceInfo from 'react-native-device-info';
import CameraRoll from "@react-native-community/cameraroll";
import { pageInfo } from "../@types/camera";

const loadCameraRoll = async() => {
    const photos = await CameraRoll.getPhotos({
        first: 9
    });
    return {
        photos: photos.edges,
        cursor: photos.page_info as pageInfo
    };
}

export const goToHome = async (uF: RNFirebase.User) => {

    const userData = await saveInitialUser(uF);
    store.dispatch(currentUserActionSet(userData));
    store.dispatch({ type: 'CHANGE_LANGUAGE', payload: { lang: userData.languageSelected } });
    store.dispatch({ type: 'CHANGE_THEME', payload: { theme: userData.themeName } });

    const { themeVariables, theme } = store.getState().theme;
    const { words } = store.getState().language;

    StatusBar.setBarStyle(theme !== 'dark' ? 'dark-content' : 'light-content', true);

    const os = Platform.OS === 'ios' ? 'ios' : 'android';
    const ICON_TABBAR_SIZE = 30;

    const [
        userIcon, inboxIcon, exploreIcon, eventsIcon, settingsIcon
    ] = await Promise.all([
        Icon.getImageSource(`${os}-contact`, ICON_TABBAR_SIZE, '#000'),
        Icon.getImageSource(`${os}-chatbubbles`, ICON_TABBAR_SIZE, '#000'),
        Icon.getImageSource(`${os}-compass`, ICON_TABBAR_SIZE, '#000'),
        Icon.getImageSource(`${os}-calendar`, ICON_TABBAR_SIZE, '#000'),
        Icon.getImageSource(`${os}-cog`, ICON_TABBAR_SIZE, '#000'),
    ])
    Navigation.setDefaultOptions(themeVariables.RN as Options);

    const [rn_tabs] = await Promise.all([
        Navigation.setRoot({
            root: {
                bottomTabs: {
                    id: 'ROOT',
                    children: [
                        {
                            stack: {
                                children: [{
                                    component: {
                                        name: PROFILE_SCREEN,
                                        options: {
                                            topBar: {
                                                title: {
                                                    text: words.PROFILE
                                                }
                                            },
                                            statusBar: {
                                                visible: true
                                            }
                                        },
                                        passProps: {
                                            isCurrentUser: true,
                                            userId: '0',
                                            user: {}
                                        }
                                    },
                                }],
                                options: {
                                    bottomTab: {
                                        text: words.PROFILE,
                                        testID: 'PROFILE_SCREEN',
                                        icon: userIcon,
                                        ...(themeVariables.RN.bottomTab)
                                    },
                                },
                            },
                        },
                        /*
                        {
                            stack: {
                                children: [{
                                    component: {
                                        name: INBOX_SCREEN,
                                        options: {
                                            topBar: {
                                                title: {
                                                    text: words.INBOX
                                                },
                                                searchBar: true,
                                                searchBarPlaceholder: 'Search',
                                                hideNavBarOnFocusSearchBar: false,
                                            }
                                        }
                                    }
                                }],
                                options: {
                                    bottomTab: {
                                        text: words.INBOX,
                                        testID: 'PROFILE_SCREEN',
                                        icon: inboxIcon,
                                        ...(themeVariables.RN.bottomTab)
                                    },
                                },
                            },
                        },
                        */
                        {
                            stack: {
                                children: [{
                                    component: {
                                        name: EXPLORE_SCREEN,
                                        options: {
                                            topBar: {
                                                title: {
                                                    text: words.EXPLORE,
                                                },
                                                hideNavBarOnFocusSearchBar: false,
                                                searchBar: true,
                                                searchBarPlaceholder: 'Search',
                                                searchBarHiddenWhenScrolling: true,
                                            }
                                        }
                                    }
                                }],
                                options: {
                                    bottomTab: {
                                        text: words.EXPLORE,
                                        testID: 'Explore',
                                        icon: exploreIcon,
                                        ...(themeVariables.RN.bottomTab)
                                    },
                                },
                            },
                        }, {
                            stack: {
                                children: [{
                                    component: {
                                        name: EVENTS_SCREEN,
                                        options: {
                                            topBar: {
                                                visible: false,
                                                title: {
                                                    text: words.EVENTS
                                                }
                                            }
                                        }
                                    },
                                }],
                                options: {
                                    bottomTab: {
                                        text: words.EVENTS,
                                        testID: 'PROFILE_SCREEN',
                                        icon: eventsIcon,
                                        ...(themeVariables.RN.bottomTab)
                                    },
                                },
                            },
                        }, {
                            stack: {
                                children: [{
                                    component: {
                                        name: SETTINGS_SCREEN,
                                        options: {
                                            topBar: {
                                                title: {
                                                    text: words.SETTINGS
                                                }
                                            }
                                        }
                                    },
                                }],
                                options: {
                                    bottomTab: {
                                        text: words.SETTINGS,
                                        testID: 'SETTINGS_SCREEN',
                                        icon: settingsIcon,
                                        ...(themeVariables.RN.bottomTab)
                                    },
                                },
                            },
                        },
                    ]
                }
            }
        }),
    ]);

    const [cameraRoll, { statusBarHeight,
        topBarHeight,
        bottomTabsHeight
    }] = await Promise.all([
        loadCameraRoll(),
        Navigation.constants(),
        (userData.firstLogin ?
            Navigation.showModal({
                stack: {
                    children: [
                        {
                            component: {
                                name: SET_PREFERENCES,
                                passProps: {
                                },
                                options: {
                                    topBar: {
                                        barStyle: theme === 'dark' ? 'black' : 'default',
                                        animate: true,
                                        title: {
                                            text: words.PREFERENCES
                                        },
                                        rightButtons: [
                                            (!userData.username ? {
                                                id: `NEXT_${SET_PREFERENCES}`,
                                                text: words.NEXT,
                                            } : {
                                                    id: `CLOSE_${SET_PREFERENCES}`,
                                                    text: words.CLOSE,
                                                })
                                        ]
                                    }
                                }
                            }
                        },

                    ]
                }
            }) :
            !userData.username ?
                Navigation.showModal({
                    stack: {
                        children: [
                            {
                                component: {
                                    name: PROFILE_EDIT_SCREEN,
                                    passProps: {
                                        cancelButtonId: 'CANCEL_EDIT_PROFILE',
                                        doneButtonId: 'DONE_EDIT_PROFILE'
                                    },
                                    options: {
                                        topBar: {
                                            title: {
                                                text: words.PROFILE_EDIT_PROFILE
                                            },
                                            rightButtons: [
                                                {
                                                    id: 'DONE_EDIT_PROFILE',
                                                    text: words.DONE,
                                                }
                                            ]
                                        }
                                    }
                                }
                            },

                        ]
                    }
                }) :
                Promise.resolve()
        )
    ]);

    const rnHeights = {
        statusBarHeight: +statusBarHeight,
        topBarHeight: +topBarHeight,
        bottomTabsHeight: +bottomTabsHeight
    }

    store.dispatch({ type: 'SET_UI_HOME', payload: { rn_tabs, cameraRoll, rnHeights } });
}

export const goToWelcome = () => {

    StatusBar.setBarStyle('light-content', true);

    const [_lang, langs] = [DeviceInfo.getDeviceLocale(), DeviceInfo.getPreferredLocales()];
    const lang = _lang || (langs || []).length > 0 ? langs[0] : 'en';
    
    store.dispatch({ type: 'CHANGE_LANGUAGE', payload: { lang: lang } });

    return Navigation.setRoot({
        root: {
            stack: {
                id: 'WELCOME',
                children: [
                    {
                        component: {
                            name: WELCOME_SCREEN,
                            options: {
                                topBar: {
                                    visible: false
                                }
                            }
                        }
                    }
                ],
            }
        }
    });
}

async function saveInitialUser(user: RNFirebase.User) {
    const usersRef = firebase.firestore().collection('users').doc(user.uid);
    const _u = await usersRef.get({ source: 'server' });
    const initialData = {
        uid: user.uid,
        displayName: user.displayName || '',
        photoURL: (user.photoURL || '') + (!!user.providerData.find(p => p.providerId === 'facebook.com') ? '?height=700' : ''),
        coverURL: '',
        email: user.email || '',
        phoneNumber: user.phoneNumber || '',
        providerData: user.providerData,
        bio: '',
        username: '',
        socials: createFirebaseArrayUnion([]),
        themeName: 'default',
        languageSelected: 'en',
        location: null,
        lastSignInAt: createFirebaseTimestamp(),
        created_at: createFirebaseTimestamp(),
        updated_at: createFirebaseTimestamp(),
        firstLogin: true,
        emailVerified: false,
        allowLocation: false,
        allowPushNotifications: false,
        devices: [],
        preferences: []
    } as Mapdonut_User;
    const userData = _u.exists ? _u.data() as Mapdonut_User : initialData;
    if (!_u.exists) await usersRef.set(initialData);
    return Promise.resolve(userData);
}