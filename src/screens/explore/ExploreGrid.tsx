import React from 'React';
import { FlatList } from "react-native-gesture-handler";
import ExploreItem from "../components/ExploreItem/ExploreItem";
import { Mapdonut_ExploreItem } from '../../@types/explore';
import MasonryList from "react-native-masonry-list";
import { StoreApp } from '../../@types/store';
import { useSelector } from 'react-redux';
import { viewportWidth } from '../../utils/utils';

interface ExploreGridProps {
    data: Mapdonut_ExploreItem[],
    loadMore: () => void
}

export function ExploreGrid({ data, loadMore }: ExploreGridProps) {

    const { themeVariables } = useSelector((state: StoreApp) => state.theme);

    return <MasonryList
        images={data}
        containerWidth={viewportWidth}
        completeCustomComponent={(props: any) => <ExploreItem
            {...props}
            item={props.data}
        />}
        masonryFlatListColProps={{
            onEndReachedThreshold: 10,
            onEndReached: () => {
                if (loadMore) loadMore();
            }
        }}
        backgroundColor={themeVariables.BG_LAYOUT}
    />

    const a = () => <FlatList
        data={data}
        renderItem={(props) => {
            const {item, index} = props;
            return <ExploreItem item={item} index={index}/>
        }}
        columnWrapperStyle={{
            flex: 1
        }}
        numColumns={3}
        onEndReachedThreshold={10}
        onEndReached={() => {
            if (loadMore) loadMore();
        }}
    />
}
