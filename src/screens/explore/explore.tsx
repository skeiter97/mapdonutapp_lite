import React, { useRef, useState, useReducer, useEffect } from 'React';
import { ExploreGrid } from './ExploreGrid';

import { viewportWidth } from '../../utils/utils';
import MasonryList from "react-native-masonry-list";
import ExploreItem from '../components/ExploreItem/ExploreItem';
import { actionGeneral } from '../../redux/actions/general';
import useBindScreen, { useBindRNN } from '../hooks/useBindScreen';
import { getPhotosFromUnplashSearch } from '../../utils/unsplash';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../@types/store';
import { View, TextInput, Text, SafeAreaView } from 'react-native';
import { FakeToolbar } from '../components/FakeToolbar/FakeToolbar';
import { useInput } from '../hooks/useInput';
import { Mapdonut_ExploreItem } from '../../@types/explore';

export default function Explore(props: any) {

    const currentUser = useSelector((state: StoreApp) => state.currentUser.data);
    const preferences = currentUser.preferences.join(',');

    const [exploreItems, setExploreItems] = useState([] as Mapdonut_ExploreItem[]);
    const [pageUnplash, setPageUnplash] = useState(1);
    

    const resolveUnplashItems = async() => {
        if (pageUnplash >= 3) return; 
        const data = (await getPhotosFromUnplashSearch(preferences, pageUnplash, 60))
            .results
            .map((p: any, i: number) => {
                return {
                    id: p.id,
                    type: 'unplash',
                    url: p.urls.small,
                }
            }) as Mapdonut_ExploreItem[];
        setPageUnplash(pageUnplash + 1)
        setExploreItems(exploreItems.concat(data));
    }

    useEffect(() => {
        resolveUnplashItems();
    }, []);

    return <ExploreGrid data={exploreItems} loadMore={() => resolveUnplashItems()} />
}


function ExploreSearch() {
    const search = useInput('', 'search');
    return <TextInput {...search} />
}

/**  
 * 
 *
    

    useEffect(() => {
        loadPicsFromUnPlash();
    }, []);
 * 
 * 
 * 
 * 
 * 
 * <MasonryList
        images={data}
        containerWidth={viewportWidth}
        completeCustomComponent={(props: any) => <ExploreItem
            {...props}
            dispatch={dataDispatcher}
            item={data[props.data.index]}
        />}
        masonryFlatListColProps={{
            onEndReachedThreshold: 10,
            onEndReached: () => {
                //if (this.onEndReached) this.onEndReached();
            }
        }}
        backgroundColor={themeVariables.BG_LAYOUT}
    />
 * 
 * 
*/
