import React, { useRef, useEffect } from 'React';
import { Image, Animated, SafeAreaView } from 'react-native';
import styles from './loading.css';

export default function Loading() {

    const animScale = useRef(new Animated.Value(0.25)).current;
    const animOpacity = useRef(new Animated.Value(0.25)).current;

    let animValue = 0.5;

    const runAnimation = (v: number) => {
        Animated.parallel([
            Animated.timing(
                animScale,
                {
                    toValue: v,
                    duration: 200,
                    useNativeDriver: true
                }
            ),
            Animated.timing(
                animOpacity,
                {
                    toValue: v,
                    duration: 200,
                    useNativeDriver: true
                }
            )
        ])
            .start(() => {
                animValue = animValue > 1 ? 0 : animValue + 0.25;
                runAnimation(animValue);
            });
    }

    useEffect(() => {
        const timeout = setTimeout(() => runAnimation(animValue), 0);
        return () => clearTimeout(timeout);
    }, []);

    return <SafeAreaView className={styles.page}>
        <Animated.View style={{
            opacity: animOpacity.interpolate({
                inputRange: [0, 1],
                outputRange: [0.2, 0.8]
            }),
            transform: [{
                scale: animScale.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0.9, 1]
                })
            }]
        }}>
            <Image source={require('../../assets/mapdonut_logo.png')} className={styles.logo} />
        </Animated.View>
    </SafeAreaView>
}
