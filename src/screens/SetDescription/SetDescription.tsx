import React, { useRef, useEffect, useState } from 'React';
import { View, TextInput, KeyboardAvoidingView, Keyboard } from 'react-native';
import { Mapdonut_inputHook } from '../../@types/inputs';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../@types/store';
import useBindScreen from '../hooks/useBindScreen';
import { useInput } from '../hooks/useInput';
import { getBottomSpace, getStatusBarHeight } from 'react-native-iphone-x-helper'

interface props {
    inputField: Mapdonut_inputHook,
    [prop: string]: any
}

export default function SetDescription(props: props) {

    const inputRef = useRef<TextInput>(null);
    const input = useInput(props.inputField.value);
    const { theme, themeVariables } = useSelector((state: StoreApp) => state.theme);
    const [maxH, setMaxH] = useState(100);

    useBindScreen(this, props, {inputValue: input.value});

    this.componentDidAppear = () => {
        if (inputRef.current) inputRef.current.focus();
    }

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            (a) => setMaxH(a.endCoordinates.height || 300),
        );
        return () => keyboardDidShowListener.remove();
    })

    return <View style={{
        backgroundColor: themeVariables.BG_LAYOUT_ACCENT,
        padding: 16,
        maxHeight: maxH + getBottomSpace() + getStatusBarHeight(true) - 16
    }} >
        <TextInput
            ref={inputRef}
            {...input}
            style={{
                color: themeVariables.TEXT_COLOR,
            }}
            keyboardType={'default'}
            returnKeyType={"done"}
            autoCapitalize={"none"}
            keyboardAppearance={theme === 'dark' ? 'dark' : 'light'}
            multiline={true}
        />
    </View>
}