import React, { useRef, useState, useEffect } from 'React';
import { View, Text, Animated } from 'react-native';
import { viewportWidth, viewportHeight, createModal } from '../../utils/utils';
import Button from '../components/button';
import { Navigation } from 'react-native-navigation';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import styles from './events.css';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Carousel from 'react-native-snap-carousel';
import EventCard from '../components/EventCard/EventCard';
import { PanGestureHandler} from 'react-native-gesture-handler';
import useCurrentCity from '../hooks/useEvents';
import { Mapdonut_Event } from '../../@types/event';
import { useSelector } from 'react-redux';
import { EVENT_SINGLE, EVENT_FORM, SELECT_LOCATION } from '../screenNames';
import { StoreApp } from '../../@types/store';
import AsyncStorage from '@react-native-community/async-storage';
import { useBindRNN } from '../hooks/useBindScreen';

export default function Events(props: any) {

    const { theme, themeVariables } = useSelector((state: StoreApp) => state.theme);
    const { words } = useSelector((state: StoreApp) => state.language);
    const { nearByEvents } = useSelector((state: StoreApp) => state.events);

    const carousel = useRef();
    const mapDOM = useRef<MapView>(null);

    const bottomContentRef = useRef<View>(null);
    const [bottomContentHeight, setBottomContentHeight] = useState(0);

    const [mapInitialized, setMapInitialized] = useState(false);

    const currentCity = useCurrentCity(mapDOM.current);
    const translateY = useRef(new Animated.Value(0)).current;

    const [bottomBar, setBottomBar] = useState(0);

    useEffect(() => {
        if (bottomContentRef.current && bottomContentHeight === 0) bottomContentRef.current.measure((x, y, w, h) => {
            setBottomContentHeight(h);
            const MAX_HEIGHT = 200;
            if (h > MAX_HEIGHT) Animated.timing(translateY, {
                toValue: MAX_HEIGHT,
                duration: 200,
                useNativeDriver: true
            });
        });
        Navigation.constants().then(c => setBottomBar(c.bottomTabsHeight));
    }, []);

    useEffect(() => {
        if (mapDOM.current) currentCity.getCurrentLocation();
    }, [mapDOM.current]);

    const _onPressNewEvent = () => createModal(
        EVENT_FORM,
        { isDarkThem: theme === 'dark', done: () => { }, noSearchBar: true },
        { topBarTitle: words.NEW_EVENT },
        words
    );

    const _changeLocation = () => createModal(
        SELECT_LOCATION,
        {
            isDarkThem: theme === 'dark',
            selected: currentCity.placeId,
            selectLocation: currentCity.changeCity,
            gpOptions: { type: 'cities'},
            setPrevItems: async() => {
                const _lastCities = (await AsyncStorage.getItem('@lastCities')) || JSON.stringify([]);
                return [{
                    title: words.CURRENT_LOCATION,
                    id: 'CURRENT_LOCATION',
                    icon: 'location-arrow'
                }].concat(JSON.parse(_lastCities).reverse());
            }
        },
        { topBarTitle: words.SELECT_CITY },
        words
    );

    useBindRNN(this, props);

    return <View style={{ height: viewportHeight, width: '100%' }}>

        <MapView
            provider={PROVIDER_GOOGLE}
            style={[styles.map, { height: viewportHeight - bottomBar }]}
            showsUserLocation={false}
            ref={mapDOM}
        >
            {currentCity.currentEvents.map((ev, i) => (
                <Marker
                    key={i}
                    coordinate={{ latitude: ev.location.latitude, longitude: ev.location.longitude }}
                    title={ev.title}
                    description={ev.description}
                />
            ))}
        </MapView>
        
        <View className={styles.mapHeaderOptions}>
            <View style={{ flex: 1 }}>
            </View>
            <View style={{flex: 5}}>
                <Button onPress={_changeLocation}>
                    <View className={styles.mapHeaderOptionsLocation} style={{ backgroundColor: themeVariables.BG_LAYOUT_ACCENT }}>
                        <Icon name={'sort-down'} size={14} color={themeVariables.TEXT_COLOR} />
                        <Text style={{ color: themeVariables.TEXT_COLOR }} > {currentCity.name} </Text>
                    </View>
                </Button>
            </View>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
                <Button>
                    <View className={styles.mapHeaderOptionsButton} style={{ backgroundColor: themeVariables.BG_LAYOUT_ACCENT }} >
                        <Icon name={'search'} size={16} color={themeVariables.TEXT_COLOR} />
                    </View>
                </Button>
            </View>
        </View>

        <PanGestureHandler
            onGestureEvent={
                //Animated.event([{ nativeEvent: { translationY: translateY } }], { useNativeDriver: true })
                ({ nativeEvent }) => {
                    //console.log(bottomContentHeight, nativeEvent);
                    const v = nativeEvent.translationY;
                    const newValue = v < 20 ? 0 : v;
                    Animated.timing(translateY, {
                        toValue: newValue,
                        duration: 0,
                        useNativeDriver: true
                    })
                        .start();
                }
            }
        >
            <Animated.View
                className={styles.mapContent}
                style={[
                    {
                        backgroundColor: themeVariables.BG_LAYOUT_ACCENT,
                        shadowOffset: { width: 0, height: 2 },
                        shadowColor: 'black',
                        shadowOpacity: 0.5,
                        shadowRadius: 8,
                        bottom: bottomBar,
                        transform: [{
                            translateY: translateY.interpolate({
                                inputRange: [-100, 20, 100],
                                outputRange: [20, 0, 50],
                            })
                        }]
                    },
                ]}
            >

                <View ref={bottomContentRef}>
                    <View className={styles.mapContentTitleWrapper}>
                        <Button onPress={_changeLocation}>
                            <View className={styles.mapContentTitle}>
                                <Icon name={'sort-down'} size={18} color={themeVariables.TEXT_COLOR} />
                                <Text className={styles.mapContentTitleText} style={{ color: themeVariables.TEXT_COLOR }} > {currentCity.name} </Text>
                            </View>
                        </Button>
                    </View>
                    <View className={styles.mapContentHeader}>
                        <Button onPress={_onPressNewEvent}>
                            <View className={styles.mapContentHeaderButton} style={{ backgroundColor: themeVariables.PRIMARY_COLOR }}>
                                <Icon name={'plus'} size={14} color={themeVariables.TEXT_COLOR} />
                                <Text className={styles.mapContentHeaderButtonText} style={{ color: themeVariables.TEXT_COLOR }} > { words.CREATE } </Text>
                            </View>
                        </Button>
                    </View>

                    {currentCity.currentEvents.length > 0 ? <View style={{ padding: 16 }}>
                        <Text className={[styles.eventsSubTitle, styles[`${theme}__eventsSubTitle`]]}> {words.EVENTS_IN_THE_AREA} </Text>
                        <Carousel
                            ref={carousel}
                            data={currentCity.currentEvents}
                            renderItem={({ item }: { item: Mapdonut_Event }) => <EventCard item={item} onPress={() => {
                                Navigation.push(props.componentId, {
                                    component: {
                                        name: EVENT_SINGLE,
                                        passProps: {
                                            item: item
                                        },
                                        options: {
                                            topBar: {
                                                title: {
                                                    text: item.title
                                                },
                                            }
                                        }
                                    }
                                });
                            }} />}
                            sliderWidth={ viewportWidth - 32 }
                            firstItem={0}
                            itemWidth={ (viewportWidth - 32) * 0.7 }
                            inactiveSlideScale={1}
                        />
                    </View> : null }

                    <View>

                    </View>
                    
                </View>
            
            </Animated.View>
        </PanGestureHandler>
    </View>
}

/***
 * 
 *
                        <Button onPress={() => {

                        }}>
                            <View className={styles.mapContentHeaderButton} style={{ backgroundColor: themeVariables.PRIMARY_COLOR }}>
                                <Icon name={'plus'} size={14} color={themeVariables.TEXT_COLOR} />
                                <Text className={styles.mapContentHeaderButtonText} style={{ color: themeVariables.TEXT_COLOR }}> My Events </Text>
                            </View>
                        </Button>
                        <Button onPress={_onPress}>
                            <View className={styles.mapContentHeaderButton} style={{ backgroundColor: themeVariables.PRIMARY_COLOR }}>
                                <Icon name={'plus'} size={14} color={themeVariables.TEXT_COLOR} />
                                <Text className={styles.mapContentHeaderButtonText} style={{ color: themeVariables.TEXT_COLOR }}> Saved </Text>
                            </View>
                        </Button>
*/
