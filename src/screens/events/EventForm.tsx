import React, { useEffect, useState, useContext, useRef } from 'react';
import { ScrollView, View, Text, TextInput, Alert } from 'react-native';
import { closeModal, createModal, openLoadingOverlay, closeLoadingOverlay, createFirebaseTimestamp, createFirebaseGeopoint, formateDate } from '../../utils/utils';
import { Navigation } from 'react-native-navigation';
import firebase from 'react-native-firebase';
import uuidv5 from 'uuid/v5';
import styles from './EventForm.css';
import Button from '../components/button';
import RNGooglePlaces, { GMSTypes } from 'react-native-google-places';
import { TapGestureHandler, State, Switch } from 'react-native-gesture-handler';
import { Mapdonut_Event } from '../../@types/event';
import { useSwitch } from '../hooks/useSwitch';
import CoverTitle from '../components/CoverAndTitle/CoverAndTitle';
import { useSelector, useDispatch } from 'react-redux';
import { StoreApp } from '../../@types/store';
import { SELECT_LOCATION, SELECT_DATETIME } from '../screenNames';
import { formatRelative, setHours, setMinutes, getHours, getMinutes, addHours } from 'date-fns';
import { useInput } from '../hooks/useInput';
import { eventsActionAdd } from '../../redux/actions/eventsActions';
import { ItemSearchBarList } from '../../@types/searchBarList';
import { uploadPictureFirebaseStorage } from '../../utils/uploadMedia';

interface Props {
    id?: string;
    event?: Mapdonut_Event;
    cancelButtonId: string,
    doneButtonId: string,
    componentId: string,
}

export default function EventForm(props: Props) {

    const dispatch = useDispatch();

    const currentUser = useSelector((state: StoreApp) => state.currentUser.data);
    const { words, lang } = useSelector((state: StoreApp) => state.language);
    const { theme, themeVariables } = useSelector((state: StoreApp) => state.theme);

    const description = useInput(props.event && props.event.description ? props.event.description : '', words.DESCRIPTION);
    const title = useInput(props.event && props.event.title ? props.event.title : '');
    const ticketsUrl = useInput(props.event && props.event.ticketsUrl ? props.event.ticketsUrl : '', words.TICKETS_URL);
    const [locationId, setLocationId] = useState('');
    const locationText = useInput(props.event && props.event.locationText ? props.event.locationText : '', words.LOCATION);
    const allowComments = useSwitch(true);
    const [datetime, setDatetime] = useState(props.event ? props.event.datetime.toDate() : addHours(new Date, 3));
    const [photoUrl, setPhotoUrl] = useState(props.event && props.event.photoURL ? props.event.photoURL : '');

    const uuidEvent = props.event ?
        props.event.id : uuidv5(`Event: ${currentUser.displayName} | ${Date.now().toString()} `, uuidv5.URL);

    const savePhoto = (prePhotoUrl: string, uuidEvent: string) => {
        if (!prePhotoUrl) return Promise.resolve('');
        const defaultStorage = firebase.storage();
        const fileRef = defaultStorage.ref(`events/${uuidEvent}`);
        return fileRef.putFile(prePhotoUrl)
            .then(res => (res.downloadURL || ''));
    };

    //const saveEvent = (obj: Partial<Mapdonut_Event>, uuidEvent: string) => {
    //    const fRef = firebase.firestore().collection('events').doc(uuidEvent);
    //    const data = { ...(props.id ? props.event : {}), ...obj }
    //    return fRef[props.id ? 'update' : 'set'](data);
    //};

    const saveData = async () => {

        if (!locationText.value) {
            Alert.alert('Location is required');
            return;
        }

        closeModal(props.componentId)
        //const overlay = await openLoadingOverlay();

        const [locationPosition] = await Promise.all([
            //(!!photoUrl ? uploadPictureFirebaseStorage(photoUrl, `events/${uuidEvent}`) : Promise.resolve()),
            RNGooglePlaces.lookUpPlaceByID(locationId)
        ]);

        const data: Mapdonut_Event = {
            title: title.value,
            description: description.value,
            userUid: props.event ? props.event.userUid : currentUser.uid,
            locationText: locationText.value,
            privacy: 'public',
            allowComments: allowComments.value,
            datetime: createFirebaseTimestamp(datetime),
            createdAt: createFirebaseTimestamp(),
            location: createFirebaseGeopoint(locationPosition.location.latitude, locationPosition.location.longitude),
            ticketsUrl: '',
            //photoURL: photoUrl ? photoUrl : ''
            photoURL: '' //Todo
        }

        dispatch(eventsActionAdd(data));

    };

    useEffect(() => {
        const navigationButtonEventListener = Navigation.events()
            .registerNavigationButtonPressedListener(({ buttonId, componentId }) => {
                if (buttonId === props.cancelButtonId) closeModal(componentId);
                else if (buttonId === props.doneButtonId) saveData();
            });
        return () => {
            // console.log('REMOVE navigationButtonEventListener');
            navigationButtonEventListener.remove();
        }
    });

    const selectLocation = (item: ItemSearchBarList) => {
        locationText.setValue(`${item.title}, ${item.subtitle}`);
        setLocationId(item.id);
    };

    const changeDateTime = ({date, time}: any) => {
        let modified = setHours(date, getHours(time));
        modified = setMinutes(modified, getMinutes(time))
        setDatetime(modified);
    };

    return <ScrollView>
        <CoverTitle
            placeholder={words.EVENT_TITLE} title={title.value} setTitle={title.setValue} coverURL={photoUrl} setCoverURL={setPhotoUrl}
            path={`events/${uuidv5(`Event: ${Date.now().toString()} | ${currentUser.displayName}`, uuidv5.URL)}`}
        />

        <View style={{ marginTop: 16 }}></View>

        <View style={{ backgroundColor: themeVariables.BG_LAYOUT_ACCENT }}>

            <View className={styles.inputWrapper}>
                <View className={styles.inputWrapperInput}>
                    <TextInput {...description} multiline={true} style={{ color: themeVariables.TEXT_COLOR }} />
                </View>
            </View>

            <View className={styles.hr} style={{backgroundColor: themeVariables.BG_LAYOUT}}></View>

            <View className={styles.inputWrapper} >
                <View className={styles.inputWrapperInput}>
                    <Button onPress={() => createModal(SELECT_DATETIME, { isDarkThem: theme === 'dark' , noSearchBar: true, datetime, done: changeDateTime }, { topBarTitle: words.SET_DATETIME }, words)}>
                        <TextInput value={formateDate(datetime, lang)} editable={false} style={{ color: themeVariables.TEXT_COLOR }} />
                    </Button>
                </View>
            </View>

            <View className={styles.hr} style={{backgroundColor: themeVariables.BG_LAYOUT}}></View>

            <View className={styles.inputWrapper} >
                <View className={styles.inputWrapperInput}>
                    <Button onPress={() => createModal(SELECT_LOCATION, { isDarkThem: theme === 'dark' , selectLocation }, { topBarTitle: words.SET_LOCATION }, words)}>
                        <TextInput {...locationText} editable={false} style={{ color: themeVariables.TEXT_COLOR }} />
                    </Button>
                </View>
            </View>

            <View className={styles.hr} style={{backgroundColor: themeVariables.BG_LAYOUT}}></View>

            <View className={styles.inputWrapper}>
                <View className={styles.inputWrapperInput}>
                    <TextInput {...ticketsUrl} style={{ color: themeVariables.TEXT_COLOR }} />
                </View>
            </View>
        </View>

        <View style={{ marginTop: 16 }}></View>
        <View style={{ backgroundColor: themeVariables.BG_LAYOUT_ACCENT }}>
            <View className={styles.inputWrapper}>
                <Text className={styles.inputWrapperLabelTitle} style={{ color: themeVariables.TEXT_COLOR }}> {words.ALLOW_COMMENTS } </Text>
                <View className={styles.inputWrapperInput}>
                    <Switch {...allowComments} />
                </View>
            </View>
        </View>
        
    </ScrollView>
}
