import React, { useState, useRef } from 'React';
import { View, Text, Image } from 'react-native';
import { viewportHeight, wp, viewportWidth } from '../../utils/utils';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import styles from './_welcomeSlider.css';
import stylesButtons from '../../styles/buttons.css';
import Button from '../components/button';
import { StoreApp } from '../../@types/store';
import { useSelector } from 'react-redux';

export default function WelcomeSlider(props: any) {

    const slideWidth = wp(75);
    const itemHorizontalMargin = wp(2);

    const sliderWidth = viewportWidth;
    const itemWidth = slideWidth + itemHorizontalMargin * 2;

    const { words } = useSelector((state: StoreApp) => state.language);
    const carouselRef = useRef<Carousel>(null);

    const [sliderActiveSlide, setSliderActiveSlide] = useState(0);
    const data = ([
        {
            title: words.TRAVEL_JOURNAL,
            description: words.TRAVEL_JOURNAL_DESCRIPTION,
            img: require('../../assets/welcome/onboarding-explore.png'),
        },
        {
            title: words.WELCOME_CONNECT_WORLD,
            description: words.WELCOME_CONNECT_WORLD_DESCRIPTION,
            img: require('../../assets/welcome/onboarding-explore-2.png'),
        },
        {
            title: '',
            description: ``,
            img: require('../../assets/welcome/backpack.png'),
        },
        {
            title: words.WELCOME_LINK,
            description: words.WELCOME_LINK_DESCRIPTION,
            img: require('../../assets/welcome/onboarding-conection.png'),
            imgIsRectangle: true
        },
        {
            title: words.WELCOME_READY_TO_TRAVEL,
            description: words.WELCOME_READY_TO_TRAVEL_DESCRIPTION,
            img: require('../../assets/welcome/onboarding-travel.png'),
        },
        {
            title: words.WELCOME_EVENTS,
            description: words.WELCOME_EVENTS_DESCRIPTION,
            img: require('../../assets/welcome/onboarding-events.png'),
        }
    ]);

    const _renderItem = ({ item }: any) => <View
        className={item.imgIsRectangle ? styles.slideContentRectangle : styles.slideContent}
        style={{ height: viewportHeight * 0.54 }}
    >
        <Image source={item.img} className={item.imgIsRectangle ? styles.slideImageRectangle : styles.slideImage} />
        <View>
            <Text className={styles.slideTitle}> {item.title} </Text>
            <Text className={styles.slideDescription}> {item.description} </Text>
        </View>
    </View>;

    return <View >

        <View style={{ height: viewportHeight * 0.54 }}>
            <Carousel
                ref={carouselRef}
                data={data}
                renderItem={_renderItem}
                sliderWidth={sliderWidth}
                itemWidth={itemWidth}
                onSnapToItem={(index: any) => setSliderActiveSlide(index)}
            />
        </View>

        <Pagination
            dotsLength={data.length}
            activeDotIndex={sliderActiveSlide}
            containerStyle={styles.paginationContainer}
            dotColor={'white'}
            dotStyle={styles.paginationDot}
            inactiveDotColor={'#eee'}
            inactiveDotOpacity={0.3}
            inactiveDotScale={0.8}
            carouselRef={carouselRef.current}
            tappableDots={!!carouselRef.current}
        />

        <View style={{ flexDirection: 'row', justifyContent: 'center', padding: 16 }}>
            <Button onPress={() => props.skipWelcomeFn()}>
                <View className={stylesButtons.btn} >
                    <Text className={stylesButtons.btnText}> {words.SKIP_TUTORIAL} </Text>
                </View>
            </Button>
        </View>
    </View>
}
