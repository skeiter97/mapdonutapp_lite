import React, { useRef, useEffect } from 'React';
import { View, Text, Image, Animated } from 'react-native';
import Button from '../components/button';
import { createModal, viewportHeight } from '../../utils/utils';
import { SELECT_LANGUAGE } from '../screenNames';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../@types/store';
import styles from './_logo.css';
import { REGULAR_ANIMATION_TIMING } from '../../global/variables';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default function WelcomeLogo({ mainView, showWelcome, showLogin }: any) {

    const CONTENT_HEIGHT_DIFF_LOGIN = -60;

    const logoLimits = { hide: ((viewportHeight / 2) + 150) * -1, show: 0, inLogin: ((CONTENT_HEIGHT_DIFF_LOGIN)) };

    const { words } = useSelector((state: StoreApp) => state.language);
    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    const { lang } = useSelector((state: StoreApp) => state.language);

    const contentOpacity = useRef(new Animated.Value(0)).current;
    const logoTranslate = useRef(new Animated.Value(logoLimits.show)).current;

    const showContent = (show: boolean = true) => {
        return Animated.parallel([
            Animated.timing(
                contentOpacity,
                {
                    toValue: show ? 1 : 0,
                    duration: REGULAR_ANIMATION_TIMING,
                    useNativeDriver: true,
                }
            )
        ]);
    };

    const showLogo = (show: boolean = true, type?: string) => {
        return Animated.parallel([
            Animated.timing(
                logoTranslate,
                {
                    toValue: logoLimits[type === 'inLogin' ? 'inLogin' : show ? 'show' : 'hide'],
                    duration: REGULAR_ANIMATION_TIMING,
                    useNativeDriver: true
                }
            )
        ])
    };

    useEffect(() => {
        if (mainView === 'logo') Animated.parallel([
            //Animated.delay(REGULAR_ANIMATION_TIMING * 4),
            //Animated.parallel([
                showLogo(),
                showContent()
            //])
        ]).start();
        else if (mainView === 'login') Animated.parallel([
            showLogo(true, 'inLogin'),
            showContent(false)
        ]).start();
        else Animated.parallel([
            showLogo(false),
            showContent(false)
        ]).start();
    }, [mainView]);

    return <Animated.View style={{
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        position: 'absolute',
        left: 0,
        top: 0,
        width: '100%',
        transform: [{
            translateY: logoTranslate
        }]
    }}>
        <Text style={styles.appTitle}> MapDonut </Text>
        <View style={{ marginBottom: 8 }}></View>
        <Image source={require('../../assets/mapdonut_logo.png')} className={styles.logo} />

        <Animated.View style={{
            opacity: contentOpacity,
        }}>
            <View style={{ marginBottom: 16 }}></View>

            <Button onPress={() => createModal(SELECT_LANGUAGE, {}, { topBarTitle: words.SELECT_LANGUAGE }, words)}>
                <View style={{ padding: 16, flexDirection: "row", justifyContent: "center", borderRadius: 16 }}>
                    <Text style={{ color: themeVariables.WELCOME_TEXT_COLOR }}> {words.SELECT_LANGUAGE} </Text>
                    <Text style={{ color: themeVariables.WELCOME_TEXT_COLOR, fontWeight: "bold" }} > ({words[`LANG_${lang.toUpperCase()}`]}) </Text>
                    <Icon name={'sort-down'} size={16} color={themeVariables.WELCOME_TEXT_COLOR} style={{top: -2}} />
                </View>
            </Button>

            <View style={{ marginBottom: 16 }}></View>
            <ButtonInitial text={words.CHECK_TUTORIAL} onPress={showWelcome} />
            <View style={{ marginBottom: 16 }}></View>
            <ButtonInitial text={words.GO_LOGIN} onPress={showLogin} />
        </Animated.View>
    </Animated.View>;
}

function ButtonInitial({text, onPress}: any) {
    return <Button onPress={onPress}>
        <View style={{
            borderColor: '#ccc', borderWidth: 2, backgroundColor: '#fff',
            paddingVertical: 8, borderRadius: 16
        }}>
            <Text style={{textAlign: "center"}}> {text} </Text>
        </View>
    </Button>
}