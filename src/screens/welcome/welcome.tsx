import React, { useState, useEffect, useRef } from 'React';
import { Animated, SafeAreaView, View, Text } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import WelcomeSlider from './_welcomeSlider';
import Login from '../login/login';
import { REGULAR_ANIMATION_TIMING } from '../../global/variables';
import styles from './welcome.css';
import { viewportHeight, viewportWidth, createModal } from '../../utils/utils';
import WelcomeLogo from './_logo';
import { FakeToolbar } from '../components/FakeToolbar/FakeToolbar';
import { useSelector } from 'react-redux';
import { StoreApp } from '../../@types/store';
import Button from '../components/button';
import { SELECT_LANGUAGE } from '../screenNames';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default function Welcome(props: any) {

    const { themeVariables } = useSelector((state: StoreApp) => state.theme);
    const { words } = useSelector((state: StoreApp) => state.language);
    const { lang } = useSelector((state: StoreApp) => state.language);


    const LOGO_HEIGHT = 300;

    const logoLimits = { hide: ((viewportHeight / 2) + 150) * -1, show: 0, inLogin: (LOGO_HEIGHT / 2) * -1 };
    const welcomeLimits = { hide: viewportWidth - 39, show: 0 };
    const loginLimits = { hide: viewportHeight / 2 + 200, show: LOGO_HEIGHT / 3  };

    const [mainView, setMainView] = useState('');
    const logoTranslate = useRef(new Animated.Value(logoLimits.show)).current;
    const slidersTranslate = useRef(new Animated.Value(welcomeLimits.hide)).current;
    const slidersOpacity = useRef(new Animated.Value(0)).current;
    const loginTranslate = useRef(new Animated.Value(loginLimits.hide)).current;
    const loginOpacity = useRef(new Animated.Value(0)).current;

    const showWelcome = (show: boolean = true) => {
        return Animated.parallel([
            Animated.timing(
                slidersTranslate,
                {
                    toValue: welcomeLimits[show ? 'show' : 'hide'],
                    duration: REGULAR_ANIMATION_TIMING,
                    useNativeDriver: true
                }
            ),
            Animated.timing(
                slidersOpacity,
                {
                    toValue: show ? 1 : 0,
                    duration: REGULAR_ANIMATION_TIMING,
                    useNativeDriver: true,
                }
            )
        ])
    };

    const showLogin = (show: boolean = true) => {
        return Animated.parallel([
            Animated.timing(
                loginTranslate,
                {
                    toValue: loginLimits[show ? 'show' : 'hide'],
                    duration: REGULAR_ANIMATION_TIMING,
                    useNativeDriver: true,
                }
            ),
            Animated.timing(
                loginOpacity,
                {
                    toValue: show ? 1 : 0,
                    duration: REGULAR_ANIMATION_TIMING,
                    useNativeDriver: true,
                }
            )
        ]);
    };

    const showLogo = (show: boolean = true, type: string = '') => {
        return Animated.parallel([
            Animated.timing(
                logoTranslate,
                {
                    toValue: logoLimits[type === 'inLogin' ? 'inLogin' : show ? 'show' : 'hide'],
                    duration: REGULAR_ANIMATION_TIMING,
                    useNativeDriver: true
                }
            )
        ])
    };

    useEffect(() => {
        AsyncStorage.getItem('@APP_FirstLoad')
            .then(v => !v ?
                AsyncStorage.setItem('@APP_FirstLoad', JSON.stringify(true)) :
                Promise.resolve()
            );
        setMainView('logo');
    }, []);

    useEffect(() => {
        if (mainView === 'logo') Animated.sequence([
            Animated.delay(REGULAR_ANIMATION_TIMING * 4),
            Animated.parallel([
                //props.skipWelcome ? showLogo(true, 'inLogin') : showLogo(false),
                //props.skipWelcome ? showLogin() : showWelcome()
            ])
        ])
            .start();
        else if (mainView === 'login') Animated.parallel([
            showLogo(true, 'inLogin'),
            showLogin(),
            showWelcome(false)
        ])
            .start();
        else if (mainView === 'welcome') Animated.parallel([
            showLogo(false),
            showLogin(false),
            showWelcome(true)
        ])
            .start();
    }, [mainView]);

    return <LinearGradient colors={themeVariables.WELCOME_BG_GRADIANT} className={styles.page}>

        <FakeToolbar
            right={ mainView === 'logo' ? <View /> : <Button onPress={() => createModal(SELECT_LANGUAGE, {}, { topBarTitle: words.SELECT_LANGUAGE }, words)}>
                <View style={{ flexDirection: "row", justifyContent: "center" }}>
                    <Text style={{ color: themeVariables.WELCOME_TEXT_COLOR, fontWeight: "bold" }} > ({words[`LANG_${lang.toUpperCase()}`]}) </Text>
                    <Icon name={'sort-down'} size={16} color={themeVariables.WELCOME_TEXT_COLOR} style={{ top: -2 }} />
                </View>
            </Button>}>
        </FakeToolbar> 

        <WelcomeLogo mainView={mainView} showWelcome={() => setMainView('welcome')} showLogin={() => setMainView('login')} />

        <Animated.View style={{
            opacity: loginOpacity,
            transform: [{
                translateY: loginTranslate
            }]
        }}>
            <Login showWelcome={() => setMainView('welcome')} />
        </Animated.View>

        <Animated.View style={{
            ...styles.logoWrapper,
            opacity: slidersOpacity,
            transform: [{
                translateX: slidersTranslate
            }]
        }}>
            <WelcomeSlider skipWelcomeFn={() => setMainView('login')} />
        </Animated.View>

    </LinearGradient>
}
