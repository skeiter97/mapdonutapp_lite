import Unsplash from 'unsplash-js/native';
import { toJson } from "unsplash-js";

const unsplash = new Unsplash({
    applicationId: "c1d37bb1ed5a0c7312ab8a0000487c17b3d1aae347bb84e0954abc5f44be87b3",
    secret: "3175dd84d904fba229ab1b42bd267f3aa8303b20f1683c27c6058d757d20a6e1",
});

export const getPhotosFromUnplash = (querySearch: string, page: number = 1, perPage: number = 10) => unsplash
    .collections.getCollectionPhotos(2143051, page, perPage, 'popular')
        .then(toJson);

export const getPhotosFromUnplashSearch = (querySearch: string, page: number = 1, perPage: number = 15) => unsplash
    .search.photos(querySearch, page, perPage)
    .then(toJson);