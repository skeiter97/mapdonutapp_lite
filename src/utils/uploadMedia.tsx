import firebase, { RNFirebase } from 'react-native-firebase';
import { MapdonutFirebaseStorage } from '../@types/general';

export function uploadPictureFirebaseStorage(uri: string, _path: string, onProgres?: (percentage: number, snapshot?: any) => void): Promise<MapdonutFirebaseStorage> {
    
    if (!uri) return Promise.reject({code: 'NO_URI', message: 'Send a URI/Photo'});
    
    return new Promise((resolve, reject) => {
        const defaultStorage = firebase.storage();
        const fileRef = defaultStorage.ref(_path);
        const taskUpload = fileRef.putFile(uri);
        taskUpload.on(
            firebase.storage.TaskEvent.STATE_CHANGED,
            snapshot => {
                const _progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                if (onProgres) onProgres(_progress, snapshot);
                if (snapshot.state === firebase.storage.TaskState.SUCCESS) {
                    if (onProgres) onProgres(100, snapshot);
                    resolve({
                        url: snapshot.downloadURL || '',
                        id: _path,
                        metadata: { ...snapshot.metadata, isLoadedToCloud: true, progress: 100}
                    });
                }
            },
            error => {
                reject(error);
            }
        );
    });
}