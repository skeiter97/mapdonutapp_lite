import { Dimensions } from 'react-native';
import { Navigation } from 'react-native-navigation';
import firebase from 'react-native-firebase';
import { Mapdonut_User } from '../@types/user';
import { useState } from 'react';
import { MAPDONUT_LOADING_OVERLAY } from '../screens/screenNames';
import { DEFAULT_LOCATION } from '../global/variables';

const { width: _viewportWidth, height: _viewportHeight } = Dimensions.get('window');

export function wp(percentage: number, orientation: string = 'horizontal') {
    const value = (percentage * (orientation === 'horizontal' ? _viewportWidth : _viewportHeight)) / 100;
    return Math.round(value);
}

export const viewportWidth = _viewportWidth;
export const viewportHeight = _viewportHeight

export const openLoadingOverlay = () => Navigation.showOverlay({
    component: {
        name: MAPDONUT_LOADING_OVERLAY,
        //name: 'mapdonut.LoadingOverlay',
        options: {
            overlay: {
                interceptTouchOutside: true
            }
        }
    }
});

export const closeLoadingOverlay = (componentName: string) => {
    Navigation.dismissOverlay(componentName);
}

export const closeModal = (componentId: string) => Navigation.dismissModal(componentId);

export function useInput(initialValue: string = '') {
    const [value, setValue] = useState(initialValue);
    const onChangeText = (text: string) => setValue(text);
    return { value, onChangeText, setValue };
}

export function dateToUTC(d?: Date) {
    var date = d ? d :new Date();
    var utcDate = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
        date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    return new Date(utcDate);
}

export const saveExtraDataUser = (uid: string, extra: Partial<Mapdonut_User>) => {
    const usersRef = firebase.firestore().collection('users').doc(uid);
    return usersRef.update(extra);
}

const getRef = (collectionName: string, uuid: string) => firebase.firestore().collection(collectionName).doc(uuid);

export const saveDataInFirebase = (collectionName: string, uuid: string, extra: simpleObjectAnyValues) => {
    const collectionRef = getRef(collectionName, uuid);
    return collectionRef.set(extra);
}

export const updateDataInFirebase = (collectionName: string, uuid: string, extra: simpleObjectAnyValues) => {
    const collectionRef = getRef(collectionName, uuid);
    return collectionRef.update(extra);
}


export function watchUser(uid: string, fn: (r: Mapdonut_User) => void) {
    const ref = firebase.firestore().collection('users').doc(uid);
    return ref.onSnapshot(snap => {
        const _user = snap.data() as Mapdonut_User;
        if (!_user) return;
        fn(_user);
    });
}

export function createFirebaseTimestamp(date?: Date) {
    return firebase.firestore.Timestamp.fromDate(date ? dateToUTC(date) : dateToUTC());
}

export function createFirebaseArrayUnion(params: any[]) {
    return firebase.firestore.FieldValue.arrayUnion(...params)
}

export function createFirebaseTimestampFromSeconds(seconds: number) {
    return firebase.firestore.Timestamp.fromMillis(seconds * 1000);
}

export function createFirebaseGeopoint(lat: number, lng: number) {
    return new firebase.firestore.GeoPoint(lat, lng);
}

export function createFirebaseGeopointDefault() {
    return new firebase.firestore.GeoPoint(DEFAULT_LOCATION.lat, DEFAULT_LOCATION.lng);
}

export const isEmpty = (obj: any) => [ Object, Array ].includes((obj || {}).constructor) && !Object.entries((obj || {})).length;

export const isDefined = (obj: object, key: string) => {
    if (!obj) return false;
    return Object.keys(obj).findIndex(k => k === key) !== -1;
}

import { formatRelative, setHours, setMinutes, getHours, getMinutes, addHours } from 'date-fns';
import {
    enUS,
    es,
    zhCN,
    zhTW,
    ar,
    bn,
    de,
    it,
    ja,
    pt,
    ru,
    tr
} from 'date-fns/locale'
import { ItemSearchBarList } from '../@types/searchBarList';
import { simpleObjectAnyValues } from '../@types/general';

export function getDateFnsLocale(l = 'en') {
    return l === 'es' ? es :
        l === 'zh-Hant' ? zhCN :
        l === 'zh-Hans' ? zhTW :
        l === 'ar' ? ar :
        l === 'bn' ? bn :
        l === 'de' ? de :
        l === 'it' ? it :
        l === 'ja' ? ja :
        l === 'pt' ? pt :
        l === 'ru' ? ru :
        l === 'tr' ? tr :
        enUS;
}

export function formateDate(to: Date, lang = 'en', from: Date = new Date()) {
    return formatRelative(to, from, { locale: getDateFnsLocale(lang) })
}

export const pushScreen = (componentId: string, screenName: string, title: string = '', words: any = {}) => {
    return Navigation.push(componentId, {
        component: {
            name: screenName,
            passProps: {
                doneButtonId: `done_${screenName}`
            },
            options: {
                topBar: {
                    title: {
                        text: title
                    },
                    rightButtons: [
                        {
                            id: `done_${screenName}`,
                            text: words.DONE,
                        }
                    ]
                }
            }
        }
    });
}

export const createModal = (SCREE_NAME: string, props: {
    noSearchBar?: boolean,
    isDarkThem?: boolean,
    hideStatusBar?: boolean,
    done?: (state?: any) => void
    [prop: string] : any
} = {}, text: {
    topBarTitle: string,
    searchBarPlaceholder?: string
} = {
    topBarTitle: '',
    searchBarPlaceholder: ''
}, words: any) => {

    const buttonIds = { cancelButtonId: `CANCEL_${SCREE_NAME}`, doneButtonId: `DONE_${SCREE_NAME}`}

    return Navigation.showModal({
        stack: {
            children: [{
                component: {
                    name: SCREE_NAME,
                    passProps: {
                        ...buttonIds,
                        ...props
                    },
                    options: {
                        statusBar: {
                            visible: props.hideStatusBar ? false : true
                        },
                        topBar: {
                            //hideOnScroll: true,
                            animate: true,
                            title: {
                                text: text.topBarTitle
                            },
                            ...(props.noSearchBar ? {} : {
                                searchBar: true,
                                hideNavBarOnFocusSearchBar: true,
                                searchBarHiddenWhenScrolling: true,
                                searchBarPlaceholder: text.searchBarPlaceholder ?
                                    text.searchBarPlaceholder :
                                    words.SEARCH
                            }),
                            leftButtons: [
                                {
                                    id: buttonIds.cancelButtonId,
                                    text: words.CANCEL
                                }
                            ],
                            ...(props.done ? {
                                rightButtons: [
                                    {
                                        id: buttonIds.doneButtonId,
                                        text: words.DONE,
                                    }
                                ]
                            } : {}
                            )
                        }
                    }
                }
            }]
        }
    });
}

export function simpleSearchOnList(text: string = '', items: ItemSearchBarList[], initialItems: ItemSearchBarList[] = []) {
    return text.length === 0 ? initialItems : items.filter(item =>
        item.title.toLowerCase().indexOf(text.toLowerCase()) !== -1 ||
        item.id.toLowerCase().indexOf(text.toLowerCase()) !== -1
    );
}