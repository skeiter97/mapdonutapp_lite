const WHITE = '#fff';

const PRIMARY_COLOR = 'yellow';
const SECONDARY_COLOR = 'red';

module.exports = {
    WHITE,

    PRIMARY_COLOR,
    SECONDARY_COLOR,
}