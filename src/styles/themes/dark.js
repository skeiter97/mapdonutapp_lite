const WHITE = '#fff';

// Welcome Screen
//const WELCOME_BG_GRADIANT = ['#01AA8D', '#0E465B', '#9A65C8'];
//const WELCOME_BG_LANGUAGE = '#fff';
//const WELCOME_TEXT_COLOR = '#fff';

// Theme colors

const PRIMARY_COLOR = '#5eed9a';
const SECONDARY_COLOR = 'blue';
const TEXT_COLOR = WHITE;
const TEXT_COLOR__BIO = '#f2f4f4';
const TEXT_COLOR_PLACEHOLDER = '#adadad';
const BG_LAYOUT = '#2C2C2C';
const BG_LAYOUT_ACCENT = '#3f3f3f';
const COVER_BG = '#587eef';
const BUTTON_BORDER_DEFAULT = '#6d6c6c';
const MASONRY_SELECTED = 'green';
const PRIMARY_COLOR_TEXT = '#000';

// CAMERA SELECTION
const LIBRARY_PHOTO_SELECTED_BORDER = '#fff';

// React native Navigation

const RN_TOPBAR_TITLE_COLOR = TEXT_COLOR;
const RN_BOTTOMTAB_COLOR = TEXT_COLOR;
const RN_BOTTOMTAB_BACKGROUND = BG_LAYOUT;
const RN_LAYOUT_BACKGROUND = BG_LAYOUT;
const RN_TOPBAR_BACKGROUND = BG_LAYOUT;
const RN_BOTTOMTAB_COLOR__ACCENT = PRIMARY_COLOR;

const MARK_PIN_COLOR = BG_LAYOUT;

module.exports = {

    //WELCOME_BG_GRADIANT,
    //WELCOME_BG_LANGUAGE,
    //WELCOME_TEXT_COLOR,

    PRIMARY_COLOR,
    SECONDARY_COLOR,
    TEXT_COLOR,
    TEXT_COLOR__BIO,
    TEXT_COLOR_PLACEHOLDER,
    BG_LAYOUT,
    BG_LAYOUT_ACCENT,
    COVER_BG,
    BUTTON_BORDER_DEFAULT,
    MASONRY_SELECTED,
    PRIMARY_COLOR_TEXT,
    LIBRARY_PHOTO_SELECTED_BORDER,

    MARK_PIN_COLOR,

    RN: {
        statusBar: {
            style: 'dark'
        },
        topBar: {
            barStyle: 'black',
            background: {
                color: RN_TOPBAR_BACKGROUND,
            },
            title: {
                color: RN_TOPBAR_TITLE_COLOR
            }
        },
        bottomTabs: {
            backgroundColor: RN_BOTTOMTAB_BACKGROUND
        },

        layout: {
            backgroundColor: RN_LAYOUT_BACKGROUND
        },

        bottomTab: {
            iconColor: RN_BOTTOMTAB_COLOR,
            textColor: RN_BOTTOMTAB_COLOR,
            selectedIconColor: RN_BOTTOMTAB_COLOR__ACCENT,
            selectedTextColor: RN_BOTTOMTAB_COLOR__ACCENT
        }
    }

}