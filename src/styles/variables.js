const defaultTheme = require('./themes/default');
const darkTheme = require('./themes/dark');
const lightTheme = require('./themes/light');

const GLOBAL_VARIABLES = require('../global/variables');
const availableThemes = GLOBAL_VARIABLES.THEMES;

const setEnvStyles = (themeName, themeVariables) => {
    const envs = {};
    
    for (var prop in themeVariables) {
        const isString = typeof (themeVariables[prop]) === 'string';
        if (themeVariables.hasOwnProperty(prop) && isString) {
            envs[`--${themeName}__${prop.toLowerCase().replace(/_/g, '-')}`] = themeVariables[prop];
        }
    }

    return envs;
}

const getTheme = (name) => {
    const theme = name === 'dark' ? darkTheme :
        name === 'light' ? lightTheme :
        defaultTheme;
    const variables = Object.assign({}, defaultTheme, theme);
    const environmentVariables = Object.assign({}, setEnvStyles(name, defaultTheme), setEnvStyles(name, theme));
    return {
        variables,
        environmentVariables
    }
};


const getThemes = () => availableThemes.reduce((prev, current) => {
    const currentValues = getTheme(current);
    return Object.assign(prev, {[current]: currentValues});
}, {});

module.exports = {
    
    getTheme,
    getThemes,

    environmentVariables: availableThemes.reduce((prev, current) => {
        const currentValues = getTheme(current);
        return Object.assign(prev, currentValues.environmentVariables );
    }, {})
}