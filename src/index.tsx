import React from 'react';
import { Navigation } from 'react-native-navigation';
import { Screens, startApp } from './screens';
import { Provider } from 'react-redux';
import { store } from './redux/stores';

import { gestureHandlerRootHOC } from 'react-native-gesture-handler';

// Register screens
Screens.forEach((ScreenComponent, key) =>
    Navigation.registerComponent(key, () => props => <Provider store={store}>
        <ScreenComponent {...props}> </ScreenComponent>
    </Provider>, () => gestureHandlerRootHOC(ScreenComponent))
);

// Start application
Navigation.events().registerAppLaunchedListener(() => startApp());
