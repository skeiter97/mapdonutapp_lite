export interface imageSelected {
    uri: string
}

export interface pageInfo {
    has_next_page: boolean,
    start_cursor?: string,
    end_cursor?: string,
}