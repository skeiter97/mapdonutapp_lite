export interface ItemSearchBarList {
    title: string;
    id: string;
    subtitle?: string,
    icon?: string,
    itemData?: any,
    color?: string
}