import { UserStore } from "../redux/reducers/userReducer";
import { themeState } from "../redux/reducers/themeReducer";
import { languageState } from "../redux/reducers/languageReducer";
import { UIState } from "../redux/reducers/UIReducer";
import { CurrentUserStore } from "../redux/reducers/currentUserReducer";
import { EventStore } from "../redux/reducers/eventsReducer";

export interface StoreApp {
    currentUser: CurrentUserStore,
    theme: themeState,
    language: languageState,
    ui: UIState,
    events: EventStore
}