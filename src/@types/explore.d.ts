import { Mapdonut_metadada } from "./general";

export interface Mapdonut_ExploreItem {
    id: string,
    type: string,
    url: string,
    tripId: string,
    metadata: Mapdonut_metadada
}

