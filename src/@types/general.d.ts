import { RNFirebase } from 'react-native-firebase';

export type FirestoreTimestamp = RNFirebase.firestore.Timestamp;
export type FieldValue = RNFirebase.firestore.FieldValue;
export type Geopoint = RNFirebase.firestore.GeoPoint;

export interface LatLng {
    latitude: Number,
    longitude: Number,
}

export interface simpleObject {
    [prop: string]: string
}

export interface simpleObjectAnyValues {
    [prop: string]: any
}

export interface MapdonutFirebaseStoragePre {
    uri: string
}

export interface MapdonutFirebaseStorage { // DEprecated
    url: string,
    id: string,
    metadata: Mapdonut_metadada
}

export interface Mapdonut_metadada extends simpleObjectAnyValues {
    isLoadedToCloud: boolean,
    progress: number
}