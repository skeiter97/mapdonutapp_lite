export interface Mapdonut_inputHook {
    value: string,
    onChangeText: (txt: string) => void,
    setValue: (txt: string) => void,
    placeholder: string,
    placeholderTextColor: string
}