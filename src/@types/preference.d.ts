
export interface Mapdonut_preference {
    id: string,
    name: string,
    main: boolean,
    createdByUserId: string
}
