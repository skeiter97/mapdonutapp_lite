import { Geopoint, MapdonutFirebaseStorage, FieldValue, FirestoreTimestamp, simpleObjectAnyValues } from "./general";
import { imageSelected } from "./camera";
import { Mapdonut_ExploreItem } from "./explore";

export interface Mapdonut_Trip {
    id: string,
    location: Geopoint;
    locationText: string,
    locationId: string,
    description: string,
    photos: Mapdonut_ExploreItem[],
    createdAt: FirestoreTimestamp,
    updatedAt: FirestoreTimestamp,
    userId: string,
    metadata?: simpleObjectAnyValues
}

export interface Mapdonut_Trip_pre {
    locationGeopoint: Geopoint;
    locationText: string,
    locationId: string,
    description: string,
    photosToUpload: imageSelected[],
}
