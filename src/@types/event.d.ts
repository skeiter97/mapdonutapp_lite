import { FieldValue, FirestoreTimestamp, Geopoint } from './general';

export interface Mapdonut_Event {
    id?: string;
    title: string;
    photoURL: string;
    userUid:string;
    description: string;
    ticketsUrl: string;
    locationText: string;
    location: Geopoint;
    datetime: FieldValue;
    createdAt: FieldValue;
    privacy: string;
    allowComments: boolean;
}
