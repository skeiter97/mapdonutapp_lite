import { FieldValue, Geopoint } from './general';
import { RNFirebase } from 'react-native-firebase';

export interface Mapdonut_Social {
    url: string;
    username: string;
    provider: string;
    privacy: string;
}

export interface Mapdonut_Preference {
    id?: string;
    name: string;
    data: any;
}

export interface Mapdonut_User {

    uid: string,
    displayName: string;
    photoURL: string;
    coverURL: string;
    email: string;
    phoneNumber: string;
    providerData: RNFirebase.UserInfo[]

    bio: string;
    username: string;
    socials?: FieldValue;

    themeName: string;
    languageSelected: string;

    location: Geopoint | null;
    lastSignInAt: FieldValue;
    created_at: FieldValue,
    updated_at: FieldValue,
    firstLogin: boolean,
    emailVerified: boolean,
    allowLocation: boolean,
    allowPushNotifications: boolean,
    devices: string[] | FieldValue;
    preferences: Mapdonut_Preference[] | FieldValue

}