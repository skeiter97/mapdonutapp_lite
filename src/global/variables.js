const VERSION = '0.2.5';

const REGULAR_ANIMATION_TIMING = 300;
const THEMES = ['default', 'dark'];
const THEME_DEFAULT = THEMES[0];
const COVER_DEFAULT_URL = 'https://mymodernmet.com/wp/wp-content/uploads/2019/05/wide-angle-landscape-photography-tutorial-mads-peter-iversen-2.jpg';
const AVATAR_DEFAULT_URL = 'https://admin.my220.com/images/default_image.png';
const DEFAULT_LOCATION = { lat: 39.741952, lng: -104.988589};

const DICTIONARIES = {
    en: require('../../i18n/en.json'),
    es: require('../../i18n/es.json'),
    ar: require('../../i18n/ar.json'),
    bn: require('../../i18n/bn.json'),
    de: require('../../i18n/de.json'),
    hi: require('../../i18n/hi.json'),
    it: require('../../i18n/it.json'),
    ja: require('../../i18n/ja.json'),
    jv: require('../../i18n/jv.json'),
    pt: require('../../i18n/pt.json'),
    ru: require('../../i18n/ru.json'),
    tr: require('../../i18n/tr.json'),
    que: require('../../i18n/que.json'),
    zh_hans: require('../../i18n/zh-hans.json'),
    zh_hant: require('../../i18n/zh-hant.json')
}

const LANGUAGES = Object.keys(DICTIONARIES).map(lang => lang === 'zh_hans' ? 'zh-hans' :
    lang === 'zh_hant' ? 'zh-hant' :
    lang
);

const LANGUAGE_DEFAULT = LANGUAGES[0];

module.exports = {
    REGULAR_ANIMATION_TIMING,
    THEMES,
    VERSION,
    THEME_DEFAULT,
    LANGUAGES,
    LANGUAGE_DEFAULT,
    COVER_DEFAULT_URL,
    DICTIONARIES,
    AVATAR_DEFAULT_URL,
    DEFAULT_LOCATION
}